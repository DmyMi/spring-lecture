package cloud.dmytrominochkin.spring.boot.entity

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id

/**
 * JPA Entity representing an employee in the database.
 *
 * @property firstName The employee's first name
 * @property lastName The employee's last name
 * @property id The unique identifier for the employee, auto-generated when persisting to the database
 */
@Entity
data class Employee(
    val firstName: String,
    val lastName: String,
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = 0
)