package cloud.dmytrominochkin.spring.boot

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

/**
 * Main Spring Boot application class that serves as the entry point for the application.
 * This class is annotated with [SpringBootApplication] which enables Spring Boot's auto-configuration,
 * component scanning, and additional configuration capabilities.
 */
@SpringBootApplication
class BootApplication

/**
 * Application entry point that starts the Spring Boot application.
 *
 * @param args Command line arguments passed to the application
 */
fun main(args: Array<String>) {
    runApplication<BootApplication>(*args)
}