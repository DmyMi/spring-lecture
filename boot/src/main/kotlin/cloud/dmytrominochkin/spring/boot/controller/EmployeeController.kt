package cloud.dmytrominochkin.spring.boot.controller

import cloud.dmytrominochkin.spring.boot.dto.EmployeeDto
import cloud.dmytrominochkin.spring.boot.svc.EmployeeService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI

/**
 * REST controller that handles CRUD operations for employees.
 * Provides endpoints for creating, reading, updating, and deleting employee records.
 *
 * @property service The [EmployeeService] used to handle business logic for employee operations
 */
@RestController
@RequestMapping("/employees")
class EmployeeController @Autowired constructor(private val service: EmployeeService) {
    /**
     * Creates a new employee record.
     *
     * @param employee The employee data transfer object containing the employee information
     * @return ResponseEntity containing the created employee with location header
     */
    @PostMapping
    fun create(@RequestBody employee: EmployeeDto): ResponseEntity<EmployeeDto> {
        val emp = service.createEmployee(employee)
        return ResponseEntity.created(URI.create("/employees/${emp.id}")).body(emp).also {
            logger.info("Successfully created new employee")
        }
    }

    /**
     * Retrieves an employee by their ID.
     *
     * @param id The ID of the employee to retrieve
     * @return ResponseEntity containing the employee if found, or 404 if not found
     */
    @GetMapping("{id}")
    fun getById(@PathVariable("id") id: Long): ResponseEntity<EmployeeDto> =
        service.getEmployeeById(id)?.let {
            ResponseEntity.ok(it).also {
                logger.info("Successfully loaded an employee")
            }
        } ?: ResponseEntity.notFound().build<EmployeeDto>().also {
            logger.info("Employee not found")
        }

    /**
     * Retrieves all employees.
     *
     * @return List of all employees in the system
     */
    @GetMapping(value = ["", "/"])
    fun all(): List<EmployeeDto> = service.getAllEmployees().also {
        logger.info("Got ${it.size} employees")
    }

    /**
     * Updates an existing employee record.
     *
     * @param id The ID of the employee to update
     * @param employee The updated employee information
     * @return The updated employee data
     */
    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long, employee: EmployeeDto) = service.updateEmployee(id, employee).also {
        logger.info("Successfully updated an employee")
    }

    /**
     * Deletes an employee record.
     *
     * @param id The ID of the employee to delete
     */
    @DeleteMapping("{id}")
    fun delete(@PathVariable("id") id: Long) = service.deleteEmployee(id).also {
        logger.info("Successfully deleted an employee")
    }

    companion object {
        private val logger = LoggerFactory.getLogger(EmployeeService::class.java)
    }
}