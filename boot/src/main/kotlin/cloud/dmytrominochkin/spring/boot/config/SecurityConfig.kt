package cloud.dmytrominochkin.spring.boot.config

import cloud.dmytrominochkin.spring.boot.util.CustomRSAKey
import com.nimbusds.jose.jwk.JWKSet
import com.nimbusds.jose.jwk.RSAKey
import com.nimbusds.jose.jwk.source.ImmutableJWKSet
import com.nimbusds.jose.proc.SecurityContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.oauth2.jwt.JwtDecoder
import org.springframework.security.oauth2.jwt.JwtEncoder
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder
import org.springframework.security.oauth2.jwt.NimbusJwtEncoder
import org.springframework.security.oauth2.server.resource.web.BearerTokenAuthenticationEntryPoint
import org.springframework.security.oauth2.server.resource.web.access.BearerTokenAccessDeniedHandler
import org.springframework.security.provisioning.InMemoryUserDetailsManager
import org.springframework.security.web.SecurityFilterChain
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey

@Configuration
@EnableWebSecurity
class SecurityConfig {
    /**
     * Configures the HTTP security rules for the application.
     * Defines how requests are authorized, handles CSRF protection specifically for token endpoint,
     * sets up HTTP Basic and JWT resource server configurations, and customizes session management to stateless.
     *
     * @param http The [HttpSecurity] configuration builder provided by Spring Security.
     * @return A [SecurityFilterChain] instance encapsulating the security rules.
     */
    @Bean
    fun securityFilterChain(http: HttpSecurity): SecurityFilterChain =
        http
            .authorizeHttpRequests {
                it.requestMatchers("/word", "/actuator/**").permitAll()
                    .anyRequest().authenticated()
            }
            .csrf { it.ignoringRequestMatchers("/token") }
            .httpBasic(Customizer.withDefaults())
            .oauth2ResourceServer { it.jwt(Customizer.withDefaults()) }
            .sessionManagement { it.sessionCreationPolicy(SessionCreationPolicy.STATELESS) }
            .exceptionHandling {
                it
                    .authenticationEntryPoint(BearerTokenAuthenticationEntryPoint())
                    .accessDeniedHandler(BearerTokenAccessDeniedHandler())
            }
            .build()

    /**
     * Provides the custom [UserDetailsService] for authentication.
     *
     * @return A [UserDetailsService] instance that is used for loading user-specific data.
     */
    @Bean
    fun userDetailsService(): UserDetailsService {
        val user = User
            .withUsername("user")
            .password("{noop}password")
            .authorities("application")
            .build()

        return InMemoryUserDetailsManager(user)
    }

    /**
     * Provides a [JwtDecoder] bean that decodes JWTs using a public RSA key.
     *
     * @return [JwtDecoder] A JWT decoder that validates and decodes JWTs using the RSA public key.
     */
    @Bean
    fun jwtDecoder(): JwtDecoder =
        NimbusJwtDecoder
            .withPublicKey(CustomRSAKey.getKeyPair().public as RSAPublicKey)
            .build()

    /**
     * Provides a [JwtEncoder] for encoding JWTs using RSA private key.
     * The encoder is used for signing JWTs that are issued to clients.
     *
     * @return [JwtEncoder] A JWT encoder configured to use the RSA private key for token signing.
     */
    @Bean
    fun jwtEncoder(): JwtEncoder {
        val jwk = RSAKey
            .Builder(CustomRSAKey.getKeyPair().public as RSAPublicKey)
            .privateKey(CustomRSAKey.getKeyPair().private as RSAPrivateKey)
            .build()
        val jwks = ImmutableJWKSet<SecurityContext>(JWKSet(jwk))
        return NimbusJwtEncoder(jwks)
    }
}