package cloud.dmytrominochkin.spring.boot.dto

/**
 * Data Transfer Object representing an employee.
 * Used for transferring employee data between different layers of the application.
 *
 * @property firstName The employee's first name
 * @property lastName The employee's last name
 * @property id The unique identifier for the employee, null when creating a new employee
 */
data class EmployeeDto(
    val firstName: String,
    val lastName: String,
    val id: Long? = 0
)
