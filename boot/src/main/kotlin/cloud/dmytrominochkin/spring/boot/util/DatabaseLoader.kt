package cloud.dmytrominochkin.spring.boot.util

import cloud.dmytrominochkin.spring.boot.entity.Employee
import cloud.dmytrominochkin.spring.boot.repo.EmployeeRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

/**
 * Component responsible for loading initial data into the database when the application starts.
 * This class provides a bean that implements [CommandLineRunner] to execute database initialization logic.
 */
@Component
class DatabaseLoader {
    /**
     * Creates and returns a [CommandLineRunner] bean that initializes the database with sample employee data.
     * This method is called automatically by Spring when the application context is initialized.
     *
     * @param repository The [EmployeeRepository] used to save employee records
     * @return [CommandLineRunner] A command line runner that populates the database
     */
    @Bean
    fun init(repository: EmployeeRepository) = CommandLineRunner {
        repository.save(Employee("Mykola", "Petrenko"))
        repository.save(Employee("Patron", "Pes"))
    }
}