package cloud.dmytrominochkin.spring.boot.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.oauth2.jwt.JwtClaimsSet
import org.springframework.security.oauth2.jwt.JwtEncoder
import org.springframework.security.oauth2.jwt.JwtEncoderParameters
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController
import java.time.Instant

/**
 * Controller responsible for generating JWT tokens. This controller handles the "/token"
 * endpoint and issues tokens to authenticated users.
 *
 * @property encoder The [JwtEncoder] used to encode the JWT claims into a token.
 */
@RestController
class TokenController @Autowired constructor(
    private val encoder: JwtEncoder
) {
    /**
     * Handles POST requests to "/token" by generating a JWT for the authenticated user.
     *
     * This method collects the necessary claims, sets the issuer, subject, and scopes
     * based on the user's authorities, and uses the provided [JwtEncoder] to create the token.
     *
     * @param authentication The [Authentication] object containing the principal and authorities
     *                       which will be encoded into the JWT.
     * @return A string that represents the JWT token value.
     */
    @PostMapping("/token")
    fun token(authentication: Authentication): String {
        val now = Instant.now()
        val expiry = 36000L // Token validity in seconds (36000 seconds = 10 hours).
        val scope = authentication
            .authorities
            .joinToString(" ", transform = GrantedAuthority::getAuthority)
        val claims = JwtClaimsSet.builder()
            .issuer("self")
            .issuedAt(now)
            .expiresAt(now.plusSeconds(expiry))
            .subject(authentication.name)
            .claim("scope", scope)
            .build()
        return encoder.encode(JwtEncoderParameters.from(claims)).tokenValue
    }
}