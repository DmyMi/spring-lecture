package cloud.dmytrominochkin.spring.boot.controller

import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

/**
 * REST controller that provides access to a configured word value.
 * The word value is injected from the application configuration using Spring's @Value annotation.
 */
@RestController
class WordController {

    // Need to escape '$' so it is not treated as string template
    @Value("\${example.word}")
    private lateinit var word: String

    /**
     * Retrieves the currently configured word value.
     *
     * @return A string containing the current word value in a formatted message
     */
    @GetMapping("/word")
    fun word(): String = "Current word is: $word"
}
