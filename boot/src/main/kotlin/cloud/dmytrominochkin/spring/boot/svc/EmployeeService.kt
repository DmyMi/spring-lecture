package cloud.dmytrominochkin.spring.boot.svc

import cloud.dmytrominochkin.spring.boot.dto.EmployeeDto
import cloud.dmytrominochkin.spring.boot.entity.Employee
import cloud.dmytrominochkin.spring.boot.repo.EmployeeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Service class that handles business logic for employee operations.
 * Provides methods for creating, reading, updating, and deleting employee records.
 *
 * @property repository The [EmployeeRepository] used for database operations
 */
@Service
class EmployeeService @Autowired constructor(
    private val repository: EmployeeRepository
) {
    /**
     * Creates a new employee record.
     *
     * @param employee The employee DTO containing the employee information
     * @return The created employee as a DTO
     */
    fun createEmployee(employee: EmployeeDto): EmployeeDto {
        return repository.save(employee.toEntity()).toDto()
    }

    /**
     * Retrieves all employees from the database.
     *
     * @return List of all employees as DTOs
     */
    fun getAllEmployees(): List<EmployeeDto> {
        return repository.findAll().map { it.toDto() }
    }

    /**
     * Retrieves an employee by their ID.
     *
     * @param id The ID of the employee to retrieve
     * @return The employee as a DTO if found, null otherwise
     */
    fun getEmployeeById(id: Long): EmployeeDto? {
        return repository.findById(id).map { it.toDto() }.orElse(null)
    }

    /**
     * Updates an existing employee record.
     *
     * @param id The ID of the employee to update
     * @param employee The updated employee information as a DTO
     * @return The updated employee as a DTO
     */
    fun updateEmployee(id: Long, employee: EmployeeDto): EmployeeDto {
        return repository.save(employee.toEntity().copy(id = id)).toDto()
    }

    /**
     * Deletes an employee record by ID.
     *
     * @param id The ID of the employee to delete
     */
    fun deleteEmployee(id: Long) {
        repository.deleteById(id)
    }
}

/**
 * Converts an [EmployeeDto] to an [Employee] entity.
 *
 * @return The converted Employee entity
 */
private fun EmployeeDto.toEntity(): Employee = Employee(
    firstName,
    lastName,
    id
)

/**
 * Converts an [Employee] entity to an [EmployeeDto].
 *
 * @return The converted EmployeeDto
 */
private fun Employee.toDto(): EmployeeDto = EmployeeDto(
    firstName,
    lastName,
    id
)