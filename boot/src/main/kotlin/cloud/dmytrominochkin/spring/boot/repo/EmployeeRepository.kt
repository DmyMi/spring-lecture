package cloud.dmytrominochkin.spring.boot.repo

import cloud.dmytrominochkin.spring.boot.entity.Employee
import org.springframework.data.jpa.repository.JpaRepository

/**
 * Spring Data JPA repository interface for [Employee] entities.
 * Provides CRUD operations and additional query methods for Employee entities.
 * Extends [JpaRepository] with [Employee] as the entity type and [Long] as the ID type.
 */
interface EmployeeRepository : JpaRepository<Employee, Long>
