plugins {
    kotlin("jvm")
    // The noArg plugin automatically generates zero-argument constructors for classes annotated with specific annotations.
    // Hibernate requires no-argument constructors for entity classes to instantiate them through reflection.
    id("org.jetbrains.kotlin.plugin.noarg")
    // The allOpen plugin makes classes annotated with specific annotations non-final. Kotlin classes are final by default,
    // which prevents Hibernate from proxying them to handle lazy loading of associations. The allOpen plugin is essential
    // for Hibernate to function correctly by enabling runtime enhancement of entity classes.
    id("org.jetbrains.kotlin.plugin.allopen")
    // The Spring plugin automatically applies both noArg and allOpen configurations for typical Spring annotations,
    // facilitating the integration with Spring Data, which also benefits from these adjustments for proxying and reflection.
    id("org.jetbrains.kotlin.plugin.spring")
    application
}

group = "cloud.dmytrominochkin.spring"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    compileOnly(libs.jakarta.servlet)
    implementation(libs.jetty.webapp)
    implementation(libs.bundles.logging)

    implementation(libs.spring.mvc)
    implementation(libs.spring.thymeleaf.core)
    implementation(libs.spring.thymeleaf.security)
    // Kotlin's reflection is required for Spring Data's runtime reflection capabilities.
    implementation(libs.kotlin.reflect)
    implementation(libs.spring.data)
    implementation(libs.hsqldb)
    implementation(libs.hibernate.core)

    // Spring Security
    implementation(libs.spring.security.config)
    implementation(libs.spring.security.web)

    // Serialization
    implementation(libs.bundles.jackson)

    testImplementation(libs.kotlin.test)
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("-parameters")
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(17)
}

application {
    mainClass.set("cloud.dmytrominochkin.spring.security.SecurityMainKt")
}

// Configures the noArg plugin to generate zero-argument constructors for JPA entities,
// which is necessary for entity instantiation by Hibernate.
noArg {
    annotation("jakarta.persistence.Entity")
    annotation("jakarta.persistence.MappedSuperclass")
    annotation("jakarta.persistence.Embeddable")
    invokeInitializers = true // Ensures that property initializers are called in the generated constructors.
}

// Configures the allOpen plugin to prevent Kotlin from marking classes as final if they are annotated
// with JPA annotations. This is crucial for enabling runtime proxying and AOP-based enhancements.
allOpen {
    annotation("jakarta.persistence.Entity")
    annotation("jakarta.persistence.MappedSuperclass")
    annotation("jakarta.persistence.Embeddable")
}