package cloud.dmytrominochkin.spring.security.dto

data class UserResponse(
    val login: String,
    val roleName: String
)
