package cloud.dmytrominochkin.spring.security.dto

data class UserRequest(
    var login: String,
    var password: String
)
