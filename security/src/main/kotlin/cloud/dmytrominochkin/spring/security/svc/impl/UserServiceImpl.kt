package cloud.dmytrominochkin.spring.security.svc.impl

import cloud.dmytrominochkin.spring.security.dto.UserRequest
import cloud.dmytrominochkin.spring.security.entity.CustomRole
import cloud.dmytrominochkin.spring.security.entity.CustomUser
import cloud.dmytrominochkin.spring.security.repository.RoleRepository
import cloud.dmytrominochkin.spring.security.repository.UserRepository
import cloud.dmytrominochkin.spring.security.svc.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PostAuthorize
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

/**
 * Implementation of [UserService] that interacts with data repositories to manage user data.
 * This class provides concrete implementations of user management operations, including saving users
 * and loading user details.
 *
 * @property userRepository Repository for user data operations.
 * @property roleRepository Repository for role data operations.
 * @property passwordEncoder Service for encoding passwords.
 */
@Service
class UserServiceImpl @Autowired constructor(
    private val userRepository: UserRepository,
    private val roleRepository: RoleRepository,
    private val passwordEncoder: PasswordEncoder
) : UserService {

    /**
     * Initialize default data for the application.
     */
    init {
        initDefaultData()
    }

    override fun saveUser(userRequest: UserRequest): Boolean {
        val user = CustomUser(
            login = userRequest.login,
            password = passwordEncoder.encode(userRequest.password),
            role = roleRepository.findByName("user")
        )
        userRepository.save(user)
        return true
    }

    // @PreAuthorize ensures that the method is only accessible to users with 'user' or 'admin' authorities
    // who are requesting their own data, as indicated by the username.
    @PreAuthorize("hasAnyAuthority('user', 'admin') and #username == authentication.principal.username")
    // @PostAuthorize checks after the method execution to confirm that the returned object (user login)
    // matches the username in the current authentication token. This provides an additional layer of security,
    // ensuring data consistency and integrity.
    @PostAuthorize("hasAnyAuthority('user', 'admin') and returnObject.login == authentication.principal.username")
    override fun loadUser(username: String): CustomUser? {
        return userRepository.findByLogin(username)
    }

    override fun findByLogin(login: String): CustomUser? = userRepository.findByLogin(login)

    /**
     * Initializes default roles and an admin user if the repository is initially empty.
     * This method is called upon instantiation of the service.
     */
    private fun initDefaultData() {
        if ((roleRepository.count() == 0L) and (userRepository.count() == 0L)) {
            val adminRole = roleRepository.save(CustomRole(name = "admin"))
            roleRepository.save(CustomRole(name = "user"))
            userRepository.save(CustomUser(login = "admin", password = passwordEncoder.encode("admin"), role = adminRole))
        }
    }
}