package cloud.dmytrominochkin.spring.security.entity

import jakarta.persistence.*

/**
 * Entity representation of a custom user for authentication and authorization purposes.
 * This class is mapped to a "customusers" table in the database with fields for login credentials and role associations.
 *
 * @property id Unique identifier for the user, automatically generated.
 * @property login Username used for login.
 * @property password Password used for authentication.
 * @property role The security role associated with the user, mapped as a many-to-one relationship.
 */
@Entity
@Table(name = "customusers")
data class CustomUser(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0,
    var login: String = "",
    var password: String = "",
    @ManyToOne
    @JoinColumn(name = "role_id")
    var role: CustomRole? = null
)