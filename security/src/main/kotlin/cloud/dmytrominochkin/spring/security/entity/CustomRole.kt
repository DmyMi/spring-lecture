package cloud.dmytrominochkin.spring.security.entity

import jakarta.persistence.*

/**
 * Entity representation of a custom role associated with users.
 * Roles are used to grant authority levels within the application and are mapped to a "customroles" table.
 *
 * @property id Unique identifier for the role, automatically generated.
 * @property name The name of the role, e.g., "ADMIN", "USER", which is used to grant specific authorities.
 */
@Entity
@Table(name = "customroles")
data class CustomRole(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0,
    var name: String = ""
)