package cloud.dmytrominochkin.spring.security.custom

import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder

/**
 * Custom authentication provider that uses a username and password to authenticate a user.
 * This class integrates with Spring Security's authentication mechanism by providing a custom way
 * to authenticate users based on the credentials provided in the login process.
 *
 * @property userDetailsService Service that loads user-specific data.
 * @property passwordEncoder Service that encodes and verifies passwords.
 */
class CustomAuthProvider(
    private val userDetailsService: UserDetailsService,
    private val passwordEncoder: PasswordEncoder
): AuthenticationProvider {
    /**
     * Authenticates a user based on username and password.
     *
     * This method attempts to authenticate the user by first loading the user details
     * via the [UserDetailsService] and then comparing the provided password with the stored password using [PasswordEncoder].
     * If the password matches, it creates a fully populated authentication token.
     *
     * @param authentication A token containing the user's identity and credentials, typically username and password.
     * @return A fully authenticated object including credentials and granted authorities.
     * @throws BadCredentialsException If the password does not match the stored value.
     */
    override fun authenticate(authentication: Authentication): Authentication {
        val name = authentication.name
        val password = authentication.credentials as String
        val user = userDetailsService.loadUserByUsername(name)

        if (!passwordEncoder.matches(password, user.password)) {
            throw BadCredentialsException("Wrong password")
        }

        return UsernamePasswordAuthenticationToken(user, password, user.authorities)
    }

    /**
     * Determines if this provider can support the authentication object type.
     *
     * This method checks if the provided [Authentication] class type is supported by this provider.
     *
     * @param authentication An authentication class type to be supported.
     * @return True if the authentication type is [UsernamePasswordAuthenticationToken]; false otherwise.
     */
    override fun supports(authentication: Class<*>?): Boolean =
        authentication?.equals(UsernamePasswordAuthenticationToken::class.java) ?: false
}