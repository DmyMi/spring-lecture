package cloud.dmytrominochkin.spring.security.custom

import cloud.dmytrominochkin.spring.security.entity.CustomUser
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

/**
 * SecurityUser is an implementation of [UserDetails] which adapts the [CustomUser] entity
 * to be usable by Spring Security for authentication and authorization processes.
 *
 * This class includes methods defined by the [UserDetails] interface to support authentication features
 * such as username and password checks, and role-based authority checks.
 *
 * @property userName The username associated with the user.
 * @property password The password used for authentication.
 * @property authorities The collection of authorities granted to the user.
 * @property isActive Boolean flag indicating if the user's account is active.
 */
data class SecurityUser(
    private val userName: String,
    private val password: String,
    private val authorities: MutableList<SimpleGrantedAuthority>,
    private val isActive: Boolean
) : UserDetails {
    override fun getAuthorities(): MutableCollection<out GrantedAuthority> = authorities

    override fun getPassword(): String = password

    override fun getUsername(): String = userName

    override fun isAccountNonExpired(): Boolean = isActive

    override fun isAccountNonLocked(): Boolean = isActive

    override fun isCredentialsNonExpired(): Boolean = isActive

    override fun isEnabled(): Boolean = isActive

    companion object {
        /**
         * Factory method to create a [SecurityUser] from a [CustomUser] entity.
         * This method allows easy conversion from the database entity model to a Spring Security compliant model.
         *
         * @param user The [CustomUser] entity from which to create the [SecurityUser].
         * @return A [UserDetails] object populated with the CustomUser's data and roles.
         */
        @JvmStatic
        fun fromUser(user: CustomUser): UserDetails = SecurityUser(
            user.login,
            user.password,
            mutableListOf(SimpleGrantedAuthority(user.role?.name ?: "user")),
            true
        )
    }
}