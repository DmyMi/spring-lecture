package cloud.dmytrominochkin.spring.security.config

import cloud.dmytrominochkin.spring.security.custom.CustomAuthProvider
import cloud.dmytrominochkin.spring.security.custom.CustomUserDetailsService
import cloud.dmytrominochkin.spring.security.svc.UserService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.util.matcher.AntPathRequestMatcher


/**
 * Configuration class for Spring Security that sets up method-level security and configures
 * the global web security features of the application.
 *
 * [EnableWebSecurity]: This annotation enables Spring Security's web security support
 * and provides the Spring MVC integration.
 *
 * [EnableMethodSecurity]: This annotation enables method level security based on annotations. It allows
 * you to use annotations like @PreAuthorize, @PostAuthorize, @Secured, and @RolesAllowed, providing a
 * flexible way to manage security at the method level within your service layer.
 * - `prePostEnabled`: Allows the use of Spring's @PreAuthorize and @PostAuthorize annotations.
 * - `securedEnabled`: Allows the use of the @Secured annotation.
 * - `jsr250Enabled`: Allows the use of JSR-250 based annotations like @RolesAllowed.
 *
 * This class also configures the security filter chain that dictates the security policy for HTTP requests,
 * such as which URLs are secured and the required authentication method.
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity(
    prePostEnabled = true,
    securedEnabled = true,
    jsr250Enabled = true
)
class SecurityConfig {

    /**
     * Defines the Spring Security filter chain configuration.
     *
     * This method sets up form login, logout mechanisms, and HTTP request authorization requirements.
     *
     * @param http The [HttpSecurity] configuration builder provided by Spring Security.
     * @return A fully configured [SecurityFilterChain].
     */
    @Bean
    fun securityFilterChain(http: HttpSecurity): SecurityFilterChain =
        http
            // Configure form login with a custom login page.
            .formLogin { login ->
                login
                    .loginPage("/login")
                    .permitAll() // Allow all users to access the login page.
            }
            // Configure logout settings.
            .logout { logout ->
                logout
                    .logoutUrl("/logout")
                    .permitAll()  // Allow all users to initiate logout.
            }
            // Define authorization policies for HTTP requests.
            .authorizeHttpRequests { authorize ->
                authorize
                    // Allow unrestricted access to signup.
                    .requestMatchers(AntPathRequestMatcher.antMatcher("/signup")).permitAll()
                    // Restrict access to admin paths.
                    .requestMatchers(AntPathRequestMatcher.antMatcher("/admin/**")).hasAuthority("admin")
                    // Require authentication for any other requests.
                    .anyRequest().authenticated()
            }
            // Enable HTTP Basic Authentication for API access.
            .httpBasic(Customizer.withDefaults())
            // Disable CSRF protection for simplicity and demonstration purposes.
            .csrf { csrf -> csrf.disable() }
            .build()

    /**
     * Provides the custom [UserDetailsService] for authentication.
     *
     * @param userService The service that provides user data access.
     * @return A [UserDetailsService] instance that is used for loading user-specific data.
     */
    @Bean
    fun userDetailsService(userService: UserService): UserDetailsService =
        CustomUserDetailsService(userService)

    /**
     * Provides a password encoder that uses BCrypt hashing algorithm.
     *
     * @return A [BCryptPasswordEncoder] with a strength (log rounds) of 12.
     */
    @Bean
    fun passwordEncoder(): PasswordEncoder = BCryptPasswordEncoder(12)

    /**
     * Defines a custom [AuthenticationProvider] that uses the custom [UserDetailsService] and [PasswordEncoder].
     *
     * @param userService The [UserDetailsService] for loading user details.
     * @param passwordEncoder The [PasswordEncoder] for verifying passwords.
     * @return An [AuthenticationProvider] for authenticating users.
     */
    @Bean
    fun authProvider(userService: UserDetailsService, passwordEncoder: PasswordEncoder): AuthenticationProvider =
        CustomAuthProvider(userService, passwordEncoder)

    /**
     * Provides the [AuthenticationManager] used by Spring Security to handle authentication.
     *
     * @param http The [HttpSecurity] instance used to build the [AuthenticationManager].
     * @param authenticationProvider The custom [AuthenticationProvider] used for user authentication.
     * @return An [AuthenticationManager] that processes authentication requests.
     */
    @Bean
    fun authManager(http: HttpSecurity, authenticationProvider: AuthenticationProvider): AuthenticationManager =
        http.getSharedObject(AuthenticationManagerBuilder::class.java).also {
            it.authenticationProvider(authenticationProvider)
        }.build()
}