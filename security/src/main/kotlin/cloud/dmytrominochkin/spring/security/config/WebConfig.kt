package cloud.dmytrominochkin.spring.security.config

import cloud.dmytrominochkin.spring.security.routes.functionalRouter
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.accept.ContentNegotiationManager
import org.springframework.web.servlet.ViewResolver
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.function.RouterFunction
import org.thymeleaf.extras.springsecurity6.dialect.SpringSecurityDialect
import org.thymeleaf.spring6.SpringTemplateEngine
import org.thymeleaf.spring6.templateresolver.SpringResourceTemplateResolver
import org.thymeleaf.spring6.view.ThymeleafViewResolver
import java.text.SimpleDateFormat


/**
 * Configuration class for Spring MVC, enabling MVC support, including view resolution with Thymeleaf, configuring JSON message converters
 * and functional endpoints for handling HTTP requests.
 *
 * This class implements the [WebMvcConfigurer] interface to customize the configuration of Spring MVC.
 */
@Configuration
@EnableWebMvc
class WebConfig : WebMvcConfigurer {

    /**
     * Configures the JSON message converters for Spring MVC's HTTP messaging.
     * This method sets up a Jackson object mapper for JSON processing, enabling pretty printing,
     * and configuring date format handling.
     *
     * @param converters A mutable list of [HttpMessageConverter] to which custom converters can be added.
     */
    override fun configureMessageConverters(converters: MutableList<HttpMessageConverter<*>>) {
        val builder = Jackson2ObjectMapperBuilder()
            .indentOutput(true)
            .dateFormat(SimpleDateFormat("yyyy-MM-dd"))
            .modulesToInstall(KotlinModule.Builder().build())

        converters
            .add(MappingJackson2HttpMessageConverter(builder.build()))
    }

    /**
     * Configures the template resolver for Thymeleaf, specifying the location of view templates.
     *
     * @param applicationContext The Spring [ApplicationContext], automatically injected by Spring.
     * @return The configured template resolver.
     */
    @Bean
    fun templateResolver(applicationContext: ApplicationContext) = SpringResourceTemplateResolver().apply {
        setApplicationContext(applicationContext) // Link with Spring's ApplicationContext.
        prefix = "classpath:/templates/" // Prefix that points to the location of the Thymeleaf templates.
        suffix = ".html" // Suffix used for the Thymeleaf template files.
    }

    /**
     * Creates and configures the Thymeleaf template engine with the template resolver.
     *
     * @param templateResolver The Thymeleaf template resolver for resolving views.
     * @return The configured [SpringTemplateEngine] instance.
     */
    @Bean
    fun templateEngine(templateResolver: SpringResourceTemplateResolver) = SpringTemplateEngine().apply {
        setTemplateResolver(templateResolver) // Set the template resolver.
        enableSpringELCompiler = true // Enable Spring Expression Language compiler for efficiency.
        addDialect(SpringSecurityDialect())
    }

    /**
     * Configures the view resolver for Thymeleaf, which helps Spring MVC serve HTML content.
     *
     * @param applicationContext The Spring [ApplicationContext], automatically injected by Spring.
     * @param templateEngine The Thymeleaf template engine for processing templates.
     * @return A [ViewResolver] instance for resolving view names to actual views.
     */
    @Bean
    fun viewResolver(applicationContext: ApplicationContext, templateEngine: SpringTemplateEngine): ViewResolver =
        with(ViewResolverRegistry(ContentNegotiationManager(), applicationContext)) {
            val resolver = ThymeleafViewResolver()
            resolver.templateEngine = templateEngine // Link with the Thymeleaf template engine.
            viewResolver(resolver) // Register the resolver.
            return resolver
        }

    /**
     * Provides a router function bean configured with routes for handling HTTP requests.
     */
    @Bean
    fun functionalRoutes(): RouterFunction<*> = functionalRouter
}