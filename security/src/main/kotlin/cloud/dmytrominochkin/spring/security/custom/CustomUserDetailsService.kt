package cloud.dmytrominochkin.spring.security.custom

import cloud.dmytrominochkin.spring.security.svc.UserService
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException

/**
 * Custom implementation of the [UserDetailsService] to integrate with the application's user management system.
 *
 * This service is responsible for retrieving user details from the application's persistent storage via the [UserService],
 * and converting them into a [UserDetails] object that Spring Security can use for authentication and authorization.
 *
 * @property userService The service used to fetch user data from the data source.
 */
class CustomUserDetailsService(
    private val userService: UserService
) : UserDetailsService {
    /**
     * Loads the user-specific data necessary for Spring Security.
     *
     * Given a username, this method uses the [UserService] to fetch the corresponding user entity.
     * If the user is found, it is then converted to a [UserDetails] object which Spring Security uses
     * for configuring authentication tokens and authorizations. If the user is not found, this method
     * throws a [UsernameNotFoundException].
     *
     * @param username The username identifying the user to be loaded.
     * @return [UserDetails] The user details required by Spring Security.
     * @throws UsernameNotFoundException if the username is not found in the database.
     */
    override fun loadUserByUsername(username: String): UserDetails {
        val user = userService.findByLogin(username) ?: throw UsernameNotFoundException("User does not exist")
        return SecurityUser.fromUser(user)
    }
}