package cloud.dmytrominochkin.spring.security.svc

import cloud.dmytrominochkin.spring.security.dto.UserRequest
import cloud.dmytrominochkin.spring.security.dto.UserResponse
import cloud.dmytrominochkin.spring.security.entity.CustomUser

/**
 * Defines the service layer interface for managing user entities.
 * This interface provides methods to perform various operations involving user details,
 * such as saving a user and loading user details.
 */
interface UserService {
    /**
     * Saves a new user to the repository.
     *
     * @param userRequest The user data to save.
     * @return True if the user was saved successfully.
     */
    fun saveUser(userRequest: UserRequest): Boolean

    /**
     * Loads a user by username. This method is protected by Spring Security
     *
     * @param username The username to search for.
     * @return The found user, or null if no user is found.
     */
    fun loadUser(username: String): CustomUser?

    /**
     * Finds a user by login.
     *
     * @param login The login to search for.
     * @return The found user, or null if no user is found.
     */

    fun findByLogin(login: String): CustomUser?
}