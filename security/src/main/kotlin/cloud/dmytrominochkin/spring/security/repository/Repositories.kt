package cloud.dmytrominochkin.spring.security.repository

import cloud.dmytrominochkin.spring.security.entity.CustomRole
import cloud.dmytrominochkin.spring.security.entity.CustomUser
import org.springframework.data.jpa.repository.JpaRepository

/**
 * Spring Data repository interface for [CustomRole] entities.
 */
interface RoleRepository : JpaRepository<CustomRole, Long> {
    fun findByName(name: String): CustomRole?
}

/**
 * Spring Data repository interface for [CustomUser] entities.
 */
interface UserRepository : JpaRepository<CustomUser, Long> {
    fun findByLogin(login: String): CustomUser?
}