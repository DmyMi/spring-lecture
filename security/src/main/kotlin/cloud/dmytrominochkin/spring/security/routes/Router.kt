package cloud.dmytrominochkin.spring.security.routes

import org.springframework.web.servlet.function.router

/**
 * Provides a router function configured with routes for handling HTTP GET requests.
 * This functional approach to routing in Spring MVC uses the [router] DSL to define how requests to certain paths
 * should be handled.
 *
 * The router function here manages two simple routes:
 * - "/login": A route to render a login page.
 * - "/": The root route which renders the index page.
 *
 * This property uses lazy initialization to ensure the router configuration is only set up once when first accessed.
 *
 * @return A [org.springframework.web.servlet.function.RouterFunction] which is a part of Spring's functional
 *         web framework that maps incoming requests to handler functions.
 */
val functionalRouter
    get() = router {
        // Defines a GET route for the login page.
        GET("/login") {
            // Responds with an OK status and renders the "login" view template.
            ok().render("login")
        }
        // Defines a GET route for the index page.
        GET("/") {
            // Responds with an OK status and renders the "index" view template.
            ok().render("index")
        }
    }
