package cloud.dmytrominochkin.spring.security.controller

import cloud.dmytrominochkin.spring.security.dto.UserRequest
import cloud.dmytrominochkin.spring.security.dto.UserResponse
import cloud.dmytrominochkin.spring.security.svc.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

/**
 * A REST controller for managing user-related actions such as signing up and retrieving user profiles.
 * This controller handles HTTP requests and interacts with the [UserService] to perform business logic.
 *
 * @property userService The service that handles business logic for user operations.
 */
@RestController
class UserController @Autowired constructor(
    private val userService: UserService
) {
    /**
     * Handles POST requests to create a new user account.
     *
     * This method receives a [UserRequest] object encapsulated in the request body, which contains user data
     * necessary for registration. It delegates to [UserService] to save the user and returns a ResponseEntity
     * indicating the operation's success or failure.
     *
     * @param userRequest The user data for creating a new account.
     * @return A [ResponseEntity] containing a success status if the user is successfully created.
     */
    @PostMapping("/signup")
    fun signUp(
        @RequestBody userRequest: UserRequest
    ): ResponseEntity<*> = ResponseEntity.ok(userService.saveUser(userRequest))

    /**
     * Retrieves the user profile for a given username.
     *
     * This endpoint is accessible to general users and is intended to retrieve the profile of the
     * user whose username is specified in the path variable. It utilizes a private helper method to
     * handle the logic of fetching and building the user response.
     *
     * @param username The username of the user whose profile is requested.
     * @return A [ResponseEntity] containing the user's data or a not-found status if the user does not exist.
     */
    @GetMapping("/user/{username}")
    fun userProfile(@PathVariable username: String): ResponseEntity<UserResponse> = getUserResponse(username)

    /**
     * Retrieves the user profile for administrative purposes.
     *
     * Similar to the [userProfile] method, but typically this endpoint might be secured with additional
     * authorization checks to ensure that only administrative users can access it. It also delegates
     * to the private [getUserResponse] method.
     *
     * @param username The username of the user whose profile is requested for administrative viewing.
     * @return A [ResponseEntity] with the user's data or a not-found status.
     */
    @GetMapping("/admin/{username}")
    fun adminProfile(@PathVariable username: String): ResponseEntity<UserResponse> = getUserResponse(username)

    /**
     * A private helper method to fetch and format the user response based on the username.
     *
     * This method centralizes the common logic used by both user and admin profile retrieval endpoints.
     * It fetches the user from the [userService], and if the user is found, it formats and returns the user data;
     * otherwise, it returns a [ResponseEntity] indicating that the user was not found.
     *
     * @param username The username to look up in the [userService].
     * @return A [ResponseEntity] containing either the formatted user data or a not-found status.
     */
    private fun getUserResponse(username: String): ResponseEntity<UserResponse> {
        val user = userService.loadUser(username)
        if (user != null) {
            return ResponseEntity.ok(UserResponse(user.login, user.role!!.name))
        }
        return ResponseEntity.notFound().build()
    }
}