package cloud.dmytrominochkin.spring.security.dto

data class RoleResponse(
    val rolename: String
)
