package cloud.dmytrominochkin.spring.jwt.config

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

/**
 * Configuration class for Spring MVC, enabling MVC support.
 *
 * This class implements the [WebMvcConfigurer] interface to customize the configuration of Spring MVC.
 */
@Configuration
@ComponentScan(basePackages = ["cloud.dmytrominochkin.spring.jwt"])
@EnableWebMvc
class WebConfig : WebMvcConfigurer