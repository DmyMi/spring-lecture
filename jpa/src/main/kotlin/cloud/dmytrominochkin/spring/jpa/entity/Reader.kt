package cloud.dmytrominochkin.spring.jpa.entity

import jakarta.persistence.*
import org.hibernate.Hibernate


/**
 * An entity class representing the Reader table in the database, including relationships to other entities.
 *
 * JPA Annotations Explained:
 * - [Entity]: Marks this class as a JPA entity to be mapped to a database table.
 * - [Table]: Specifies the table in the database to which this entity is mapped. The `name` attribute defines the table name.
 * - [Column]: Specifies the column that a field is mapped to in the database. The name attribute indicates the column name.
 * - [Id]: Marks a field as the primary key of the entity.
 * - [GeneratedValue]: Specifies the strategy for generating primary key values.
 *  `GenerationType.AUTO` lets the persistence provider choose the generation strategy.
 * - [OneToMany] and [ManyToOne]: Define the relationships between entities.
 *  `mappedBy` indicates the field that owns the relationship.
 *
 * @property name The name of the reader.
 * @property using A set of Using entities representing the usage records associated with the reader.
 * @property idReader The unique identifier (primary key) of the reader.
 */
@Entity
@Table(name = "reader")
class Reader(
    val name: String,
    @OneToMany(mappedBy = "reader")
    val using: MutableSet<Using> = mutableSetOf(),
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idReader")
    val idReader: Long = 0
) {

    /**
     * Custom implementation of the [equals] method for the Reader entity.
     *
     * Explanation and Best Practices:
     * - The purpose of overriding the [equals] method in JPA entity classes is to provide
     *   a meaningful way of comparing entity instances beyond the default reference equality.
     * - This implementation first checks if the compared object is the same instance for quick reference equality.
     * - It then checks if the other object is null or not of the same Hibernate proxy class. This is crucial
     *   because Hibernate often wraps entities in proxies, and direct class comparisons might fail.
     * - The actual comparison uses the unique identifier ([idReader]), which is a common practice. However,
     *   relying solely on database-generated IDs can be problematic for new (unsaved) entities because they
     *   don't have an ID until persisted. Therefore, it's sometimes recommended to include business keys
     *   in equality checks.
     * - Using `Hibernate.getClass(this)` instead of `this::class.java` ensures the real class of the entity
     *   is used for comparison, accounting for possible proxy objects.
     *
     * Note:
     * - When using entities in collections (like `Set` or `Map`), ensure that `hashCode` is consistent with `equals`.
     * - Entities should not change their equality state while part of a collection, as this can lead to inconsistent
     *   collection behavior.
     *
     * @param other The reference object with which to compare.
     * @return `true` if this object is the same as the obj argument; `false` otherwise.
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Reader
        return idReader != 0L && idReader == other.idReader
    }

    /**
     * Provides a hash code value for the entity. This method is overridden to ensure that
     * the hash code is consistent with the `equals` method implementation.
     *
     * Explanation and Best Practices:
     * - The `hashCode` method is crucial for entities that will be used in hash-based collections like `HashSet` or `HashMap`.
     * - Good choice is ot write a hash code that is derived from the entity's unique identifier. This choice
     *   is aligned with the `equals` method, which bases equality on the same identifier.
     * - It's essential for the `hashCode` method to return consistent values for the same entity instance and to ensure
     *   that if two entities are equal according to the `equals` method, they must also have the same hash code.
     * - However, relying on a database-generated ID can be problematic because the entity might not have an ID until it's persisted.
     *   Therefore, entities not yet persisted could potentially cause inconsistencies in hash-based collections.
     * - A common strategy to mitigate this issue is to include a combination of several stable attribute values (forming a "business key")
     *   in the hash code computation.
     * - Note that using mutable fields in `hashCode` (and `equals`) can lead to inconsistent behavior when the entity is stored
     *   in a collection. Hence, it's typically recommended to use immutable fields for hash code calculations, or at least ensure
     *   the fields used do not change while the entity is in a collection.
     *
     * Implementation Note:
     * - This simplistic approach uses `javaClass.hashCode()` as a base to ensure that the hash code remains consistent with
     *   the `equals` method which also considers the entity's class type. However, in real-world applications, you might
     *   need a more sophisticated strategy that accounts for the entity's state.
     *
     * @return A hash code value for this object.
     */
    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(idReader = $idReader , name = $name )"
    }
}

/**
 * Extension function that provides a copy mechanism for the [Reader] entity.
 *
 * Immutability in JPA Entities:
 * - Immutable entities help maintain data consistency and prevent bugs related to entity state changes.
 * - When entities are shared between different parts of the application or stored in collections,
 *   immutability ensures their state remains consistent throughout their lifecycle.
 * - Instead of modifying an existing entity, creating a new copy with updated values is safer
 *   as it prevents unintended side effects in the persistence context or cached references.
 *
 * This copy function follows Kotlin's data class copy() pattern but for a regular class:
 * - All parameters are optional and default to the current values
 * - Returns a new instance with the specified fields updated
 * - Maintains immutability by creating a new instance instead of modifying the existing one
 *
 * @param name Optional new name for the reader copy
 * @param using Optional new set of Using records for the reader copy
 * @param idReader Optional new ID for the reader copy
 * @return A new [Reader] instance with the specified fields updated
 */
fun Reader.copy(
    name: String = this.name,
    using: MutableSet<Using> = this.using,
    idReader: Long = this.idReader
): Reader = Reader(name, using, idReader)
