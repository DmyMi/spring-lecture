package cloud.dmytrominochkin.spring.jpa.session.dao

/**
 * Generic interface for data access operations on an entity.
 *
 * @param E The type of the entity this DAO manages.
 */
interface EntityDAO<E> {

    /**
     * Saves the given entity in the database.
     * @param element The entity to be saved.
     */
    fun save(element: E)

    /**
     * Updates the given entity in the database and returns the updated entity.
     * @param element The entity to be updated.
     * @return The updated entity.
     */
    fun update(element: E): E

    /**
     * Finds an entity by its ID.
     * @param elementId The ID of the entity to find.
     * @return The found entity or `null` if no entity with the given ID was found.
     */
    fun findById(elementId: Long): E?

    /**
     * Retrieves all entities of type [E].
     * @return A list of all entities of type [E].
     */
    val all: List<E>

    /**
     * Deletes the given entity from the database.
     * @param element The entity to be deleted.
     */
    fun delete(element: E)
}