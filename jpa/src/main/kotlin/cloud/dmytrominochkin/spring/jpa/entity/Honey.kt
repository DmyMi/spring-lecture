package cloud.dmytrominochkin.spring.jpa.entity

import jakarta.persistence.*

/**
 * An entity class representing the Honey table in the database.
 *
 * JPA Annotations Explained:
 * - [Entity]: Marks this class as a JPA entity to be mapped to a database table.
 * - [Table]: Specifies the table in the database to which this entity is mapped. The [Table.name] attribute defines the table name.
 * - [Column]: Specifies the column that a field is mapped to in the database. The name attribute indicates the column name.
 * - [Id]: Marks a field as the primary key of the entity.
 * - [GeneratedValue]: Specifies the strategy for generating primary key values.
 *  [GenerationType.AUTO] lets the persistence provider choose the generation strategy.
 *
 * @property name The name of the honey.
 * @property taste The taste description of the honey.
 * @property id The unique identifier (primary key) of the honey.
 */
@Entity
@Table(name = "honey")
data class Honey(
    @Column(name = "name")
    var name: String,
    @Column(name = "taste")
    var taste: String,
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    var id: Int = 0,
)