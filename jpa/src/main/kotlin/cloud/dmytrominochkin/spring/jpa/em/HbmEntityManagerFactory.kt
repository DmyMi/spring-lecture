package cloud.dmytrominochkin.spring.jpa.em

import jakarta.persistence.Persistence
import org.hibernate.cfg.AvailableSettings
import org.hibernate.tool.schema.Action

/**
 * Singleton object responsible for creating and providing an instance of [jakarta.persistence.EntityManagerFactory].
 * It provides a global point of access to the [jakarta.persistence.EntityManagerFactory].
 * This pattern ensures that the application uses a single instance of [jakarta.persistence.EntityManagerFactory], which is
 * recommended for performance and resource management.
 *
 * Overview
 * [jakarta.persistence.EntityManagerFactory] is a fundamental class in JPA (Jakarta Persistence API) that creates
 * [jakarta.persistence.EntityManager] instances. [jakarta.persistence.EntityManager] instances are used to interact with the persistence
 * context and perform CRUD operations on entities mapped to a database.
 *
 * Details
 * - The factory is created by calling [Persistence.createEntityManagerFactory] with the persistence
 *   unit name and a map of properties. The persistence unit name ("honey" in this case) refers to a
 *   specific set of entity mappings and JPA settings defined in `persistence.xml` or equivalent configuration.
 *
 * Custom Configuration
 * - This example demonstrates how to pass additional properties programmatically at runtime. It uses
 *   [AvailableSettings.JAKARTA_HBM2DDL_DATABASE_ACTION] to specify the schema generation action [Action.CREATE].
 *   This tells Hibernate, the JPA provider, to create the database schema based on the entity mappings upon
 *   the [jakarta.persistence.EntityManagerFactory] initialization.
 *
 * Key Classes and Interfaces:
 * - [Persistence]: A bootstrap class in JPA that provides static methods to obtain [jakarta.persistence.EntityManagerFactory]
 *   instances based on persistence unit configuration.
 * - [jakarta.persistence.EntityManagerFactory]: An interface to the factory that creates [jakarta.persistence.EntityManager] instances. Once created,
 *   it's a heavyweight object that should be kept around and reused.
 * - [AvailableSettings]: A Hibernate-specific class that contains the keys of standard properties and
 *   Hibernate-specific settings that can be passed to the JPA provider.
 * - [Action]: Enum provided by Hibernate to define actions for schema generation tooling.
 *
 * Best Practices:
 * - It's crucial to properly manage the lifecycle of [jakarta.persistence.EntityManagerFactory] and [jakarta.persistence.EntityManager] instances.
 *   [jakarta.persistence.EntityManagerFactory] should be created at application startup and closed when the application stops.
 *   [jakarta.persistence.EntityManager] instances should be created and closed within the scope of a transaction.
 *
 * @property factory The [jakarta.persistence.EntityManagerFactory] instance created for the "honey" persistence unit with
 * specific Hibernate settings.
 */
object HbmEntityManagerFactory {
    val factory = Persistence.createEntityManagerFactory(
        "honey",
        mapOf(AvailableSettings.JAKARTA_HBM2DDL_DATABASE_ACTION to Action.CREATE)
    )
}