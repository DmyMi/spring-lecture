package cloud.dmytrominochkin.spring.jpa.session

import cloud.dmytrominochkin.spring.jpa.entity.Book
import cloud.dmytrominochkin.spring.jpa.entity.Reader
import cloud.dmytrominochkin.spring.jpa.entity.Using
import cloud.dmytrominochkin.spring.jpa.entity.copy
import cloud.dmytrominochkin.spring.jpa.session.dao.BookDAO
import cloud.dmytrominochkin.spring.jpa.session.dao.ReaderDAO
import cloud.dmytrominochkin.spring.jpa.session.dao.UsingDAO
import java.sql.Date

/**
 * Demonstrates the use of the DAO (Data Access Object) pattern to perform database operations on entities such as [Book], [Reader], and [Using].
 * The DAO pattern provides a layer of abstraction between the application's business logic and the database access logic.
 *
 * Benefits of Using the DAO Pattern:
 * - Abstraction: Hides the complexity of the database operations from the business logic. The business layer interacts with the DAO layer,
 *   which in turn handles all the low-level database operations.
 * - Decoupling: Reduces the coupling between different parts of the application. Changes to the database access logic do not affect the
 *   business logic, making the application more maintainable and scalable.
 * - Reusability: Encourages reusing the DAO classes across different parts of the application without duplicating code.
 * - Testability: Facilitates unit testing of the business logic by mocking the DAOs, thereby not requiring an actual database for testing.
 * - Consistency: Ensures consistent access to the data source, as all database access logic goes through the DAO layer. This centralizes
 *   the handling of connections, transactions, and exception management.
 *
 * This example creates instances of [Book], [Reader], and [Using] entities, simulating a simple library system where books can be lent out
 * to readers. It showcases how entities are persisted, updated, and deleted through their respective DAOs ([BookDAO], [ReaderDAO], [UsingDAO]).
 * It demonstrates listing all books and their associated usage records after performing various operations.
 */
fun main() {
    // Entity instantiation and associations
    val book1 = Book(title = "Title_1")
    var book2 = Book(title = "Title_2")
    var book3 = Book(title = "Title_3")

    val reader1 = Reader(name = "Name_1")
    val reader2 = Reader(name = "Name_2")

    var using1 = Using(dateReturn = Date(System.currentTimeMillis() + 60 * 60 * 24 * 1000 * 4))
    using1 = using1.copy(book = book1, reader = reader1)

    var using2 = Using(dateReturn = Date(System.currentTimeMillis() + 60 * 60 * 24 * 1000 * 5))
    using2 = using2.copy(book = book2, reader = reader1)

    var using3 = Using(dateReturn = Date(System.currentTimeMillis() + 60 * 60 * 24 * 1000 * 6))
    using3 = using3.copy(book = book3, reader = reader2)

    // DAO operations
    val bd = BookDAO()
    val rd = ReaderDAO()
    val ud = UsingDAO()

    bd.save(book1)
    bd.save(book2)
    bd.save(book3)

    rd.save(reader1)
    rd.save(reader2)

    ud.save(using1)
    ud.save(using2)
    ud.save(using3)

    // Demonstrate fetch and update operations
    println("book1: id=${book1.idBook} Title=${book1.title}")
    println("book2: id=${book2.idBook} Title=${book2.title}")
    println("book3: id=${book3.idBook} Title=${book3.title}")

    book2 = bd.findById(2)!!
    book3 = bd.findById(3)!!

    book3 = book3.copy(title = "abcde")

    bd.update(book3)
    bd.delete(book2)

    // Listing all books to see the effects of CRUD operations
    println("\nAll Books:")
    bd.all.forEach { b ->
        println("book: id=${b.idBook} Title=${b.title}")
        b.using.forEach { u ->
            println("\tDate:" + u.dateReturn)
        }
    }
}