package cloud.dmytrominochkin.spring.jpa.em

import cloud.dmytrominochkin.spring.jpa.entity.Honey
import cloud.dmytrominochkin.spring.jpa.entity.Honey_
import jakarta.persistence.EntityManager
import jakarta.persistence.EntityManagerFactory

/**
 * Demonstrates basic operations using JPA (Jakarta Persistence API), including creating, updating, and listing entities.
 * Utilizes [EntityManagerFactory] and [EntityManager] to manage persistence context and entity transactions.
 */
fun main() {
    // Instantiate a new Honey entity
    val honey = Honey(name = "favourite white", taste = "very sweet")
    // Persist the new entity to the database
    createHoney(honey)
    println("Primary key is ${honey.id}")

    // List all Honey entities from the database
    listHoney()

    // Update the existing Honey entity
    honey.name = "greece forest"
    honey.taste = "very good"
    updateHoney(honey)

    // List all Honey entities again to see the update effect
    listHoney()

    // Find Honey be id with Criteria Query
    findHoneyByIdCriteria(1)?.run {
        println(name)
    }
}

/**
 * Persists a new Honey entity into the database.
 *
 * JPA Features:
 * - [EntityManager.persist]: Makes a new instance managed and persistent by adding it to the persistence context.
 *
 * @param honey The Honey entity to be persisted.
 */
private fun createHoney(honey: Honey) {
    println("creating honey")
    HbmEntityManagerFactory.factory.inTransaction { em ->
        em.persist(honey)
    }
}

/**
 * Updates an existing Honey entity in the database. Implementation is similar to how [EntityManager.merge] operates.
 *
 * JPA Features:
 * - [EntityManager.find]: Finds an entity by its primary key. If the entity instance is contained in the current
 *   persistence context, it returns that instance, otherwise, it loads an instance from the database.
 * - [EntityManager.flush]: Synchronizes the persistence context to the underlying database immediately.
 *
 * @param honey The Honey entity with updated fields to be synchronized with the database.
 */
private fun updateHoney(honey: Honey) {
    println("updating honey")
    HbmEntityManagerFactory.factory.inTransaction { em ->
        val dbHoney = em.find(
            Honey::class.java,
            honey.id
        )
        dbHoney.name = honey.name
        dbHoney.taste = honey.taste
        em.flush()
    }
}

/**
 * Lists all Honey entities stored in the database.
 *
 * JPA Features:
 * - [EntityManager.createQuery]: Used to create a TypedQuery to select entities according to criteria.
 * - [jakarta.persistence.TypedQuery.getResultList]: Executes the query and returns the results as a list.
 */
private fun listHoney() {
    println("listing honey")
    HbmEntityManagerFactory.factory.inTransaction { em ->
        val honeys = em.createQuery("from Honey", Honey::class.java).resultList
        honeys.forEach(::println)
    }
}

/**
 * Finds a Honey entity by its ID using JPA Criteria API.
 *
 * JPA Features:
 * - [EntityManager.getCriteriaBuilder]: Creates a criteria builder for constructing type-safe queries.
 * - [jakarta.persistence.criteria.CriteriaBuilder.createQuery]: Creates a criteria query object for the entity type.
 * - [jakarta.persistence.criteria.CriteriaQuery.from]: Defines the FROM clause of the query, specifying the entity root.
 * - [jakarta.persistence.criteria.Root.get]: Accesses entity attributes in a type-safe manner using the generated metamodel.
 *
 * The method demonstrates the use of the JPA Metamodel (Honey_) for type-safe criteria queries,
 * which helps prevent runtime errors by catching invalid property references at compile time.
 *
 * @param elementId The primary key of the Honey entity to find.
 * @return The found Honey entity or null if not found.
 */
private fun findHoneyByIdCriteria(elementId: Long): Honey? {
    println("finding honey by id $elementId")
    return HbmEntityManagerFactory.factory.inTransaction { em ->
        val builder = em.criteriaBuilder
        val criteria = builder.createQuery(Honey::class.java)
        val elementRoot = criteria.from(Honey::class.java)
        criteria.select(elementRoot)
        criteria.where(
            builder.equal(
                elementRoot.get(Honey_.id),
                elementId
            )
        )
        em.createQuery(criteria).resultList.singleOrNull()
    }
}

/**
 * Extension function for [EntityManagerFactory] to simplify transaction management.
 *
 * This function abstracts the boilerplate code for transaction management, making it easier to execute
 * a block of operations within a transactional context. It ensures that each operation is executed within
 * its own [EntityManager] scope, with transaction begin and commit/rollback handled automatically.
 *
 * @param work A lambda function representing the work to be done within the transaction.
 */
private fun <T> EntityManagerFactory.inTransaction(work: (em: EntityManager) -> T): T? {
    return createEntityManager().use { entityManager ->
        val transaction = entityManager.transaction

        try {
            transaction.begin()
            val result = work(entityManager)
            transaction.commit()
            return@use result
        } catch (e: Exception) {
            if (transaction.isActive) transaction.rollback()
            println(e.message)
            return@use null
        }
    }
}