package cloud.dmytrominochkin.spring.jpa.session.hibernate

import cloud.dmytrominochkin.spring.jpa.entity.Book
import cloud.dmytrominochkin.spring.jpa.entity.Honey
import cloud.dmytrominochkin.spring.jpa.entity.Reader
import cloud.dmytrominochkin.spring.jpa.entity.Using
import org.hibernate.SessionFactory

/**
 * Configures and builds a [SessionFactory] using Hibernate's XML configuration approach.
 *
 * Explanation:
 * - [SessionFactory] is a thread-safe, immutable cache of compiled mappings for a single database. A factory for [org.hibernate.Session] instances.
 * - This function leverages Hibernate's [org.hibernate.cfg.Configuration] class to load Hibernate's configuration file (hibernate.cfg.xml by default),
 *   and then builds a [SessionFactory].
 * - The configuration file typically includes database connection settings, dialect information, and mapping file locations.
 *
 * @return A [SessionFactory] instance configured based on hibernate.cfg.xml or other specified configuration file.
 */
fun xmlConfig(): SessionFactory =
    org.hibernate.cfg.Configuration()
        .configure()
        .buildSessionFactory()

fun kotlinConfig(): SessionFactory =
    org.hibernate.cfg.Configuration()
        .apply {
            setProperty(
                "hibernate.connection.driver_class",
                "org.hsqldb.jdbcDriver"
            )
            setProperty(
                "hibernate.connection.url",
                "jdbc:hsqldb:mem:hibernateexample"
            )
            setProperty("hibernate.connection.username", "sa")
            setProperty("hibernate.connection.password", "1")
            setProperty("hibernate.connection.pool_size", "1")
            setProperty("hibernate.connection.autocommit", "false")
            setProperty(
                "hibernate.cache.provider_class",
                "org.hibernate.cache.NoCacheProvider"
            )
            setProperty("hibernate.cache.use_second_level_cache", "false")
            setProperty("hibernate.cache.use_query_cache", "false")
            setProperty(
                "hibernate.dialect",
                "org.hibernate.dialect.HSQLDialect"
            )
            setProperty("hibernate.show_sql", "true")
            setProperty("hibernate.current_session_context_class", "thread")
            setProperty("hibernate.archive.autodetection", "class,hbm")
            addAnnotatedClass(Honey::class.java)
            addAnnotatedClass(Book::class.java)
            addAnnotatedClass(Reader::class.java)
            addAnnotatedClass(Using::class.java)
        }
        .configure()
        .buildSessionFactory()
