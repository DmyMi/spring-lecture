package cloud.dmytrominochkin.spring.jpa.session.dao

import cloud.dmytrominochkin.spring.jpa.entity.Book
import cloud.dmytrominochkin.spring.jpa.entity.Reader
import cloud.dmytrominochkin.spring.jpa.entity.Using

/**
 * DAO for managing [Book] entities.
 */
class BookDAO : EntityDAOImpl<Book>(Book::class.java)

/**
 * DAO for managing [Reader] entities.
 */
class ReaderDAO : EntityDAOImpl<Reader>(Reader::class.java)

/**
 * DAO for managing [Using] entities.
 */
class UsingDAO : EntityDAOImpl<Using>(Using::class.java)