package cloud.dmytrominochkin.spring.jpa.session.dao

import cloud.dmytrominochkin.spring.jpa.session.hibernate.HibernateSessionFactory
import org.hibernate.Session


/**
 * Implementation of the [EntityDAO] interface, providing concrete data access operations.
 *
 * @param elementClass The class of the entity type [E], needed for dynamic query generation and criteria building.
 * @param E The type of the entity this DAO manages.
 */
open class EntityDAOImpl<E>(private val elementClass: Class<E>) : EntityDAO<E> {

    /**
     * Executes a database operation within a session and transaction. This is a higher-order function that
     * takes another function as a parameter to execute specific entity operations within a transactional context.
     *
     * Details:
     * - Opens a [Session] from the [org.hibernate.SessionFactory], automatically closing it to prevent resource leaks.
     * - Begins a transaction at the start of the session. If the operation completes successfully, the transaction
     *   is committed. If there's an exception, the transaction is rolled back (if applicable).
     * - This pattern ensures that database operations are executed within a transactional boundary, promoting
     *   data integrity and consistency.
     *
     * @param task A function that contains the database operation to be executed. It receives an open [Session] as a parameter.
     * @return The result of the executed task.
     */
    private inline fun <T> executeInSession(task: (session: Session) -> T): T =
        HibernateSessionFactory.sessionFactory.currentSession.use { session ->
            session.beginTransaction()

            val result = task(session)

            session.transaction.commit()
            result
        }

    override fun save(element: E) =
        executeInSession { session -> session.persist(element) }

    override fun update(element: E): E =
        executeInSession { session -> session.merge(element) }

    override fun findById(elementId: Long): E? =
        executeInSession { session -> session.find(elementClass, elementId) }

    /**
     * Implementation Details:
     * - The JPA Criteria API is used here to programmatically construct a query to retrieve all entities of type [E]. This approach
     *   is type-safe, making it less error-prone compared to string-based JPQL or native SQL queries.
     * - Begins by obtaining a [jakarta.persistence.criteria.CriteriaBuilder] instance from the current session, which is used to construct the [jakarta.persistence.criteria.CriteriaQuery] object.
     *   The [jakarta.persistence.criteria.CriteriaBuilder] is a factory for several criteria-related objects.
     * - A [jakarta.persistence.criteria.CriteriaQuery] instance is created for the entity type [E] ([elementClass]). This query object defines the query to be executed.
     * - The [jakarta.persistence.criteria.CriteriaQuery.from] method of [jakarta.persistence.criteria.CriteriaQuery] is used to set the root of the query
     *   (i.e., the FROM clause) to the entity type [E]. This specifies the query's target entity.
     * - The [jakarta.persistence.criteria.CriteriaQuery.select] method of [jakarta.persistence.criteria.CriteriaQuery] is then used to select the root,
     *   effectively creating a SELECT query that retrieves all columns of the entity.
     * - Finally, the query is executed using the [Session.createQuery] method of the [Session], and [org.hibernate.query.Query.getResultList] is called to fetch and return
     *   the list of entities.
     * - This pattern encapsulates the query construction and execution in a transactional boundary, ensuring consistency and integrity of
     *   the operation.
     *
     * Advantages of Using Criteria API:
     * - Type Safety: Reduces runtime errors by catching issues at compile-time.
     * - Dynamic Query Construction: Facilitates building complex queries programmatically, making it ideal for scenarios where query
     *   specifications are determined at runtime.
     * - Readability: Although more verbose, it can be more readable and maintainable compared to string-based queries, especially for
     *   complex queries.
     *
     * @return A list of all entities of type [E].
     */
    override val all: List<E>
        get() {
            return executeInSession { session ->
                val builder = session.criteriaBuilder
                val criteria = builder.createQuery(elementClass)
                val elementRoot = criteria.from(elementClass)
                criteria.select(elementRoot)
                session.createQuery(criteria).resultList
            }
        }

    override fun delete(element: E) =
        executeInSession { session -> session.remove(element) }
}