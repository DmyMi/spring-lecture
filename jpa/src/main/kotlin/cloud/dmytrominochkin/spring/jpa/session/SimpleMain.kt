package cloud.dmytrominochkin.spring.jpa.session

import cloud.dmytrominochkin.spring.jpa.entity.Honey
import cloud.dmytrominochkin.spring.jpa.entity.Honey_
import cloud.dmytrominochkin.spring.jpa.session.hibernate.HibernateSessionFactory


/**
 * Demonstrates basic Hibernate operations including create, update, and list entities.
 * Utilizes the [org.hibernate.SessionFactory] and [org.hibernate.Session] from Hibernate to manage the persistence context
 * and perform transactions.
 */
fun main() {
    // Creating a new Honey instance
    val honey = Honey(name = "favourite white", taste = "very sweet")
    createHoney(honey)
    println("primary key is ${honey.id}")

    // Listing all Honey instances
    listHoney()

    // Updating the Honey instance
    honey.name = "greece forest"
    honey.taste = "very good"
    updateHoney(honey)

    // Listing all Honey instances to see the update effect
    listHoney()

    // Find Honey be id with Criteria Query
    findHoneyByIdCriteria(1)?.run {
        println(name)
    }
}

/**
 * Saves a Honey entity to the database.
 *
 * Hibernate Features:
 * - [org.hibernate.SessionFactory.inTransaction]: A convenience method that encapsulates the operation within a transaction.
 *   It automatically begins and commits the transaction, or rolls it back if an exception occurs.
 * - [org.hibernate.Session.persist]: This method makes a transient instance persistent by adding it to the persistence context.
 *
 * @param honey The Honey entity to be persisted.
 */
private fun createHoney(honey: Honey) {
    println("creating honey")
    HibernateSessionFactory.sessionFactory.inTransaction { session ->
        session.persist(honey)
    }
}

/**
 * Updates an existing Honey entity in the database. Implementation is similar to how [org.hibernate.Session.merge] operates.
 *
 * Hibernate Features:
 * - [org.hibernate.Session.find]: Retrieves an entity by its primary key. If the entity instance is contained in the current
 *   persistence context, it returns that instance, otherwise, it loads an instance from the database.
 * - [org.hibernate.Session.flush]: Synchronizes the persistence context to the underlying database.
 *
 * @param honey The Honey entity with updated fields to be synchronized with the database.
 */
private fun updateHoney(honey: Honey) {
    println("updating honey")
    HibernateSessionFactory.sessionFactory.inTransaction { session ->
        val dbHoney = session.find(
            Honey::class.java,
            honey.id
        )
        dbHoney.name = honey.name
        dbHoney.taste = honey.taste
        session.flush()
    }
}

/**
 * Lists all Honey entities from the database.
 *
 * Hibernate Features:
 * - [org.hibernate.Session.createQuery]: Creates a [org.hibernate.query.Query] object to retrieve entities by the HQL (Hibernate Query Language).
 * - [org.hibernate.query.Query.list]: Executes the query and returns the results as a list.
 */
private fun listHoney() {
    println("listing honey")
    HibernateSessionFactory.sessionFactory.inSession { session ->
        val honeys = session.createQuery("from Honey", Honey::class.java).list()
        honeys.forEach(::println)
    }
}

/**
 * Finds a Honey entity by its ID using Hibernate Criteria API.
 *
 * Hibernate Features:
 * - [org.hibernate.Session.getCriteriaBuilder]: Creates a criteria builder for constructing type-safe queries.
 * - [javax.persistence.criteria.CriteriaBuilder.createQuery]: Creates a criteria query object for the entity type.
 * - [javax.persistence.criteria.CriteriaQuery.from]: Defines the FROM clause of the query, specifying the entity root.
 * - [javax.persistence.criteria.Root.get]: Accesses entity attributes in a type-safe manner using the generated metamodel.
 *
 * @param elementId The primary key of the Honey entity to find.
 * @return The found Honey entity or null if not found.
 */
private fun findHoneyByIdCriteria(elementId: Long): Honey? {
    println("finding honey by id $elementId")
    return HibernateSessionFactory.sessionFactory.fromSession { session ->
        val builder = session.criteriaBuilder
        val criteria = builder.createQuery(Honey::class.java)
        val elementRoot = criteria.from(Honey::class.java)
        criteria.select(elementRoot)
        criteria.where(
            builder.equal(
                elementRoot.get(Honey_.id),
                elementId
            )
        )
        return@fromSession session.createQuery(criteria).resultList.singleOrNull()
    }
}