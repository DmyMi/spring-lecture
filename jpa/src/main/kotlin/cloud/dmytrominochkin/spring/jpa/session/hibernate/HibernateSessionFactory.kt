package cloud.dmytrominochkin.spring.jpa.session.hibernate

import org.hibernate.SessionFactory

/**
 * Singleton object to hold the application-wide [SessionFactory].
 *
 * Usage:
 * - Provides a centralized and accessible point for obtaining a [SessionFactory] instance.
 * - Ensures that only one [SessionFactory] is instantiated and used throughout the application lifecycle, which is essential for
 *   efficient resource management.
 */
object HibernateSessionFactory {
    val sessionFactory = xmlConfig()
}