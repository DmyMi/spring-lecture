plugins {
    kotlin("jvm")
    // The Spring plugin automatically applies both noArg and allOpen configurations for typical Spring annotations,
    // facilitating the integration with Spring Data, which also benefits from these adjustments for proxying and reflection.
    id("org.jetbrains.kotlin.plugin.spring")
    application
}

group = "cloud.dmytrominochkin.spring"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    compileOnly(libs.jakarta.servlet)
    implementation(libs.jetty.webapp)
    implementation(libs.bundles.logging)

    implementation(libs.spring.mvc)
    implementation(libs.spring.thymeleaf.core)
    implementation(libs.spring.thymeleaf.security)

    implementation(libs.kotlin.reflect)

    // Spring Security & OAuth2
    // Spring Security Config/Web, Resource Server and JWT libraries
    // are part of Authorization Server library
    implementation(libs.spring.security.sts)
    implementation(libs.spring.security.client)

    // Serialization
    implementation(libs.bundles.jackson)

    testImplementation(libs.kotlin.test)
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("-parameters")
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(17)
}

application {
    mainClass.set("cloud.dmytrominochkin.spring.oauth2.OAuthMainKt")
}
