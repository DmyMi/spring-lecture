package cloud.dmytrominochkin.spring.oauth2.sts.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

/**
 * Configuration class for Spring MVC.
 *
 * This class implements the [WebMvcConfigurer] interface to customize the configuration of Spring MVC.
 */
@Configuration
@EnableWebMvc
class WebConfig : WebMvcConfigurer