package cloud.dmytrominochkin.spring.oauth2.sts.config

import com.nimbusds.jose.jwk.JWKSet
import com.nimbusds.jose.jwk.RSAKey
import com.nimbusds.jose.jwk.source.ImmutableJWKSet
import com.nimbusds.jose.jwk.source.JWKSource
import com.nimbusds.jose.proc.SecurityContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.http.MediaType
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.oauth2.core.AuthorizationGrantType
import org.springframework.security.oauth2.core.ClientAuthenticationMethod
import org.springframework.security.oauth2.core.oidc.OidcScopes
import org.springframework.security.oauth2.jwt.JwtDecoder
import org.springframework.security.oauth2.server.authorization.OAuth2TokenType
import org.springframework.security.oauth2.server.authorization.client.InMemoryRegisteredClientRepository
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository
import org.springframework.security.oauth2.server.authorization.config.annotation.web.configuration.OAuth2AuthorizationServerConfiguration
import org.springframework.security.oauth2.server.authorization.config.annotation.web.configurers.OAuth2AuthorizationServerConfigurer
import org.springframework.security.oauth2.server.authorization.settings.AuthorizationServerSettings
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings
import org.springframework.security.oauth2.server.authorization.token.JwtEncodingContext
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenCustomizer
import org.springframework.security.provisioning.InMemoryUserDetailsManager
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint
import org.springframework.security.web.util.matcher.MediaTypeRequestMatcher
import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.util.*

/**
 * Configures Spring Security to act as an OAuth2 Authorization Server.
 * This class sets up the security configurations necessary for the application to issue tokens,
 * manage client registrations, and handle other aspects related to OAuth2 and OpenID Connect (OIDC).
 */
@Configuration
@EnableWebSecurity
class StsConfig {

    /**
     * Defines the security filter chain specifically for OAuth2 authorization server endpoints.
     * Configures authentication mechanisms, request handling, and endpoint security tailored to OAuth2.
     *
     * @param http The [HttpSecurity] configuration builder provided by Spring Security.
     * @return A [SecurityFilterChain] that includes security rules for the authorization server.
     */
    @Bean
    @Order(1)
    fun authorizationServerSecurityFilterChain(http: HttpSecurity): SecurityFilterChain =
        http
            .also { chain ->
                OAuth2AuthorizationServerConfiguration.applyDefaultSecurity(chain)
                chain
                    .getConfigurer(OAuth2AuthorizationServerConfigurer::class.java)
                    .oidc(Customizer.withDefaults())
            }
            // Redirect to the login page when not authenticated from the
            // authorization endpoint
            .exceptionHandling { exceptions ->
                exceptions.defaultAuthenticationEntryPointFor(
                    LoginUrlAuthenticationEntryPoint("/login"),
                    MediaTypeRequestMatcher(MediaType.TEXT_HTML)
                )
            }
            // Accept access tokens for User Info and/or Client Registration
            .oauth2ResourceServer { resourceServer ->
                resourceServer.jwt(Customizer.withDefaults())
            }
            .build()

    /**
     * General web security filter chain configuration for the application.
     * Sets up basic authentication, form login, and default security directives for web requests.
     *
     * @param http The [HttpSecurity] object provided by Spring Security.
     * @return A [SecurityFilterChain] for general web application security.
     */
    @Bean
    @Order(2)
    fun securityFilterChain(http: HttpSecurity): SecurityFilterChain =
        http
            .authorizeHttpRequests { authorizedRequest ->
                authorizedRequest.anyRequest().authenticated()
            }
            .formLogin(Customizer.withDefaults())
            .build()

    /**
     * Settings for the OAuth2 authorization server.
     * Specifies the issuer URL and other critical settings necessary for the OAuth2 functioning.
     *
     * @return [AuthorizationServerSettings] configured with issuer and other properties.
     */
    @Bean
    fun authorizationServerSettings(): AuthorizationServerSettings =
        AuthorizationServerSettings
            .builder()
            .issuer("http://localhost:8080/sts")
            .build()

    /**
     * Repository for client configurations, allowing the authorization server to recognize and interact with registered clients.
     * Uses an in-memory approach for storing client details.
     *
     * @return [RegisteredClientRepository] containing client registration details.
     */
    @Bean
    fun clientRegistrationRepository(): RegisteredClientRepository {
        val pkceClient = RegisteredClient
            .withId("pkce")
            .clientId("pkce-client")
            .clientSecret("{noop}muchsecret")
            .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
            .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
            .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
            .authorizationGrantType(AuthorizationGrantType.CLIENT_CREDENTIALS)
            .scope(OidcScopes.OPENID)
            .scope(OidcScopes.PROFILE)
            .clientSettings(
                ClientSettings
                    .builder()
                    .requireAuthorizationConsent(false)
                    .requireProofKey(true)
                    .build()
            )
            .redirectUri("http://127.0.0.1:8080/client/login/oauth2/code/pkce") // Localhost not allowed
            .postLogoutRedirectUri("http://127.0.0.1:8080/client")
            .build()
        return InMemoryRegisteredClientRepository(pkceClient)
    }

    /**
     * Provides a JWT decoder using the RSA key pair generated for signing JWTs.
     * This decoder is essential for validating JWTs issued by the authorization server.
     *
     * @param jwkSource Source of JSON Web Keys used for decoding tokens.
     * @return [JwtDecoder] for decoding JWTs.
     */
    @Bean
    fun jwtDecoder(jwkSource: JWKSource<SecurityContext>): JwtDecoder =
        OAuth2AuthorizationServerConfiguration.jwtDecoder(jwkSource)

    /**
     * Source of JSON Web Keys (JWK) providing public and private keys for JWT operations.
     * This setup supports the dynamic addition of keys for token signing and encryption.
     *
     * @return [JWKSource] managing the JWKs used by the authorization server.
     */
    @Bean
    fun jwkSource(): JWKSource<SecurityContext> {
        val keyPair = generateRsaKey()
        val publicKey = keyPair.public as RSAPublicKey
        val privateKey = keyPair.private as RSAPrivateKey
        val rsaKey = RSAKey
            .Builder(publicKey)
            .privateKey(privateKey)
            .keyID(UUID.randomUUID().toString())
            .build()
        val jwkSet = JWKSet(rsaKey)
        return ImmutableJWKSet(jwkSet)
    }

    /**
     * Provides the custom [UserDetailsService] for authentication.
     *
     * @return A [UserDetailsService] instance that is used for loading user-specific data.
     */
    @Bean
    fun userDetailsService(): UserDetailsService {
        val user = User
            .withUsername("user")
            .password("{noop}password")
            .roles("USER")
            .build()
        val admin = User
            .withUsername("admin")
            .password("{noop}password")
            .roles("ADMIN")
            .build()

        return InMemoryUserDetailsManager(user, admin)
    }

    /**
     * Configures a custom JWT token enhancer to include additional claims in the JWT.
     *
     * This bean creates a customizer for JWT tokens that modifies the JWT structure by adding custom claims.
     * It specifically adjusts the JWT access tokens to include a simplified list of user roles, which are derived
     * from the authorities granted to the user.
     *
     * @return OAuth2TokenCustomizer<JwtEncodingContext> A customizer that is applied to the JWTs generated by the authorization server.
     */
    @Bean
    fun jwtTokenCustomizer(): OAuth2TokenCustomizer<JwtEncodingContext> = OAuth2TokenCustomizer { context ->
        // Check if the token being customized is an access token
        if (OAuth2TokenType.ACCESS_TOKEN.equals(context.tokenType)) {
            context.claims.claims { claims ->
                // Extract the roles from the authorities, removing the 'ROLE_' prefix and adding them as a simple list of roles
                val roles = AuthorityUtils.authorityListToSet(context.getPrincipal<UsernamePasswordAuthenticationToken>().authorities)
                    .map { it.replaceFirst("ROLE_", "") }
                    .toSet()
                claims["roles"] = roles
            }
        }
    }

    companion object {
        /**
         * Utility method to generate an RSA key pair for signing JWTs.
         * Uses RSA algorithm and initializes a 2048-bit key size.
         *
         * @return [KeyPair] consisting of RSA public and private keys.
         */
        @JvmStatic
        private fun generateRsaKey(): KeyPair =
            try {
                val keyPairGenerator = KeyPairGenerator.getInstance("RSA")
                keyPairGenerator.initialize(2048)
                keyPairGenerator.genKeyPair()
            } catch (e: Exception) {
                throw IllegalStateException(e)
            }
    }
}