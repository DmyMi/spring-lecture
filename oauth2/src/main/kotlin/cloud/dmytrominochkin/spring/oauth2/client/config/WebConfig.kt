package cloud.dmytrominochkin.spring.oauth2.client.config

import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken
import org.springframework.web.accept.ContentNegotiationManager
import org.springframework.web.client.RestClientException
import org.springframework.web.client.RestTemplate
import org.springframework.web.servlet.ViewResolver
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.function.RouterFunction
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.router
import org.thymeleaf.extras.springsecurity6.dialect.SpringSecurityDialect
import org.thymeleaf.spring6.SpringTemplateEngine
import org.thymeleaf.spring6.templateresolver.SpringResourceTemplateResolver
import org.thymeleaf.spring6.view.ThymeleafViewResolver

/**
 * Configuration class for Spring MVC, enabling MVC support, including view resolution with Thymeleaf
 * and functional endpoints for handling HTTP requests.
 *
 * This class implements the [WebMvcConfigurer] interface to customize the configuration of Spring MVC.
 */
@Configuration
@ComponentScan(basePackages = ["cloud.dmytrominochkin.spring.oauth2.client"])
@EnableWebMvc
class WebConfig : WebMvcConfigurer {
    @Bean
    fun functionalRoutes(clientService: OAuth2AuthorizedClientService): RouterFunction<*> = router {
        GET("/simpleapi") { serverRequest ->
            val headers = httpHeaders(serverRequest, clientService)
            val restTemplate = RestTemplate()
            val entity = HttpEntity("", headers)
            val response =
                restTemplate.exchange("http://localhost:8080/api/", HttpMethod.GET, entity, String::class.java)

            ok().body(response.body ?: "")
        }
        GET("/adminapi") { serverRequest ->
            val headers = httpHeaders(serverRequest, clientService)
            val restTemplate = RestTemplate()
            val entity = HttpEntity("", headers)
            try {
                val response = restTemplate.exchange("http://localhost:8080/api/admin", HttpMethod.GET, entity, String::class.java)
                ok().body(response.body ?: "")
            } catch (e: RestClientException) {
                // This is just for demonstration of response:)
                ok().body(e.message ?: "Something went wrong")
            }
        }
        GET("/") { serverRequest ->
            val authentication = serverRequest.principal().orElseThrow() as OAuth2AuthenticationToken
            val oauth2User = authentication.principal
            val client = clientService.loadAuthorizedClient<OAuth2AuthorizedClient>(
                authentication.authorizedClientRegistrationId,
                authentication.name
            )
            ok().render(
                "index",
                mapOf(
                    "userName" to oauth2User.name,
                    "clientName" to client.clientRegistration.clientName,
                    "userAttributes" to oauth2User.attributes,
                    "userAuthorities" to oauth2User.authorities.map { it.authority }
                )
            )
        }
    }

    /**
     * Configures the template resolver for Thymeleaf, specifying the location of view templates.
     *
     * @param applicationContext The Spring [ApplicationContext], automatically injected by Spring.
     * @return The configured template resolver.
     */
    @Bean
    fun templateResolver(applicationContext: ApplicationContext) = SpringResourceTemplateResolver().apply {
        setApplicationContext(applicationContext) // Link with Spring's ApplicationContext.
        prefix = "classpath:/templates/" // Prefix that points to the location of the Thymeleaf templates.
        suffix = ".html" // Suffix used for the Thymeleaf template files.
    }

    /**
     * Creates and configures the Thymeleaf template engine with the template resolver.
     *
     * @param templateResolver The Thymeleaf template resolver for resolving views.
     * @return The configured [SpringTemplateEngine] instance.
     */
    @Bean
    fun templateEngine(templateResolver: SpringResourceTemplateResolver) = SpringTemplateEngine().apply {
        setTemplateResolver(templateResolver) // Set the template resolver.
        enableSpringELCompiler = true // Enable Spring Expression Language compiler for efficiency.
        addDialect(SpringSecurityDialect())
    }

    /**
     * Configures the view resolver for Thymeleaf, which helps Spring MVC serve HTML content.
     *
     * @param applicationContext The Spring [ApplicationContext], automatically injected by Spring.
     * @param templateEngine The Thymeleaf template engine for processing templates.
     * @return A [ViewResolver] instance for resolving view names to actual views.
     */
    @Bean
    fun viewResolver(applicationContext: ApplicationContext, templateEngine: SpringTemplateEngine): ViewResolver =
        with(ViewResolverRegistry(ContentNegotiationManager(), applicationContext)) {
            val resolver = ThymeleafViewResolver()
            resolver.templateEngine = templateEngine // Link with the Thymeleaf template engine.
            viewResolver(resolver) // Register the resolver.
            return resolver
        }

    /**
     * Constructs the HTTP headers necessary for making authenticated HTTP requests, including the OAuth2 Bearer token.
     *
     * This function extracts the OAuth2 authentication token from the server request, retrieves the associated [OAuth2AuthorizedClient]
     * using a client service, and constructs an [HttpHeaders] object containing the Authorization header populated with the Bearer token.
     *
     * @param serverRequest The server request from which to extract the current user's authentication information.
     * @param clientService The service used to retrieve OAuth2 authorized client information, such as access tokens.
     * @return [HttpHeaders] containing the Authorization header with the Bearer token.
     * @throws NoSuchElementException if the authentication token cannot be found in the server request, indicating that the request is unauthenticated.
     */
    private fun httpHeaders(
        serverRequest: ServerRequest,
        clientService: OAuth2AuthorizedClientService
    ): HttpHeaders {
        val authentication = serverRequest.principal().orElseThrow() as OAuth2AuthenticationToken
        val client = clientService.loadAuthorizedClient<OAuth2AuthorizedClient>(
            authentication.authorizedClientRegistrationId,
            authentication.name
        )
        return HttpHeaders().apply {
            add(HttpHeaders.AUTHORIZATION, "Bearer ${client.accessToken.tokenValue}")
        }
    }
}