package cloud.dmytrominochkin.spring.oauth2.client.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.oauth2.client.InMemoryOAuth2AuthorizedClientService
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService
import org.springframework.security.oauth2.client.registration.ClientRegistration
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository
import org.springframework.security.oauth2.client.web.DefaultOAuth2AuthorizationRequestResolver
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestCustomizers
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestResolver
import org.springframework.security.oauth2.core.AuthorizationGrantType
import org.springframework.security.oauth2.core.ClientAuthenticationMethod
import org.springframework.security.oauth2.core.oidc.OidcScopes
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.util.matcher.AntPathRequestMatcher

/**
 * Configuration class for Spring Security that configures this application to serve as an OAuth2
 * Authorization Server
 *
 * [EnableWebSecurity]: This annotation enables Spring Security's web security support
 * and provides the Spring MVC integration.
 */
@Configuration
@EnableWebSecurity
class ClientConfig {

    @Bean
    fun securityFilterChain(
        http: HttpSecurity,
        resolver: OAuth2AuthorizationRequestResolver,
        repo: ClientRegistrationRepository,
        svc: OAuth2AuthorizedClientService
    ): SecurityFilterChain =
        http
            .authorizeHttpRequests { authRequest ->
                authRequest
                    .requestMatchers(AntPathRequestMatcher.antMatcher("/login/**")).permitAll()
                    .anyRequest().authenticated()
            }
            .oauth2Login { oauth ->
                oauth.authorizationEndpoint { endpoint ->
                    endpoint.authorizationRequestResolver(resolver)
                }
                oauth.clientRegistrationRepository(repo)
                oauth.authorizedClientService(svc)
            }
            .oauth2Client(Customizer.withDefaults())
            .build()

    @Bean
    fun pkceResolver(repo: ClientRegistrationRepository): OAuth2AuthorizationRequestResolver =
        DefaultOAuth2AuthorizationRequestResolver(repo, "/oauth2/authorization/").apply {
            setAuthorizationRequestCustomizer(OAuth2AuthorizationRequestCustomizers.withPkce())
        }

    @Bean
    fun clientRegistrationRepository(): ClientRegistrationRepository {
        val pkceClient = ClientRegistration
            .withRegistrationId("pkce")
            .clientName("Spring STS")
            .authorizationUri("http://localhost:8080/sts/oauth2/authorize")
            .tokenUri("http://localhost:8080/sts/oauth2/token")
            .redirectUri("http://127.0.0.1:8080/client/login/oauth2/code/pkce")
            .issuerUri("http://localhost:8080/sts")
            .jwkSetUri("http://localhost:8080/sts/oauth2/jwks")
            .userInfoUri("http://localhost:8080/sts/userinfo")
            .clientId("pkce-client")
            .clientSecret("muchsecret")
            .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
            .userNameAttributeName("sub")
            .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
            .scope(OidcScopes.OPENID, OidcScopes.PROFILE)
            .build()
        return InMemoryClientRegistrationRepository(pkceClient)
    }

    @Bean
    fun authorizedClientService(repo: ClientRegistrationRepository): OAuth2AuthorizedClientService =
        InMemoryOAuth2AuthorizedClientService(repo)
}