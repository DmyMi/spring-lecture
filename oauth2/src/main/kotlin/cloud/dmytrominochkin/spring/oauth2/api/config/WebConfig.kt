package cloud.dmytrominochkin.spring.oauth2.api.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.function.RouterFunction
import org.springframework.web.servlet.function.router

/**
 * Configuration class for Spring MVC, enabling MVC support and functional endpoints for handling HTTP requests.
 *
 * This class implements the [WebMvcConfigurer] interface to customize the configuration of Spring MVC.
 */
@Configuration
@EnableWebMvc
class WebConfig : WebMvcConfigurer {
    @Bean
    fun functionalRoutes(): RouterFunction<*> = router {
        GET("/admin") { serverRequest ->
            val jwt = serverRequest.principal().orElseThrow() as JwtAuthenticationToken
            ok().body("Hello, ${jwt.name}")
        }
        GET("/") { serverRequest ->
            val jwt = serverRequest.principal().orElseThrow() as JwtAuthenticationToken
            ok().body("Hello, ${jwt.name}")
        }
    }
}