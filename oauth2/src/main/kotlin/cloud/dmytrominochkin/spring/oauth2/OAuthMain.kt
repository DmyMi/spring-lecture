package cloud.dmytrominochkin.spring.oauth2

import jakarta.servlet.DispatcherType
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.eclipse.jetty.ee10.servlet.FilterHolder
import org.eclipse.jetty.ee10.servlet.ServletContextHandler
import org.eclipse.jetty.ee10.servlet.ServletHolder
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.handler.ContextHandlerCollection
import org.springframework.web.context.ContextLoaderListener
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext
import org.springframework.web.filter.DelegatingFilterProxy
import org.springframework.web.servlet.DispatcherServlet
import java.util.*

/**
 * The main entry point of the application that sets up and starts a Jetty server with Spring MVC configured.
 */
fun main(args: Array<String>) {
    // Log the initiation of the server startup process.
    logger.info("Starting server at port {}", 8080)

    // Configure and initialize the Jetty server on port 8080.
    val server = Server(8080).apply {
        handler = servletContextHandlers // Set a custom handler to integrate Spring MVC.
        addRuntimeShutdownHook(logger) // Add a shutdown hook for clean server shutdown.
    }

    // Start the server.
    server.start()
    logger.info("Server started at port {}", 8080) // Log the successful server start.

    // Wait for the server to be terminated before exiting the main thread.
    server.join()
}

/**
 * A logger instance for logging information about the server's operation.
 */
private val logger: Logger
    get() = LogManager.getLogger()

/**
 * Manages a collection of ServletContextHandlers for different parts of a Spring-based application,
 * enabling modular configuration of separate contexts like STS, client, and API services.
 *
 * @return [ContextHandlerCollection] A collection of configured servlet context handlers.
 */
private val servletContextHandlers: ContextHandlerCollection
    get() = ContextHandlerCollection(
        getServletContextHandler("cloud.dmytrominochkin.spring.oauth2.sts.config", "/sts/"),
        getServletContextHandler("cloud.dmytrominochkin.spring.oauth2.client.config", "/client/"),
        getServletContextHandler("cloud.dmytrominochkin.spring.oauth2.api.config", "/api/")
    )

/**
 * Creates and configures a ServletContextHandler for a specific part of the application,
 * using Spring's DispatcherServlet tied to a specific Spring WebApplicationContext.
 *
 * @param configPackage The package location where the Spring configuration for this context is defined.
 * @param path The context path that this servlet context handler will serve.
 * @return ServletContextHandler Configured with a DispatcherServlet, Spring Security filter, and context listeners.
 */

private fun getServletContextHandler(
    configPackage: String,
    path: String
): ServletContextHandler {
    val webApplicationContext = AnnotationConfigWebApplicationContext().apply {
        setConfigLocation(configPackage)
    }
    val dispatcherServlet = DispatcherServlet(webApplicationContext) // Create Spring's DispatcherServlet.
    val filter = DelegatingFilterProxy(
        "springSecurityFilterChain",
        webApplicationContext
    ) // Create the DelegatingFilterProxy to use as Spring Security filter chain.
    val springServletHolder = ServletHolder("dispatcherServlet", dispatcherServlet) // Wrap the servlet in a holder.

    return ServletContextHandler(ServletContextHandler.SESSIONS).apply {
        errorHandler = null // Optional: Custom error handling can be configured here.
        contextPath = path // Set the context path to the root.
        addServlet(springServletHolder, "/*") // Register the DispatcherServlet to handle all requests.
        addFilter(
            FilterHolder(filter),
            "/*",
            EnumSet.of(DispatcherType.REQUEST)
        ) // Register the Spring Security filter chain to handle all requests.
        addEventListener(ContextLoaderListener(webApplicationContext)) // Ensure the Spring context is loaded and closed correctly.
    }
}

/**
 * Adds a runtime shutdown hook to the server for a graceful shutdown.
 * This method attempts to stop the server when the JVM is shutting down.
 *
 * @param logger A logger instance for logging shutdown events or errors.
 */
private fun Server.addRuntimeShutdownHook(logger: Logger) {
    Runtime.getRuntime().addShutdownHook(Thread {
        if (isStarted) {
            stopAtShutdown = true
            try {
                // Attempt to stop the server on JVM shutdown.
                stop()
            } catch (e: Exception) {
                logger.error("Error while stopping jetty server: ${e.message}", e)
            }
        }
    })
}