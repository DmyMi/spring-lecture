package cloud.dmytrominochkin.spring.oauth2.api.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.oauth2.jwt.JwtDecoder
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.util.matcher.AntPathRequestMatcher

/**
 * Configuration class for securing an API using Spring Security with JWT for authentication.
 * This class demonstrates how to set up a security filter chain specifically for API endpoints,
 * implement JWT decoding, and convert JWT claims to Spring Security authorities.
 *
 * [EnableWebSecurity] Enables Spring Security's web security support.
 */
@Configuration
@EnableWebSecurity
class ApiConfig {
    /**
     * Defines the security filter chain for API requests.
     * Configures authorization rules and sets up OAuth2 resource server support for handling JWT tokens.
     *
     * @param http The [HttpSecurity] configuration builder provided by Spring Security.
     * @return A fully configured [SecurityFilterChain].
     */
    @Bean
    fun securityFilterChain(http: HttpSecurity): SecurityFilterChain =
        http
            .authorizeHttpRequests { authRequest ->
                authRequest
                    .requestMatchers(AntPathRequestMatcher.antMatcher(HttpMethod.GET, "/admin")).hasRole("ADMIN")
                    .anyRequest().authenticated()
            }
            .oauth2ResourceServer { rs ->
                rs.jwt(Customizer.withDefaults())
            }
            .build()

    /**
     * Configures a [JwtDecoder] using a JWK set URI.
     * The [JwtDecoder] is used to decode and validate JWTs using JSON Web Keys (JWK) obtained from the specified URI.
     *
     * @return [JwtDecoder] for decoding JWTs based on JWKs hosted at a specific endpoint.
     */
    @Bean
    fun jwtDecoder(): JwtDecoder = NimbusJwtDecoder
        .withJwkSetUri("http://localhost:8080/sts/oauth2/jwks").build()

    /**
     * Customizes how JWT tokens are converted into Authentication objects.
     * Sets up a [JwtAuthenticationConverter] that maps JWT claims to granted authorities in Spring Security.
     *
     * @return [JwtAuthenticationConverter] to process JWTs and extract authorities.
     */
    @Bean
    fun jwtAuthenticationConverter(): JwtAuthenticationConverter {
        val grantedAuthoritiesConverter = JwtGrantedAuthoritiesConverter()
        grantedAuthoritiesConverter.setAuthoritiesClaimName("roles")
        grantedAuthoritiesConverter.setAuthorityPrefix("ROLE_")

        val jwtAuthenticationConverter = JwtAuthenticationConverter()
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(grantedAuthoritiesConverter)
        return jwtAuthenticationConverter
    }
}