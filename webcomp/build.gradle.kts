plugins {
    kotlin("jvm")
    application
}

group = "cloud.dmytrominochkin.spring"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    compileOnly(libs.jakarta.servlet)
    compileOnly(libs.jakarta.jsp)
    compileOnly(libs.jakarta.jstl)
    implementation(libs.jetty.webapp)
    implementation(libs.jetty.annotations)
    implementation(libs.jetty.jsp)
    implementation(libs.jetty.jstl)
    implementation(libs.bundles.logging)

    testImplementation(libs.kotlin.test)
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(17)
}

application {
    mainClass.set("cloud.dmytrominochkin.spring.webcomp.WebCompMain")
}