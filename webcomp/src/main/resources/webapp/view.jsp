<%-- The page directive with the import attribute is used to import Java classes,
     allowing them to be referenced directly in the JSP page without needing to specify the fully qualified class name. --%>
<%@ page import="cloud.dmytrominochkin.spring.webcomp.mvc.Employee" %>
<%@ page import="cloud.dmytrominochkin.spring.webcomp.mvc.EmployeeModel" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%-- The taglib directive is used to declare the usage of a tag library within the JSP page.
     'prefix' assigns a shorthand name ('c' in this case) to be used as a namespace for the tags from the library.
     'uri' specifies the URI that identifies the tag library --%>
<%@ taglib prefix="c" uri="jakarta.tags.core" %>

<html>
<head>
    <title>Employee List</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 10px;
        }
    </style>
</head>
<body>

<h2>Employee List</h2>

<%-- A JSP scriptlet is used here to execute Java code. This code retrieves an instance of EmployeeModel
     from the request attributes. --%>
<%
    EmployeeModel model = (EmployeeModel) request.getAttribute("model");
%>
<p>Use custom code to process model</p>
<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Experience (Years)</th>
        <th>Salary ($)</th>
    </tr>
    </thead>
    <tbody>
    <%-- Another scriptlet that iterates over the employees in the model. The opening and closing tags of the scriptlet
         encapsulate the loop construct.
         JSP expressions are used inside a loop to dynamically insert the values of the employee's name,
         experience, and salary into the HTML. --%>
    <%
        for (Employee emp : model.getEmployees()) {
    %>
    <tr>
        <td><%= emp.getName() %></td>
        <td><%= emp.getExperience() %></td>
        <td><%= emp.getSalary() %></td>
    </tr>
    <%
        }
    %>
    </tbody>
</table>

<%-- This scriptlet demonstrates setting an attribute in the page context, making it accessible to JSTL tags and Expression Language (EL) expressions.
This is done to use the model with JSTL below. --%>
<%
    pageContext.setAttribute("employees", model.getEmployees());
%>

<p>Use JSTL tags to process model</p>
<table>
    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Experience (Years)</th>
        <th>Salary ($)</th>
    </tr>
    </thead>
    <tbody>
    <%-- The JSTL <c:forEach> tag iterates over a collection. 'items' specifies the collection,
         'var' is the loop variable that stores the current item, and 'varStatus' provides loop status information (e.g., count).
         EL expressions are used to access properties of the current 'emp' (the loop variable) and 'status' (loop status). --%>
    <c:forEach var="emp" items="${employees}" varStatus="status">
        <tr>
            <td>${status.count}</td>
            <td>${emp.name}</td>
            <td>${emp.experience}</td>
            <td>${emp.salary}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>

</body>
</html>