<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%-- The @taglib directive declares the usage of a custom tag library within the JSP page:
      - uri specifies the path to the Tag Library Descriptor (TLD). This TLD file
        contains definitions for custom tags in the library. In this case, the TLD is located at "/WEB-INF/tlds/mylibrary".
      - prefix defines a namespace prefix ("say") that is used to reference the custom tags defined in the tag library. This prefix must
        precede the tag name when using a tag from the library in the JSP page. --%>
<%@ taglib uri="/WEB-INF/tlds/mylibrary" prefix="say" %>
<html>
<head>
    <title>Example tag</title>
</head>
<body>
<p>
    <%-- This line demonstrates the usage of a custom tag (hello) from the imported tag library. The tag is referenced by combining
         the namespace prefix ("say") with the tag name ("hello"), separated by a colon. This custom tag is self-closing and,
         when processed by the JSP engine, will invoke the corresponding tag handler class defined in the TLD. The behavior of the
         tag, such as generating specific output or processing data, is implemented in this tag handler class. --%>
    <say:hello/>
</p>
</body>
</html>
