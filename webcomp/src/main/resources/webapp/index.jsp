<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Web Components Examples</title>
    </head>
    <body>
        <p>Java in a static page: <a href="/static.jsp" target="_blank">here</a></p>
        <p>MVC example (with JSTL): <a href="/mvc" target="_blank">here</a></p>
        <p>Using custom tag library: <a href="/hello" target="_blank">here</a></p>
        <p>Using response decorator: <a href="/decorate" target="_blank">here</a></p>
        <p>A long-running servlet: <a href="/long" target="_blank">here</a></p>
        <p>Using async listener: <a href="/async" target="_blank">here</a></p>
        <p>Using async listener that timeouts: <a href="/async?timeout=5000" target="_blank">here</a></p>
    </body>
</html>