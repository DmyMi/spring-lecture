package cloud.dmytrominochkin.spring.webcomp.filters

import jakarta.servlet.FilterChain
import jakarta.servlet.annotation.WebFilter
import jakarta.servlet.http.HttpFilter
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import java.util.concurrent.atomic.AtomicInteger

/**
 * A servlet filter for measuring and logging the processing time of HTTP requests.
 * This filter intercepts all requests and logs the time taken to process each request, along with a request counter.
 *
 * [jakarta.servlet.Filter] represents a generic filter that can be applied to requests or responses (or both)
 * of any type, not just HTTP. The [HttpFilter], introduced in Servlet API 4.0,
 * is an abstract class that extends the functionality of Filter specifically for HTTP requests.
 * It simplifies the creation of filters that only need to handle HTTP requests and responses.
 *
 * The filter demonstrates a practical use of servlet filters for monitoring and performance analysis.
 *
 * Annotations:
 * - [WebFilter]: Marks this class as a filter and sets its configuration.
 *   - [WebFilter.urlPatterns]: Specifies that this filter should be applied to all requests.
 *   - [WebFilter.asyncSupported]: Indicates that this filter supports asynchronous request processing,
 *     necessary for applications using asynchronous servlets.
 */
@WebFilter(
    urlPatterns = ["/*"],
    asyncSupported = true
)
class ResponseTimeFilter : HttpFilter() {
    /**
     * An AtomicInteger used for counting the number of requests processed by this filter.
     * This is thread-safe and ensures accurate counting even under high concurrency.
     */
    private val counter = AtomicInteger()

    /**
     * Intercepts HTTP requests to measure and log the time taken for their processing.
     *
     * @param request The [HttpServletRequest] object that contains the request the client has made of the servlet.
     * @param response The [HttpServletResponse] object that contains the response the servlet sends to the client.
     * @param chain The [FilterChain] for invoking the next filter in the chain, or if the calling filter is the last filter,
     *              the resource at the end of the chain.
     */
    override fun doFilter(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        // Increment the request counter and store the current count.
        val n = counter.addAndGet(1)

        // Record the start time of the request processing.
        val t0 = System.currentTimeMillis()

        // Proceed with the rest of the filter chain (including the target servlet or static resource).
        chain.doFilter(request, response)

        // Record the end time of the request processing.
        val t1 = System.currentTimeMillis()

        // Log the request number, the servlet path, and the time taken for processing the request.
        request.servletContext.log("Request #$n to ${request.servletPath} took ${t1 - t0} ms")
    }
}
