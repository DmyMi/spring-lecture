package cloud.dmytrominochkin.spring.webcomp.filters

import jakarta.servlet.*

/**
 * An abstract filter class that provides a structured way to process HTTP requests and responses
 * by defining pre-processing, post-processing, and error-handling behaviors.
 *
 * This class implements the [Filter] interface, requiring the implementation of the [doFilter] method.
 * It outlines a generic filtering pattern that can be applied to a variety of use cases, such as logging,
 * authentication, request modification, response enhancement, and error handling.
 *
 * Subclasses are expected to implement the abstract methods [doBeforeProcessing], [doAfterProcessing],
 * and [processError] to provide specific behavior during those phases of request processing.
 */
abstract class BaseFilter : Filter {
    /**
     * The core method of the filter that orchestrates the filtering process.
     * It wraps the invocation of the filter chain with pre-processing and post-processing steps,
     * and catches any exceptions thrown during the filter chain execution to handle them appropriately.
     *
     * @param request The ServletRequest object contains the client's request.
     * @param response The ServletResponse object contains the servlet's response.
     * @param chain The FilterChain for invoking the next filter in the chain, or if the calling filter is
     *              the last filter, the resource at the end of the chain.
     */
    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        // Perform pre-processing with the abstract method to be implemented by subclasses.
        doBeforeProcessing(request, response)

        // Initialize a variable to capture any potential error thrown during filter chain execution.
        var problem: Throwable? = null

        try {
            // Proceed with the rest of the filter chain (or the requested resource) execution.
            chain.doFilter(request, response)
        } catch (t: Throwable) {
            // Catch and record any throwable for later processing.
            problem = t
        }

        // Perform post-processing with the abstract method to be implemented by subclasses.
        doAfterProcessing(request, response)

        // If an error was caught, process it with the abstract method to be implemented by subclasses.
        problem?.let { processError(it, response) }
    }

    /**
     * Abstract method to perform processing before the filter chain is invoked.
     * Subclasses must implement this method to define specific pre-filtering behavior.
     *
     * @param request The ServletRequest object contains the client's request.
     * @param response The ServletResponse object contains the servlet's response.
     */
    abstract fun doBeforeProcessing(request: ServletRequest, response: ServletResponse)

    /**
     * Abstract method to perform processing after the filter chain has been invoked.
     * Subclasses must implement this method to define specific post-filtering behavior.
     *
     * @param request The ServletRequest object contains the client's request.
     * @param response The ServletResponse object contains the servlet's response.
     */
    abstract fun doAfterProcessing(request: ServletRequest, response: ServletResponse)

    /**
     * Abstract method to handle errors that occurred during the execution of the filter chain.
     * Subclasses must implement this method to define specific error handling behavior.
     *
     * @param error The Throwable object that was caught during the filter chain execution.
     * @param response The ServletResponse object contains the servlet's response.
     */
    abstract fun processError(error: Throwable, response: ServletResponse)
}

