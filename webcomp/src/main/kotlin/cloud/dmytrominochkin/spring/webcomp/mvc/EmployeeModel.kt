package cloud.dmytrominochkin.spring.webcomp.mvc

data class EmployeeModel(
    val employees: List<Employee> = listOf(
        Employee("Mykola", 2, 2000.0),
        Employee("Patron", 5, 4000.0),
        Employee("Stepan", 1, 500.0),
    )
)

data class Employee(
    val name: String,
    val experience: Int,
    val salary: Double
)