package cloud.dmytrominochkin.spring.webcomp.async

import jakarta.servlet.ServletContextEvent
import jakarta.servlet.ServletContextListener
import jakarta.servlet.annotation.WebListener
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

/**
 * A servlet context listener that initializes and destroys a thread pool executor
 * for handling asynchronous tasks in a web application.
 *
 * This listener responds to servlet context initialization and destruction events,
 * creating a shared thread pool executor at startup and shutting it down gracefully at shutdown.
 */
@WebListener
class AppContextListener : ServletContextListener {
    /**
     * Called when the servlet context is being initialized. It sets up a [ThreadPoolExecutor]
     * and stores it as a context attribute, making it accessible across the application.
     *
     * @param sce The [ServletContextEvent] containing the [ServletContext] that is being initialized.
     */
    override fun contextInitialized(sce: ServletContextEvent) {
        // Initialize a ThreadPoolExecutor with core pool size, maximum pool size, keep-alive time, time unit,
        // and a work queue capable of holding 100 tasks.
        val executor = ThreadPoolExecutor(
            100, // Core pool size
            200, // Maximum pool size
            50000L, // Keep-alive time
            TimeUnit.MILLISECONDS, // Time unit for keep-alive
            ArrayBlockingQueue(100) // Blocking queue for tasks
        )
        // Store the executor in the servlet context for application-wide access.
        sce.servletContext.setAttribute("executor", executor)
    }

    /**
     * Called when the servlet context is about to be destroyed. This method retrieves the
     * [ThreadPoolExecutor] from the context and shuts it down gracefully.
     *
     * @param sce The [ServletContextEvent] containing the [ServletContext] that is being destroyed.
     */
    override fun contextDestroyed(sce: ServletContextEvent) {
        // Retrieve the ThreadPoolExecutor from the servlet context.
        val executor = sce.servletContext.getAttribute("executor") as ThreadPoolExecutor
        // Shut down the executor, attempting to complete executing tasks.
        executor.shutdown()
    }
}
