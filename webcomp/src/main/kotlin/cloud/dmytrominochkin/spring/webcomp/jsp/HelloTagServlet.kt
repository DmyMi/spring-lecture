package cloud.dmytrominochkin.spring.webcomp.jsp

import jakarta.servlet.annotation.WebServlet
import jakarta.servlet.http.HttpServlet
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse


/**
 * A servlet that forwards requests to a JSP page which demonstrates the use of custom JSP tags.
 *
 * This servlet responds to GET requests by forwarding them to a specific JSP page named `hello-tag.jsp`.
 * The purpose is to showcase how servlets can interact with JSP pages, particularly in the context of
 * using custom tags defined within JSP files.
 */
@WebServlet(name = "helloTagServlet", value = ["/hello"])
class HelloTagServlet : HttpServlet() {
    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        val requestDispatcher = req.getRequestDispatcher("/hello-tag.jsp")
        requestDispatcher.forward(req, resp)
    }
}