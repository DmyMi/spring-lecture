package cloud.dmytrominochkin.spring.webcomp.async

import jakarta.servlet.AsyncContext

/**
 * A Runnable that encapsulates the logic for asynchronously processing a request.
 * It is designed to be executed by a [java.util.concurrent.ThreadPoolExecutor], allowing the servlet container
 * to handle other tasks while this long-running process completes.
 *
 * @property asyncContext The [AsyncContext] associated with the servlet request being processed.
 *                         This context allows interaction with the asynchronous operation, including
 *                         sending a response and completing the operation.
 */
class AsyncRequestProcessor(
    private val asyncContext: AsyncContext
) : Runnable {
    /**
     * The run method is invoked when this [Runnable] is executed by a [java.util.concurrent.ThreadPoolExecutor].
     * It performs a long-running processing task, writes a success message to the response,
     * and then completes the asynchronous context.
     */
    override fun run() {
        longProcessing() // Simulates a long-running task.

        try {
            // Attempt to write a success message to the response.
            val out = asyncContext.response.writer
            out.write("Success!")
            asyncContext.complete() // Mark the asynchronous operation as complete.
        } catch (e: IllegalStateException) {
            // Handle the case where the asynchronous operation cannot be completed.
            println(e.message)
        }
    }

    /**
     * Simulates a long-running task by causing the current thread to sleep.
     */
    private fun longProcessing() {
        try {
            Thread.sleep(10000) // Sleep for 10 seconds.
        } catch (e: InterruptedException) {
            // Log any interruption errors.
            asyncContext.request.servletContext.log("Some error", e)
        }
    }
}
