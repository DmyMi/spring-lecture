package cloud.dmytrominochkin.spring.webcomp.listeners

import jakarta.servlet.annotation.WebListener
import jakarta.servlet.http.HttpSessionAttributeListener
import jakarta.servlet.http.HttpSessionBindingEvent

/**
 * A listener for session attribute events, including addition, removal, and replacement of session attributes.
 * It implements the [HttpSessionAttributeListener] to respond to changes in session attribute state.
 *
 * Annotations:
 * - [WebListener]: Designates this class as a listener for session attribute events.
 */
@WebListener
class MySessionAttributeListener : HttpSessionAttributeListener {
    /**
     * Logs the addition of a session attribute.
     *
     * @param event Provides details about the session attribute event.
     */
    override fun attributeAdded(event: HttpSessionBindingEvent) {
        println("attribute added: ${event.value}")
    }

    /**
     * Logs the removal of a session attribute.
     *
     * @param event Provides details about the session attribute event.
     */
    override fun attributeRemoved(event: HttpSessionBindingEvent) {
        println("attribute removed: ${event.value}")
    }

    /**
     * Logs the replacement of a session attribute.
     *
     * @param event Provides details about the session attribute event.
     */
    override fun attributeReplaced(event: HttpSessionBindingEvent) {
        println("attribute replaced: ${event.value}")
    }
}
