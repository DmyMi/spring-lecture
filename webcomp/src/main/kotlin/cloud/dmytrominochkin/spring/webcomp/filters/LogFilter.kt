package cloud.dmytrominochkin.spring.webcomp.filters

import jakarta.servlet.ServletRequest
import jakarta.servlet.ServletResponse
import jakarta.servlet.annotation.WebFilter
import jakarta.servlet.http.HttpServletRequest
import java.util.*

/**
 * A web filter that logs the IP address and timestamp of each incoming request.
 * It demonstrates the use of servlet filters for logging purposes and intercepting requests
 * for auditing or monitoring.
 *
 * This filter applies to all incoming requests and supports asynchronous operations,
 * which is particularly useful for non-blocking servlets.
 *
 * Annotations:
 * - [WebFilter]: Marks this class as a filter and sets its configuration.
 *   - [WebFilter.urlPatterns]: Specifies that this filter should be applied to all requests.
 *   - [WebFilter.asyncSupported]: Indicates that this filter supports asynchronous request processing,
 *     necessary for applications using asynchronous servlets.
 */
@WebFilter(
    urlPatterns = ["/*"],
    asyncSupported = true
)
class LogFilter : BaseFilter() {
    /**
     * Logs each request's IP address and current timestamp before the request is processed.
     * Overrides [doBeforeProcessing] from [BaseFilter] to implement the logging logic.
     *
     * @param request The [ServletRequest] object contains the client's request.
     * @param response The [ServletResponse] object, not used here but part of the method signature.
     */
    override fun doBeforeProcessing(request: ServletRequest, response: ServletResponse) {
        // Cast the ServletRequest to HttpServletRequest to access method specific to HTTP requests.
        val req = request as HttpServletRequest

        // Retrieve the IP address from which the request originated.
        val ipAddress = req.remoteAddr

        // Use the servlet context's log method to write the IP address and current timestamp to the server log.
        request.servletContext.log("Ip: $ipAddress, Time: ${Date()}")
    }

    /**
     * A placeholder implementation of [doAfterProcessing]. In this filter, no actions are taken after the request is processed.
     *
     * @param request The [ServletRequest] object contains the client's request.
     * @param response The [ServletResponse] object, not used here but part of the method signature.
     */
    override fun doAfterProcessing(request: ServletRequest, response: ServletResponse) {
        // No post-processing action is implemented in this filter.
    }

    /**
     * A placeholder implementation of processError. In this filter, no specific error handling logic is implemented.
     *
     * @param error The [Throwable] object representing the error that occurred during request processing.
     * @param response The [ServletResponse] object, not used here but part of the method signature.
     */
    override fun processError(error: Throwable, response: ServletResponse) {
        // No error processing action is implemented in this filter.
    }
}
