package cloud.dmytrominochkin.spring.webcomp.async

import jakarta.servlet.annotation.WebServlet
import jakarta.servlet.http.HttpServlet
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import java.util.concurrent.ThreadPoolExecutor

/**
 * A servlet that handles GET requests asynchronously. When a GET request is received,
 * the servlet initializes an asynchronous context, registers an asynchronous listener,
 * and submits the request for processing by a [ThreadPoolExecutor]. This allows the servlet
 * to respond to long-running requests without blocking the servlet container's main thread.
 *
 * Annotations:
 * - : Configures this class as a servlet and sets its URL pattern and async support.
 *   - asyncSupported: Indicates that this servlet supports asynchronous operations.
 */
@WebServlet(
    urlPatterns = ["/async"],
    asyncSupported = true
)
class AsyncLongRunningServlet : HttpServlet() {
    /**
     * Processes HTTP GET requests by starting an asynchronous operation and offloading
     * the request processing to an executor service. This method logs the time taken to
     * initiate the asynchronous processing.
     *
     * @param req The [HttpServletRequest] object containing the client's request.
     * @param resp The [HttpServletResponse] object containing the servlet's response.
     */
    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        val startTime = System.currentTimeMillis() // Record start time.

        // Start the asynchronous context for this request and response.
        val asyncCtx = req.startAsync(req, resp)
        asyncCtx.addListener(AppAsyncListener()) // Register an asynchronous listener.

        // Optional: Set a custom timeout for the asynchronous operation.
        val timeout = req.getParameter("timeout")?.toLongOrNull()
        asyncCtx.timeout = timeout ?: 20_000 // Default timeout is 20 seconds.

        // Retrieve the ThreadPoolExecutor from the servlet context and execute the request processor.
        val executor = req.servletContext.getAttribute("executor") as ThreadPoolExecutor
        executor.execute(AsyncRequestProcessor(asyncCtx))

        val endTime = System.currentTimeMillis() // Record end time.
        servletContext.log("Time: ${endTime - startTime} ms") // Log the initiation time.
    }
}
