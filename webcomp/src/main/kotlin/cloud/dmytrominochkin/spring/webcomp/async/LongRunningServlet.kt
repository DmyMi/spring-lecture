package cloud.dmytrominochkin.spring.webcomp.async

import jakarta.servlet.annotation.WebServlet
import jakarta.servlet.http.HttpServlet
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse

/**
 * Demonstrates a servlet designed to handle long-running processes within a web application.
 * This servlet specifically showcases handling of a process that takes a significant amount of time to complete,
 * simulating a delay (e.g., complex calculations, external service calls, database operations) before sending a response.
 *
 * The use of [Thread.sleep] in this example is for demonstration purposes only and represents the long-running operation.
 */
@WebServlet("/long")
class LongRunningServlet : HttpServlet() {
    /**
     * Handles HTTP GET requests by executing a simulated long-running process and logging the execution time.
     */
    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        // Record the start time of the request.
        val startTime = System.currentTimeMillis()

        // Execute the long-running process.
        longProcessing()

        // Record the end time of the request, after the long processing completes.
        val endTime = System.currentTimeMillis()

        // Obtain a PrintWriter to send text data back to the client.
        val out = resp.writer
        // Write a simple success message to indicate the completion of the process.
        out.write("Success!")

        // Log the duration of the long-running process in the servlet context's log.
        servletContext.log("Time: ${endTime - startTime} ms")
    }

    /**
     * Simulates a long-running process by causing the current thread to sleep for 10 seconds.
     * This method is a stand-in for any time-consuming operation such as database transactions,
     * file processing, or calling external services.
     */
    private fun longProcessing() {
        try {
            // Pause the thread for 10 seconds to simulate a delay.
            Thread.sleep(10_000)
        } catch (e: InterruptedException) {
            // Exception handling for the thread being interrupted during sleep.
        }
    }
}
