package cloud.dmytrominochkin.spring.webcomp.listeners

import jakarta.servlet.annotation.WebListener
import jakarta.servlet.http.HttpSessionEvent
import jakarta.servlet.http.HttpSessionListener

/**
 * A listener that tracks the number of concurrent sessions in the web application.
 * It implements the `[HttpSessionListener]` interface to respond to session creation and destruction events.
 *
 * Annotations:
 * - [WebListener]: Indicates that this class is a listener which automatically registers it to listen
 *   for session events.
 */
@WebListener
class SessionCounter : HttpSessionListener {
    /**
     * Increments the count of concurrent users when a session is created.
     *
     * @param httpSessionEvent The session event.
     */
    override fun sessionCreated(httpSessionEvent: HttpSessionEvent) {
        concurrentUsers++
    }

    /**
     * Decrements the count of concurrent users when a session is invalidated or expires.
     *
     * @param httpSessionEvent The session event.
     */
    override fun sessionDestroyed(httpSessionEvent: HttpSessionEvent) {
        concurrentUsers--
    }

    companion object {
        /**
         * Tracks the number of concurrent users. This variable is incremented when a session is created
         * and decremented when a session is destroyed.
         */
        var concurrentUsers = 0
            private set
    }
}

