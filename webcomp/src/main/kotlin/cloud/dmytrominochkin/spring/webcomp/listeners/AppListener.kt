package cloud.dmytrominochkin.spring.webcomp.listeners

import jakarta.servlet.ServletContextEvent
import jakarta.servlet.ServletContextListener
import jakarta.servlet.annotation.WebListener
import java.util.*

/**
 * A listener for servlet context lifecycle events, logging the application startup time.
 * It implements the [ServletContextListener] interface to respond to context initialization and destruction events.
 *
 * Annotations:
 * - [WebListener]: Marks this class as a listener for various types of servlet events.
 *   This annotation is used by the servlet container to deploy the listener without needing web.xml configuration.
 */
@WebListener
class AppListener : ServletContextListener {
    /**
     * Captures and logs the timestamp when the servlet context is initialized (i.e., when the web application starts).
     *
     * @param ce The event containing the ServletContext that is being initialized.
     */
    override fun contextInitialized(ce: ServletContextEvent) {
        startupTime = System.currentTimeMillis()
        ce.servletContext.log("Server listener - started at: ${getStartupTime()}")
    }

    /**
     * A placeholder method for handling context destruction events. This method is empty
     * as this listener focuses on capturing the startup time.
     *
     * @param ce The event containing the ServletContext that is being destroyed.
     */
    override fun contextDestroyed(ce: ServletContextEvent) {}

    companion object {
        private var startupTime = 0L

        /**
         * Converts the startup timestamp to a [Date] object for easier readability.
         *
         * @return The startup time as a [Date] object.
         */
        fun getStartupTime(): Date = Date(startupTime)
    }
}

