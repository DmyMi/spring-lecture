package cloud.dmytrominochkin.spring.webcomp.filters

import jakarta.servlet.http.HttpServletResponse
import jakarta.servlet.http.HttpServletResponseWrapper
import java.io.PrintWriter
import java.io.StringWriter

/**
 * A response wrapper that captures the servlet's response output into a [StringWriter].
 * This class extends [HttpServletResponseWrapper], allowing it to wrap and modify
 * the behavior of an [HttpServletResponse].
 *
 * By overriding the [getWriter] method, it redirects the response output that would normally
 * be sent to the client, to an internal [StringWriter]. This can be useful for logging,
 * auditing, or modifying the response content before it's sent.
 *
 * Extends:
 * - [HttpServletResponseWrapper]: A convenient implementation of [HttpServletResponse]
 *   that decorates another response object, providing a way to modify response behavior.
 */
class SimpleResponseWrapper(response: HttpServletResponse) : HttpServletResponseWrapper(response) {
    /**
     * A [StringWriter] to capture the output of the response. Unlike the original response writer,
     * which writes directly to the client, this writer stores the data in a string buffer that
     * can be accessed and manipulated.
     */
    private val output = StringWriter()

    /**
     * Overrides [getWriter] to return a [PrintWriter] that writes to the internal [StringWriter]
     * instead of writing directly to the HTTP response. This effectively hides the original
     * writer, redirecting all output to [output].
     *
     * @return A [PrintWriter] that writes to an internal [StringWriter], capturing the response data.
     */
    override fun getWriter(): PrintWriter {
        return PrintWriter(output)
    }

    /**
     * Overrides the [toString] method to return the captured response content as a string.
     * This method allows easy access to the response data collected by the [StringWriter].
     *
     * @return The response content captured by the [StringWriter].
     */
    override fun toString(): String {
        return output.toString()
    }
}

