package cloud.dmytrominochkin.spring.webcomp

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.eclipse.jetty.ee10.servlet.ServletContextHandler
import org.eclipse.jetty.ee10.webapp.WebAppContext
import org.eclipse.jetty.server.Server

/**
 * Application that initializes and starts a Jetty server to serve web content.
 * Jetty is a lightweight, highly scalable, and flexible web server and servlet engine,
 * making it a suitable choice for serving dynamic web content from within an application.
 *
 * The server is configured to start on port 8080 and serve web content located in the
 * `/webapp` resource directory.
 */
class WebCompMain {
    companion object {
        /**
         * The main entry point of the application.
         *
         * @param args Command line arguments passed to the application (not used).
         */
        @JvmStatic
        fun main(args: Array<String>) {
            // Log the start of the server.
            logger.info("Starting server at port {}", 8080)

            // Initialize the Jetty server on port 8080 with custom handler and shutdown hook.
            val server = Server(8080).apply {
                handler = servletContextHandler
                addRuntimeShutdownHook(logger)
            }

            // Start the server and log its successful launch.
            server.start()
            logger.info("Server started at port {}", 8080)

            // Causes the current thread to wait until the server is terminated.
            server.join()
        }

        /**
         * Lazily-initialized logger for logging server events.
         */
        private val logger: Logger
            get() = LogManager.getLogger()

        /**
         * Configures a [ServletContextHandler] that specifies the web application context path
         * and location of the webapp resources.
         */
        private val servletContextHandler: ServletContextHandler
            get() {
                // Look for resource in classpath (this is the best choice when working with a jar/war archive)
                val webXml = WebCompMain::class.java.getResource("/webapp/WEB-INF/web.xml")
                    ?: throw RuntimeException("Unable to find resource directory")

                // Determine the root URI for the web application.
                val webRootUri = webXml.toURI().resolve("../").normalize()
                logger.info("WebRoot is {}", webRootUri)

                // Configure and return a WebAppContext.
                // WebAppContext is a ServletContextHandler that autoconfigures itself by reading a web.xml Servlet configuration file.
                return WebAppContext().apply {
                    contextPath = "/"
                    war = webRootUri.toASCIIString()
                    isParentLoaderPriority = true

                    // Jetty follows a hierarchical classloading model similar to that of other Jakarta EE web servers.
                    // This model allows different parts of the server and its deployed applications to have isolated classpaths,
                    // which helps in preventing conflicts and ensuring that applications run correctly with their bundled dependencies.
                    //
                    // By setting the ContainerIncludeJarPattern, we're fine-tuning this classloading behavior
                    // to precisely control which classes are visible to the application.
                    setAttribute("org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern", Regex(""".*/classes/.*|.*/.*jstl.*\.jar$""").toString())
                }
            }
    }
}

/**
 * Adds a shutdown hook to the Jetty server for graceful shutdown.
 * This ensures the server is cleanly shut down when the JVM exits.
 *
 * @param logger The Logger instance for logging shutdown events.
 */
private fun Server.addRuntimeShutdownHook(logger: Logger) {
    Runtime.getRuntime().addShutdownHook(Thread {
        if (isStarted) {
            stopAtShutdown = true
            try {
                // Attempt to stop the server on JVM shutdown.
                stop()
            } catch (e: Exception) {
                logger.error("Error while stopping jetty server: ${e.message}", e)
            }
        }
    })
}