package cloud.dmytrominochkin.spring.webcomp.jsp

import jakarta.servlet.jsp.tagext.SimpleTagSupport

/**
 * A custom tag handler for creating a simple "Hello" tag in JSP pages.
 * This class extends [SimpleTagSupport], a convenience class provided by the Jakarta Servlet API
 * for developing new custom tags with minimal effort.
 *
 * When a JSP page uses this custom tag, the [doTag] method is invoked to perform the tag's action.
 * In this case, it outputs a simple greeting message.
 *
 * Extends:
 * - [SimpleTagSupport]: Provides default implementations for all methods in the [jakarta.servlet.jsp.tagext.SimpleTag] interface
 *   except [jakarta.servlet.jsp.tagext.SimpleTag.doTag], which must be implemented by all subclasses. This class simplifies custom tag
 *   development by handling common tasks, such as managing parent-child relationships between tags.
 */
class HelloTagHandler : SimpleTagSupport() {
    /**
     * Processes the custom tag's action, generating output to include in the JSP page.
     *
     * This method overrides [SimpleTagSupport.doTag] from [SimpleTagSupport] to define the custom behavior
     * of the "Hello" tag. When the tag is encountered within a JSP page, this method is called
     * to execute its custom logic.
     */
    override fun doTag() {
        // Obtain a reference to the JSP output stream. `jspContext` is inherited from `SimpleTagSupport`
        // and provides context information about the JSP page.
        val out = jspContext.out

        // Use the JSP output stream to print "Hello tag!" directly into the JSP page where the tag is used.
        out.print("Hello tag!")
    }
}
