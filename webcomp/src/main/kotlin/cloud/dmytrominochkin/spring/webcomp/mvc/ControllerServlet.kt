package cloud.dmytrominochkin.spring.webcomp.mvc

import jakarta.servlet.annotation.WebServlet
import jakarta.servlet.http.HttpServlet
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse

/**
 * A servlet that acts as a controller in an MVC (Model-View-Controller) architecture.
 * It is responsible for handling requests, preparing data models, and forwarding the request
 * and model to a view (JSP page) for presentation.
 *
 * This servlet demonstrates a basic pattern for separating business logic and presentation
 * logic in a web application, making the application easier to maintain and extend.
 */
@WebServlet(
    name = "controllerServlet",
    urlPatterns = ["/mvc"]
)
class ControllerServlet : HttpServlet() {
    /**
     * Processes HTTP GET requests by preparing a model and forwarding it to a JSP page.
     *
     * @param req The [HttpServletRequest] object that contains the request made by the client.
     * @param resp The [HttpServletResponse] object that contains the response to be sent to the client.
     */
    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        // Create or retrieve the model to be used in the view. In this example, an EmployeeModel
        // instance is created to hold data about employees. This part of the code could be replaced
        // or expanded to include more complex business logic and data retrieval.
        val model = EmployeeModel()

        // Store the model in the request's attribute list under the key "model". This makes the model
        // accessible to the JSP page (view) that will render the response.
        req.setAttribute("model", model)

        // Obtain a RequestDispatcher for "/view.jsp", the JSP page responsible for rendering the model.
        // Then, use the RequestDispatcher to forward the request and response objects (along with the model)
        // to the view. This separation allows the servlet (controller) to focus on business logic and data
        // preparation, while the JSP page (view) focuses on presentation logic.
        req
            .getRequestDispatcher("/view.jsp")
            .forward(req, resp)
    }
}
