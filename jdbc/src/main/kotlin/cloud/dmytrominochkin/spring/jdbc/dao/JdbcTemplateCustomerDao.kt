package cloud.dmytrominochkin.spring.jdbc.dao

import cloud.dmytrominochkin.spring.jdbc.mapper.CustomerRowMapper
import cloud.dmytrominochkin.spring.jdbc.model.Customer
import org.springframework.jdbc.core.JdbcTemplate


/**
 * An implementation of the [CustomerDao] interface that uses Spring's [JdbcTemplate] for data
 * access operations on `Customer` entities stored in a relational database. The [JdbcTemplate]
 * provides a high-level abstraction for database access, simplifying JDBC operations, handling
 * SQL execution, and result set mapping.
 *
 * The Spring JDBC Template simplifies the need for boilerplate code traditionally associated with
 * JDBC, such as opening and closing connections, handling exceptions, and processing result sets.
 * It allows for focusing on the SQL queries and the business logic, leaving the resource management
 * and low-level implementation details to the framework.
 *
 * @property jdbcTemplate The [JdbcTemplate] instance used for executing database operations.
 * It is injected into this DAO implementation, enabling the execution of SQL queries, updates,
 * and other JDBC operations with simplified API methods.
 */
class JdbcTemplateCustomerDao(
    private val jdbcTemplate: JdbcTemplate
) : CustomerDao {

    /**
     * Inserts a new [Customer] into the database using a prepared statement update.
     *
     * @param model The [Customer] object to be inserted.
     */
    override fun insert(model: Customer) {
        val sql = "INSERT INTO CUSTOMER (CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_AGE) VALUES (?, ?, ?)"

        jdbcTemplate.update(sql, model.id, model.name, model.age)
    }

    /**
     * Finds and returns a [Customer] by its ID, using a query for object method with a custom
     * row mapper to map the SQL result set to a [Customer] object.
     *
     * @param id The ID of the [Customer] to find.
     * @return The [Customer] object if found, or `null` if no customer with the specified ID exists.
     */
    override fun findById(id: Int): Customer? {
        val sql = "SELECT * FROM CUSTOMER WHERE CUSTOMER_ID = ?"

        return jdbcTemplate.queryForObject(sql, CustomerRowMapper(), id)
    }

    /**
     * Retrieves and returns a list of all [Customer] objects from the database, using a query
     * method with a custom row mapper to convert each row of the SQL result set into a [Customer] object.
     *
     * @return A list of all [Customer] objects.
     */
    override fun findAll(): List<Customer> {
        val sql = "SELECT * FROM CUSTOMER"

        return jdbcTemplate.query(sql, CustomerRowMapper())
    }

    /**
     * Updates the details of an existing [Customer] in the database using a prepared statement update.
     *
     * @param model The [Customer] object containing the updated details.
     */
    override fun update(model: Customer) {
        val sql = "UPDATE CUSTOMER SET CUSTOMER_NAME = ?, CUSTOMER_AGE = ? WHERE CUSTOMER_ID = ?"

        jdbcTemplate.update(sql, model.name, model.age, model.id)
    }

    /**
     * Deletes a [Customer] from the database by its ID using a prepared statement update.
     *
     * @param id The ID of the [Customer] to be deleted.
     */
    override fun delete(id: Int) {
        val sql = "DELETE FROM CUSTOMER WHERE CUSTOMER_ID = ?"

        jdbcTemplate.update(sql, id)
    }
}