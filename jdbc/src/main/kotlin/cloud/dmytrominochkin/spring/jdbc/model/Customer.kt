package cloud.dmytrominochkin.spring.jdbc.model

data class Customer(
    var id: Int,
    var name: String,
    var age: Int
)
