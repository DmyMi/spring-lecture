package cloud.dmytrominochkin.spring.jdbc.dao

import cloud.dmytrominochkin.spring.jdbc.model.Customer


/**
 * An interface that extends the generic [Dao] interface to define data access operations
 * specifically for [Customer] objects. By extending [Dao], this interface inherits the standard
 * CRUD operations (Create, Read, Update, Delete) for managing [Customer] instances within a data source.
 *
 * Implementations of [CustomerDao] should provide the concrete logic for accessing the data source
 * (e.g., a relational database, a NoSQL database, or a web service) and mapping between [Customer]
 * objects and the data source.
 */
interface CustomerDao : Dao<Customer> {
    // Additional customer-specific methods can be defined here if needed.
}

