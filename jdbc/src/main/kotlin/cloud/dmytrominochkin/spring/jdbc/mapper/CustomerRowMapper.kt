package cloud.dmytrominochkin.spring.jdbc.mapper

import cloud.dmytrominochkin.spring.jdbc.model.Customer
import org.springframework.jdbc.core.RowMapper
import java.sql.ResultSet

/**
 * Implementation of the [RowMapper] interface for mapping rows of a [ResultSet] to [Customer] objects.
 * This mapper is used by JDBC operations to convert rows of a database result set into instances of
 * [Customer], allowing for easy extraction and transformation of data from a database into objects
 * that can be used within the application.
 *
 * Each call to [mapRow] converts a single row of the result set into a [Customer] object by extracting
 * the values of columns `CUSTOMER_ID`, `CUSTOMER_NAME`, and `CUSTOMER_AGE`. The [mapRow] method is
 * typically called by the Spring JDBC framework while processing a query's result set, for each row
 * in the result set.
 *
 * @property rs The [ResultSet] from which to extract [Customer] information.
 * @property rowNum The number of the current row to map. It can be used if needed for logging or
 * other purposes, but it is not used directly in mapping in this simple example.
 */
class CustomerRowMapper : RowMapper<Customer> {
    /**
     * Maps the current row of the provided [ResultSet] to a [Customer] object.
     * This method extracts the `CUSTOMER_ID`, `CUSTOMER_NAME`, and `CUSTOMER_AGE` fields from the
     * [ResultSet] and uses them to instantiate a [Customer] object.
     *
     * @param rs The [ResultSet] from which to extract data.
     * @param rowNum The row number of the current row in the [ResultSet].
     * @return A [Customer] object populated with data from the current row of the [ResultSet].
     */
    override fun mapRow(rs: ResultSet, rowNum: Int) =
        Customer(
            id = rs.getInt("CUSTOMER_ID"),
            name = rs.getString("CUSTOMER_NAME"),
            age = rs.getInt("CUSTOMER_AGE")
        )
}
