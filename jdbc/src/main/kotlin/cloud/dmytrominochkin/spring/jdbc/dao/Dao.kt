package cloud.dmytrominochkin.spring.jdbc.dao

/**
 * The Data Access Object (DAO) pattern is a structural pattern that allows for abstracting and
 * encapsulating all access to the data source. The DAO manages the connection with the data source
 * to obtain and store data. It abstracts the underlying data access implementation for the business
 * logic to enable transparent access to the data source.
 *
 * The DAO pattern also allows for keeping the data access mechanism agile, making it possible to
 * switch to a different storage mechanism (like from a local database to a web service) without
 * changing the rest of the application code.
 *
 * This interface defines the standard operations to be performed on a model object(s) as part of
 * the DAO pattern.
 *
 * @param T The type of the model object that this DAO will manage. This generic approach allows the
 * DAO to be flexible and reusable for various types of domain entities.
 */
interface Dao<T> {
    /**
     * Inserts a new model instance into the data source.
     *
     * @param model The model instance to be inserted.
     */
    fun insert(model: T)

    /**
     * Finds a model instance by its ID.
     *
     * @param id The ID of the model to find.
     * @return The found model instance or null if no model with the specified ID exists.
     */
    fun findById(id: Int): T?

    /**
     * Retrieves all model instances from the data source.
     *
     * @return A list of all model instances.
     */
    fun findAll(): List<T>

    /**
     * Updates an existing model instance in the data source.
     *
     * @param model The model instance with updated fields that should replace the existing one.
     */
    fun update(model: T)

    /**
     * Deletes a model instance by its ID.
     *
     * @param id The ID of the model to be deleted.
     */
    fun delete(id: Int)
}
