package cloud.dmytrominochkin.spring.jdbc

import cloud.dmytrominochkin.spring.jdbc.config.Config
import cloud.dmytrominochkin.spring.jdbc.dao.CustomerDao
import cloud.dmytrominochkin.spring.jdbc.model.Customer
import org.springframework.context.annotation.AnnotationConfigApplicationContext

fun main() {
    val ctx = AnnotationConfigApplicationContext(Config::class.java)

    val customerDao = ctx.getBean("jdbc", CustomerDao::class.java)

    val customer = Customer(6, "Tester Petro", 40)
    customerDao.insert(customer)

    val customer1 = customerDao.findById(1)
    println(customer1)

    println("After update")

    // in this example we are sure it is not null
    // in real life it's better to check
    customer1!!.age = 21
    customerDao.update(customer1)

    val all = customerDao.findAll()
    all.forEach(::println)
    println("--------------------------------------------")
    val customerTemplateDao = ctx.getBean("template", CustomerDao::class.java)

    val customerTemplate = Customer(7, "Tester Vasyl", 42)
    customerTemplateDao.insert(customerTemplate)

    val customerTemplate2 = customerTemplateDao.findById(2)
    println(customerTemplate2)

    println("After update")

    // in this example we are sure it is not null
    // in real life it's better to check
    customerTemplate2!!.age = 26
    customerTemplateDao.update(customerTemplate2)

    val allTemplate = customerTemplateDao.findAll()
    allTemplate.forEach(::println)

}