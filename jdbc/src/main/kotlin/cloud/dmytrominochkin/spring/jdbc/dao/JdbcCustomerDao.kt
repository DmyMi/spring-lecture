package cloud.dmytrominochkin.spring.jdbc.dao

import cloud.dmytrominochkin.spring.jdbc.model.Customer
import javax.sql.DataSource


/**
 * An implementation of the [CustomerDao] interface that uses JDBC (Java Database Connectivity)
 * for data access operations on `Customer` entities stored in a relational database. This class
 * demonstrates how to perform basic CRUD (Create, Read, Update, Delete) operations using JDBC
 * while abstracting the underlying SQL and JDBC boilerplate code.
 *
 * The operations include inserting a new customer, finding a customer by ID, listing all customers,
 * updating a customer's details, and deleting a customer. It leverages the [DataSource] abstraction
 * provided by the JDBC API for connection management, ensuring that connections are properly closed
 * after use to avoid resource leaks.
 *
 * @property dataSource The [DataSource] instance used to obtain connections to the database. It is
 * injected into this DAO implementation to decouple the database connection management from the
 * data access logic.
 */
class JdbcCustomerDao(
    private val dataSource: DataSource
) : CustomerDao {

    /**
     * Inserts a new [Customer] into the database.
     *
     * @param model The [Customer] object to be inserted.
     */
    override fun insert(model: Customer) {
        val sql = "INSERT INTO CUSTOMER (CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_AGE) VALUES (?, ?, ?)"

        dataSource.connection.use { conn ->
            conn.prepareStatement(sql).use { preparedStatement ->
                preparedStatement.run {
                    setInt(1, model.id)
                    setString(2, model.name)
                    setInt(3, model.age)
                }
                preparedStatement.executeUpdate()
            }
        }
    }

    /**
     * Finds and returns a [Customer] by its ID.
     *
     * @param id The ID of the [Customer] to find.
     * @return The [Customer] object if found, or `null` if no customer with the specified ID exists.
     */
    override fun findById(id: Int): Customer? {
        val sql = "SELECT * FROM CUSTOMER WHERE CUSTOMER_ID = ?"

        return dataSource.connection.use { conn ->
            conn.prepareStatement(sql).use { preparedStatement ->
                preparedStatement.run {
                    setInt(1, id)
                }

                preparedStatement.executeQuery().use { resultSet ->
                    if (resultSet.next()) {
                        Customer(
                            id = resultSet.getInt("CUSTOMER_ID"),
                            name = resultSet.getString("CUSTOMER_NAME"),
                            age = resultSet.getInt("CUSTOMER_AGE")
                        )
                    } else {
                        null
                    }
                }
            }
        }
    }

    /**
     * Retrieves and returns a list of all [Customer] objects from the database.
     *
     * @return A list of all [Customer] objects.
     */
    override fun findAll(): List<Customer> {
        val sql = "SELECT * FROM CUSTOMER"

        return dataSource.connection.use { conn ->
            conn.createStatement().use { statement ->
                statement.executeQuery(sql).use { resultSet ->
                    generateSequence {
                        if (resultSet.next()) {
                            Customer(
                                id = resultSet.getInt("CUSTOMER_ID"),
                                name = resultSet.getString("CUSTOMER_NAME"),
                                age = resultSet.getInt("CUSTOMER_AGE")
                            )
                        } else null
                    }.toList()
                }
            }
        }
    }

    /**
     * Updates the details of an existing [Customer] in the database.
     *
     * @param model The [Customer] object containing the updated details.
     */
    override fun update(model: Customer) {
        val sql = "UPDATE CUSTOMER SET CUSTOMER_NAME = ?, CUSTOMER_AGE = ? WHERE CUSTOMER_ID = ?"

        dataSource.connection.use { conn ->
            conn.prepareStatement(sql).use { preparedStatement ->
                preparedStatement.run {
                    setString(1, model.name)
                    setInt(2, model.age)
                    setInt(3, model.id)
                }

                preparedStatement.executeUpdate()
            }
        }
    }

    /**
     * Deletes a [Customer] from the database by its ID.
     *
     * @param id The ID of the [Customer] to be deleted.
     */
    override fun delete(id: Int) {
        val sql = "DELETE FROM CUSTOMER WHERE CUSTOMER_ID = ?"

        dataSource.connection.use { conn ->
            conn.prepareStatement(sql).use { preparedStatement ->
                preparedStatement.run {
                    setInt(1, id)
                }

                preparedStatement.executeUpdate()
            }
        }
    }
}