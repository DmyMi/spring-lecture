package cloud.dmytrominochkin.spring.jdbc.config

import cloud.dmytrominochkin.spring.jdbc.dao.CustomerDao
import cloud.dmytrominochkin.spring.jdbc.dao.JdbcCustomerDao
import cloud.dmytrominochkin.spring.jdbc.dao.JdbcTemplateCustomerDao
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType
import javax.sql.DataSource


@Configuration
open class Config {
    @Bean
    open fun dataSource(): DataSource =
        EmbeddedDatabaseBuilder()
            .setType(EmbeddedDatabaseType.HSQL)
            .addScript("classpath:table.sql")
            .addScript("classpath:data.sql")
            .build()

    @Bean(name = ["jdbc"])
    open fun jdbcDao(dataSource: DataSource): CustomerDao =
        JdbcCustomerDao(dataSource)

    @Bean
    open fun jdbcTemplate(dataSource: DataSource): JdbcTemplate =
        JdbcTemplate(dataSource)

    @Bean(name = ["template"])
    open fun jdbcTemplateDao(jdbcTemplate: JdbcTemplate): CustomerDao =
        JdbcTemplateCustomerDao(jdbcTemplate)
}