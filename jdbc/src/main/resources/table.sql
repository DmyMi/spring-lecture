DROP TABLE CUSTOMER IF EXISTS;
CREATE TABLE CUSTOMER
(
    CUSTOMER_ID   integer identity primary key,
    CUSTOMER_NAME varchar(50) not null,
    CUSTOMER_AGE  integer     not null
);