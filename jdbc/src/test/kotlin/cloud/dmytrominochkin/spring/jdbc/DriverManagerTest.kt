package cloud.dmytrominochkin.spring.jdbc

import cloud.dmytrominochkin.spring.jdbc.model.Customer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.sql.Connection
import java.sql.DriverManager
import kotlin.test.assertEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DriverManagerTest {

    private lateinit var connection: Connection

    @BeforeEach
    fun setup() {
        connection = DriverManager.getConnection("jdbc:hsqldb:mem:mymemdb", "SA", "")
        connection.createStatement().executeUpdate(ddl)
        connection.createStatement().executeUpdate(initialData)
    }

    @Test
    fun insertData() {
        val sql = "INSERT INTO CUSTOMER (CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_AGE) VALUES (?, ?, ?)"

        val result = connection.prepareStatement(sql).use { preparedStatement ->
            preparedStatement.setInt(1, 6)
            preparedStatement.setString(2, "Tester")
            preparedStatement.setInt(3, 42)

            preparedStatement.executeUpdate()
        }

        assertEquals(1, result, "One row added")
    }

    @Test
    fun queryData() {
        val sql = "SELECT * FROM CUSTOMER"
        val result = connection.createStatement().use { statement ->
            statement.executeQuery(sql).use { resultSet ->
                generateSequence {
                    if (resultSet.next()) {
                        Customer(
                            id = resultSet.getInt("CUSTOMER_ID"),
                            name = resultSet.getString("CUSTOMER_NAME"),
                            age = resultSet.getInt("CUSTOMER_AGE")
                        )
                    } else null
                }.toList()
            }
        }

        assertEquals(5, result.size, "5 rows in table")
    }

    @AfterEach
    fun teardown() {
        connection.close()
    }

    companion object {
        const val ddl = """
            DROP TABLE CUSTOMER IF EXISTS;
            CREATE TABLE CUSTOMER
            (
                CUSTOMER_ID   integer identity primary key,
                CUSTOMER_NAME varchar(50) not null,
                CUSTOMER_AGE  integer     not null
            );
        """
        const val initialData = """
            INSERT INTO CUSTOMER VALUES(1,'Vasyl Pupkin', 20);
            INSERT INTO CUSTOMER VALUES(2,'Petro Petrenko', 25);
            INSERT INTO CUSTOMER VALUES(3,'Mustafa Abdul', 30);
            INSERT INTO CUSTOMER VALUES(4,'Jack Black', 60);
            INSERT INTO CUSTOMER VALUES(5,'Ivan Ivanov', 21);
        """
    }
}