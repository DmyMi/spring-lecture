package cloud.dmytrominochkin.spring.mvc.repository

import cloud.dmytrominochkin.spring.mvc.entity.Student
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data repository interface for [Student] entities.
 */
@Repository
interface StudentsRepository : JpaRepository<Student, Long>