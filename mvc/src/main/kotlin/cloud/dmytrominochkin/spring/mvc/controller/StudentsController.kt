package cloud.dmytrominochkin.spring.mvc.controller

import cloud.dmytrominochkin.spring.mvc.entity.Student
import cloud.dmytrominochkin.spring.mvc.repository.StudentsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping


/**
 * Controller for managing student records. This controller handles all web requests
 * for adding, updating, and deleting student entities, as well as listing all students.
 *
 * The controller is mapped to handle requests under the "/students" URL path.
 *
 * Annotations:
 * - [Controller]: Indicates that this class is a Spring MVC controller.
 * - [RequestMapping]: Specifies that all the handler methods in this class
 *   are relative to the "/students" path.
 */
@Controller
@RequestMapping("/students")
class StudentsController @Autowired constructor(
    private val studentsRepository: StudentsRepository
) {
    /**
     * Displays a form to add a new student.
     *
     * @param model The model object to pass data to the view.
     * @return The name of the view template to render the form.
     */
    @GetMapping("form")
    fun studentAddForm(model: Model): String {
        model.addAttribute("student", Student())
        return "students-add"
    }

    /**
     * Displays a list of all students.
     *
     * @param model The model object to pass data to the view.
     * @return The name of the view template to render the list of students.
     */
    @GetMapping(path = ["", "/"])
    fun students(model: Model): String {
        model.addAttribute("students", studentsRepository.findAll())
        return "students"
    }

    /**
     * Handles the submission of the student add form, validates the data, and adds the student to the database.
     *
     * @param student The student data filled from the form.
     * @param bindingResult Contains errors related to form data binding.
     * @param model The model object to pass data to the view.
     * @return The view to redirect to after the student is added.
     */
    @PostMapping("add")
    fun addStudent(student: Student, bindingResult: BindingResult, model: Model): String {
        if (bindingResult.hasErrors()) {
            return "students-add"
        }
        studentsRepository.save(student)
        return "redirect:"
    }

    /**
     * Displays a form to update an existing student identified by id.
     *
     * @param id The unique identifier for the student to update.
     * @param model The model object to pass data to the view.
     * @return The name of the view template to render the update form.
     */
    @GetMapping("edit/{id}")
    fun studentUpdateForm(@PathVariable("id") id: Long, model: Model): String {
        val student = studentsRepository.findById(id).orElseThrow()
        model.addAttribute("student", student)
        return "students-update"
    }

    /**
     * Handles the submission of the student update form, validates the data, and updates the student in the database.
     *
     * @param id The unique identifier of the student to update.
     * @param student The updated student data from the form.
     * @param result Contains errors related to form data binding.
     * @param model The model object to pass data to the view.
     * @return The view to redirect to after the student is updated.
     */
    @PostMapping("update/{id}")
    fun updateStudent(
        @PathVariable("id") id: Long,
        student: Student,
        result: BindingResult,
        model: Model
    ): String {
        if (result.hasErrors()) {
            student.id = id
            return "students-update"
        }

        // update student
        studentsRepository.save(student)

        // get all students ( with update)
        model.addAttribute("students", studentsRepository.findAll())
        return "students"
    }

    /**
     * Handles the deletion of a student identified by id and updates the list of students displayed.
     *
     * @param id The unique identifier of the student to delete.
     * @param model The model object to pass updated data to the view after deletion.
     * @return The name of the view template to render the updated list of students.
     */
    @GetMapping("delete/{id}")
    fun deleteStudent(@PathVariable("id") id: Long, model: Model): String {
        val student = studentsRepository.findById(id).orElseThrow()
        studentsRepository.delete(student)
        model.addAttribute("students", studentsRepository.findAll())
        return "students"
    }
}