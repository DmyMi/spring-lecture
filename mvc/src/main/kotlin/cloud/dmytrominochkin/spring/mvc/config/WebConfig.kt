package cloud.dmytrominochkin.spring.mvc.config

import cloud.dmytrominochkin.spring.mvc.repository.StudentsRepository
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.accept.ContentNegotiationManager
import org.springframework.web.servlet.ViewResolver
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.function.RouterFunction
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.router
import org.thymeleaf.spring6.SpringTemplateEngine
import org.thymeleaf.spring6.templateresolver.SpringResourceTemplateResolver
import org.thymeleaf.spring6.view.ThymeleafViewResolver


/**
 * Configuration class for Spring MVC web application. It defines beans and configuration settings
 * needed for setting up Spring MVC, including view resolution with Thymeleaf and a functional routing
 * API for handling web requests.
 *
 * Annotations:
 * - [Configuration]: Indicates that this class contains bean definitions and configuration settings for the Spring context.
 * - [EnableWebMvc]: Enables default Spring MVC configuration and functionalities, allowing customization through callback
 *   interfaces or by extending the WebMvcConfigurer interface.
 */
@Configuration
@EnableWebMvc
class WebConfig : WebMvcConfigurer {
    /**
     * Configures the template resolver for Thymeleaf, specifying the location of view templates.
     *
     * @param applicationContext The Spring [ApplicationContext], automatically injected by Spring.
     * @return The configured template resolver.
     */
    @Bean
    fun templateResolver(applicationContext: ApplicationContext) = SpringResourceTemplateResolver().apply {
        setApplicationContext(applicationContext) // Link with Spring's ApplicationContext.
        prefix = "classpath:/templates/" // Prefix that points to the location of the Thymeleaf templates.
        suffix = ".html" // Suffix used for the Thymeleaf template files.
    }

    /**
     * Creates and configures the Thymeleaf template engine with the template resolver.
     *
     * @param templateResolver The Thymeleaf template resolver for resolving views.
     * @return The configured [SpringTemplateEngine] instance.
     */
    @Bean
    fun templateEngine(templateResolver: SpringResourceTemplateResolver) = SpringTemplateEngine().apply {
        setTemplateResolver(templateResolver) // Set the template resolver.
        enableSpringELCompiler = true // Enable Spring Expression Language compiler for efficiency.
    }

    /**
     * Configures the view resolver for Thymeleaf, which helps Spring MVC serve HTML content.
     *
     * @param applicationContext The Spring [ApplicationContext], automatically injected by Spring.
     * @param templateEngine The Thymeleaf template engine for processing templates.
     * @return A [ViewResolver] instance for resolving view names to actual views.
     */
    @Bean
    fun viewResolver(applicationContext: ApplicationContext, templateEngine: SpringTemplateEngine): ViewResolver =
        with(ViewResolverRegistry(ContentNegotiationManager(), applicationContext)) {
            val resolver = ThymeleafViewResolver()
            resolver.templateEngine = templateEngine // Link with the Thymeleaf template engine.
            viewResolver(resolver) // Register the resolver.
            return resolver
        }

    /**
     * Defines a RouterFunction bean for handling web requests in a functional style.
     * This demonstrates an alternative to [org.springframework.web.bind.annotation.RequestMapping] methods for configuring routes.
     *
     * @param studentsRepository The repository for accessing student data, automatically injected by Spring.
     * @return A [RouterFunction] for handling specific routes.
     */
    @Bean
    fun studentsFunctional(studentsRepository: StudentsRepository): RouterFunction<*> = router {
        "/fn".nest { // Creates a nested router that maps routes under "/functional".
            GET("/students/{id}") { serverRequest ->
                // Handles GET requests by looking up a student by ID and responding with the student data.
                val studentId = serverRequest.pathVariable("id").toLong()
                studentsRepository.findById(studentId)
                    .map { ok().contentType(MediaType.TEXT_PLAIN).body(it.toString()) }
                    .orElse(notFound().build())
            }
            GET("") {
                // Serves the index view with a predefined list of words.
                val words = listOf("mountain", "noon", "rock", "river", "spring")
                ServerResponse.ok().render("index", mapOf("words" to words))
            }
        }
    }
}