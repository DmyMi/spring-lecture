package cloud.dmytrominochkin.spring.mvc.controller

import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

/**
 * A Spring MVC controller responsible for handling requests to the application's root ("/") URL.
 * This controller demonstrates the use of Spring MVC annotations to define routes and
 * handle HTTP GET requests, serving dynamic content through a view template.
 *
 * Annotations:
 * - [Controller]: Indicates that this class serves the role of a controller in the MVC pattern.
 *   This annotation allows the class to be discovered by Spring during component scanning,
 *   enabling it to handle web requests.
 */
@Controller
class HomeController {
    /**
     * Handles HTTP GET requests to the root ("/") URL, producing a text/html response.
     * This method populates a model with data and selects a view template for rendering the response.
     *
     * The method demonstrates how to:
     * - Define a route with [GetMapping].
     * - Populate a model with data to be used by the view.
     * - Select a view template ("index") for rendering the HTML response.
     *
     * Annotations:
     * - [GetMapping]: Specifies that this method should handle GET requests for the specified path(s).
     *   - value: An array of URL paths that this method should handle. In this case, it's set to handle the root path ("/").
     *   - produces: Specifies the type of the content that this method produces, indicating that this method produces
     *               text/html content, suitable for returning an HTML page.
     *
     * @param model The Model object provided by Spring MVC, used for adding attributes passed to the view.
     * @return A String indicating the name of the view template ("index") to render as a response.
     */
    @GetMapping(value = ["/"], produces = [MediaType.TEXT_HTML_VALUE])
    fun home(model: Model): String {
        // Sample data to be displayed on the view.
        val words = listOf("mountain", "noon", "rock", "river", "spring")

        // Add the sample data to the model, making it accessible to the view template.
        model.addAttribute("words", words)

        // Return the name of the view template to be rendered.
        return "index"
    }
}
