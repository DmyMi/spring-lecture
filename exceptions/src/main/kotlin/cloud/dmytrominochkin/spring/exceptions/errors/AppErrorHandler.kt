package cloud.dmytrominochkin.spring.exceptions.errors

import jakarta.servlet.http.HttpServletRequest
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.ErrorResponse
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.sql.SQLException

/**
 * Global error handler class that provides centralized exception handling across all
 * controllers using Spring's [ControllerAdvice] annotation.
 */
@ControllerAdvice
class AppErrorHandler {

    /**
     * Handles SQLExceptions that might be thrown during database operations.
     * Logs the exception and constructs a detailed error response.
     *
     * @param request The [HttpServletRequest] in which the exception was raised.
     * @param e The caught SQLException.
     * @return [ErrorResponse] representing the error details formatted for the API consumer.
     */
    @ExceptionHandler(SQLException::class)
    fun sampleDatabaseError(request: HttpServletRequest, e: SQLException): ErrorResponse {
        logger.error("Some database error occurred. Request URL: ${request.requestURL}", e)
        return ErrorResponse.create(e, HttpStatus.INTERNAL_SERVER_ERROR, "Database not working.")
    }

    companion object {
        @JvmStatic
        private val logger = LoggerFactory.getLogger(AppErrorHandler::class.java)
    }
}