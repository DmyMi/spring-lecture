package cloud.dmytrominochkin.spring.exceptions.config

import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import java.text.SimpleDateFormat

/**
 * Configuration class for Spring MVC, enabling MVC support and configuring JSON message converters.
 *
 * This class implements the [WebMvcConfigurer] interface to customize the configuration of Spring MVC.
 */
@Configuration
@ComponentScan(basePackages = ["cloud.dmytrominochkin.spring.exceptions"])
@EnableWebMvc
class WebConfig : WebMvcConfigurer {
    /**
     * Configures the JSON message converters for Spring MVC's HTTP messaging.
     * This method sets up a Jackson object mapper for JSON processing, enabling pretty printing,
     * and configuring date format handling.
     *
     * @param converters A mutable list of [HttpMessageConverter] to which custom converters can be added.
     */
    override fun configureMessageConverters(converters: MutableList<HttpMessageConverter<*>>) {
        val builder = Jackson2ObjectMapperBuilder()
            .indentOutput(true)
            .dateFormat(SimpleDateFormat("yyyy-MM-dd"))
            .modulesToInstall(KotlinModule.Builder().build())

        converters
            .add(MappingJackson2HttpMessageConverter(builder.build()))
    }
}