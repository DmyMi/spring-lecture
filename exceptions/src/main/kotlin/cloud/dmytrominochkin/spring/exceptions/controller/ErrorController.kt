package cloud.dmytrominochkin.spring.exceptions.controller

import cloud.dmytrominochkin.spring.exceptions.errors.CustomResourceNotFoundExceptions
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.ErrorResponse
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.sql.SQLException

/**
 * Controller that demonstrates different ways to handle errors within a REST context.
 */
@RestController
class ErrorController {

    /**
     * Endpoint that simulates a database error.
     * Intentionally throws a SQLException to be handled by the global exception handler.
     */
    @GetMapping("advice")
    fun advice(): ResponseEntity<*> {
        throw SQLException("Something went wrong")
    }

    /**
     * Endpoint that simulates an access to a non-existent resource.
     * Throws CustomResourceNotFoundExceptions to be handled globally.
     */
    @GetMapping("custom")
    fun customError(): ResponseEntity<*> {
        throw CustomResourceNotFoundExceptions("No such resource.")
    }

    /**
     * Local exception handling within a controller.
     * Throws IllegalArgumentException which is handled locally by the defined handler.
     */
    @GetMapping("local")
    fun localHandler(): ResponseEntity<*> {
        throw IllegalArgumentException("Data is invalid.")
    }

    /**
     * Handles [IllegalArgumentException] thrown within this controller.
     * Logs the error and returns a formatted error response.
     *
     * @param e The caught [IllegalArgumentException].
     * @return [ErrorResponse] detailing the error and suggesting correction.
     */
    @ExceptionHandler(IllegalArgumentException::class)
    fun handleException(e: IllegalArgumentException): ErrorResponse {
        logger.error("Additional info about error", e)
        return ErrorResponse.create(e, HttpStatus.BAD_REQUEST, "Enter valid data.")
    }

    companion object {
        @JvmStatic
        private val logger = LoggerFactory.getLogger(ErrorController::class.java)
    }
}