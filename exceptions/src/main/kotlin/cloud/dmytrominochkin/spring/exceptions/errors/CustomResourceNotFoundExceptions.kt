package cloud.dmytrominochkin.spring.exceptions.errors

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * Custom exception class to indicate resource not found errors within the application.
 * This exception results in an HTTP 404 response.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Such resource does not exist")
class CustomResourceNotFoundExceptions : RuntimeException {
    constructor() : super()
    constructor(message: String) : super(message)
}