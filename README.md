# Spring Framework and Jakarta EE Demos

This repository is designed as an educational resource for demonstrating key concepts of the Spring Framework and Jakarta EE during lectures. It serves as a practical reference for students, offering insights into the application of these powerful Java frameworks in real-world scenarios.

## Subprojects Overview

The project is organized into multiple subprojects, each focusing on a specific area of the Spring Framework or Jakarta EE. Currently, the repository includes the following subprojects:

### 1. Spring IoC

[The Spring IoC (Inversion of Control)](/ioc) subproject demonstrates the core principles of dependency injection and how Spring's IoC container manages the lifecycle and configuration of application components.

Key Features:
- Configuration using XML, annotations, and Java Config
- Usage of `BeanFactory` and `ApplicationContext`
- Demonstration of bean scopes and lifecycle callbacks

### 2. Spring JDBC

[The Spring JDBC](/jdbc) subproject illustrates the simplification of database access and manipulation using the Spring JDBC module. It provides examples of how to configure a `DataSource`, execute SQL queries, and map results to domain objects without the boilerplate code typically associated with raw JDBC.

Key Features:
- Configuration of a `DataSource` for database connections
- Usage of `JdbcTemplate` for executing queries and updates
- Mapping of SQL results to objects using `RowMapper`

### 3. JPA & Hibernate

[The JPA](/jpa) subproject is designed to demonstrate the basic usage of JPA (Java Persistence API) and Hibernate,
before going forward to using them within the context of a Spring application.

Key Features:
- How to define and use entities in JPA and Hibernate, including annotations and mappings to relational database tables.
- Implementation of the DAO (Data Access Object) pattern to abstract and encapsulate all access to the data source.
- Configuration and usage of transactions to ensure data integrity and consistency.
- Use of JPQL (Java Persistence Query Language), Criteria API, and native SQL for retrieving and manipulating data.

### 4. Spring Data JPA

The [subproject](/data) is designed to demonstrate the use and capabilities of Spring Data JPA within the Spring framework.

Key Features:
- Use of Spring Data JPA repositories for CRUD operations on entities.
- Custom query methods and the `@Query` annotation for complex queries.
- Management of entity relationships (e.g., one-to-many, many-to-one) and the use of `CascadeType.PERSIST` for cascading operations.
- Use of `EntityManager` for more fine-grained control over entities, e.g., for operations not directly supported by repositories.
- Use of the `@Transactional` annotation to ensure data integrity and consistency across business operations.
- Implements a service layer with a base abstract class containing common methods and derived classes showcasing specific features like cascading persist operations and manual entity management.

### 5. Jakarta Servlet API

This [subproject](/servlet) is designed to demonstrate various features of the Jakarta Servlet API, which is a Java specification for interacting with web clients using servlets, that dynamically process requests and construct responses.

Key Features:
- Demonstrates how to define a servlet using annotations or `web.xml` and how to handle GET and POST requests.
- Use of HTTP sessions to maintain state between client requests.
- Management of cookies in web applications.
- Forward requests from one servlet to another to include content from other resources in the response.

### 6. Jakarta Web Components
This [subproject](/webcomp) demonstrates the use of Jakarta Web Components, an integral part of Jakarta EE that allows for the development
of dynamic and interactive web applications.

Key Features:
- Demonstrates the creation and configuration of Servlets to handle client requests and generate dynamic content using JSP.
- Shows how Filters can be used to preprocess and postprocess requests and responses, including examples of logging, authentication, and response modification.
- Examples of using ServletContextListeners, HttpSessionListeners, and other event-driven components for application lifecycle management and session tracking.
- Highlights the creation and use of custom JSP tags to encapsulate reusable functionality and presentation logic, improving code maintainability and separation of concerns.

### 7. Spring Web MVC
This [subproject](/mvc) demonstrates the features of Spring MVC, highlighting both traditional Controllers and the WebMVC.fn framework.

Key Features:
- Examples of annotated classes to handle web requests and deliver content, demonstrating how controllers can be used to manage HTTP requests and return responses directly or through views.
- Demonstrates an alternative approach to routing and handling requests using Spring's functional programming model, WebMVC.fn, which allows for routing configurations to be defined in a more concise and flexible manner.
- Utilizes Thymeleaf as the template engine for rendering HTML views, demonstrating how to integrate it with Spring MVC for dynamic content generation.
- Explains the setup of the `WebConfig` class and the configuration of the `DispatcherServlet`, which is central to the Spring MVC architecture for dispatching requests to the appropriate handlers.

### 8. RESTful Web Services with Spring MVC
This [subproject](/rest) demonstrates how to build RESTful web services using Spring MVC, leveraging both annotation-based controllers and the functional WebMVC.fn configuration.

Key Features:
- Demonstrates how to use controllers with @GetMapping, @PostMapping, @PutMapping, and @DeleteMapping to create REST endpoints.
- Demonstrates the functional configuration route to define HTTP request handlers, providing a clear and concise way to build reactive-style web handlers.
- Uses DTOs to ensure that request and response formats are controlled, demonstrating best practices in separating the internal data model from what is exposed to users.
- Configures Jackson for JSON serialization and deserialization, including custom object mappers for handling complex data types and formatting.

### 9. Spring Security
This [subproject](/security) demonstrates how to implement and customize Spring Security to secure web services.
It demonstrates various security configurations, such as authentication mechanisms, authorization policies, and custom security expressions.

Key Features:
- Demonstrates how to create a custom authentication provider to manage user authentication processes uniquely tailored to specific business requirements.
- Uses annotations like `@PreAuthorize` and `@PostAuthorize` to secure individual service methods based on user roles and conditions.
- Configures custom form login and logout functionalities, illustrating how to manage user sessions in a web application.
- Details the setup of HTTP security rules for protecting web resources and endpoints, including the setup of CSRF protection and the configuration of HTTP Basic Authentication.
- Uses Thymeleaf templates integrated with Spring Security tags to dynamically display content based on the user's authentication status and authority.

Example requests:

* Perform an HTTP POST request passing data URL

```bash
curl -i -X POST \
  --data '{"login": "Patron", "password": "secret"}' \
  -H "Content-Type: application/json" \
  http://localhost:8080/signup
```

* Get data by different users

```bash
curl http://localhost:8080/admin/admin \
  --user admin:admin
```

```bash
curl http://localhost:8080/user/Patron \
  --user Patron:secret
```

### 10. JWT
This [subproject](/jwt) demonstrates the integration of Spring Security with JWT (JSON Web Tokens) for authentication and authorization.
This setup eliminates the need for an external authorization server by handling token issuance directly within the application.

Key Features:
- Demonstrates how to generate JWT tokens directly within the service upon successful authentication.
- Utilize Spring Security to secure API endpoints using JWT for access control.
- Uses stateless authentication to ensure scalability and simplicity in session management.

- Example requests:

* Perform an HTTP POST request passing HTTP Basic Auth for user (`user:password`)

```bash
curl -XPOST user:password@localhost:8080/token
```

The application should respond with a JWT token similar to the following:

```
eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJzZWxmIiwic3ViIjoidXNlciIsImV4cCI6MTYwNDA0MzA1MSwiaWF0IjoxNjA0MDA3MDUxfQ.yDF_JgSwl5sk21CF7AE1AYbYzRd5YYqe3MIgSWpgN0t2UqsjaaEDhmmICKizt-_0iZy8nkEpNnvgqv5bOHDhs7AXlYS1pg8dgPKuyfkhyVIKa3DhuGyb7tFjwJxHpr128BXf1Dbq-p7Njy46tbKsZhP5zGTjdXlqlAhR4Bl5Fxaxr7D0gdTVBVTlUp9DCy6l-pTBpsvHxShkjXJ0GHVpIZdB-c2e_K9PfTW5MDPcHekG9djnWPSEy-fRvKzTsyVFhdy-X3NXQWWkjFv9bNarV-bhxMlzqhujuaeXJGEqUZlkhBxTsqFr1N7XVcmhs3ECdjEyun2fUSge4BoC7budsQ
```

* Perform an HTTP POST request and export the token

```bash
export TOKEN=`curl -XPOST user:password@localhost:8080/token`
```

* Perform an HTTP GET request including the bearer token for authentication:

```bash
curl -H "Authorization: Bearer $TOKEN" localhost:8080 && echo
```

### 11. OAuth2.x / OpenID Connect
This [subproject](/oauth2) illustrates the implementation of OAuth2.x and OpenID Connect (OIDC) protocols
using Spring Security for robust authentication and authorization solutions.
It provides a clear example of setting up an OAuth2 authorization server, securing API resources, and handling client applications using modern security protocols.

Key Features:
- Demonstrates the configuration a Spring Security OAuth2 authorization server to issue access, refresh, and ID tokens.
- Defines and manages client details that can request tokens from the authorization server.
- Shows how to secure API endpoints using OAuth2 tokens to ensure that only authenticated and authorized requests access them.
- Details the customization of JWT tokens with additional claims and configuring JWT validation using public key infrastructure (JWK).
- Leverages Spring Security filters and handlers to integrate OAuth2 and OIDC seamlessly into the Spring ecosystem.

### 12. Logging & Exception handling
This [subproject](/exceptions) showcases the integration of logging capabilities using Log4j2 and Slf4j in a Spring MVC application.
It also demonstrates robust exception handling strategies to manage and respond to various runtime issues effectively.

Key Features:
- Demonstrates the Log4j2 configuration to provide detailed logging across the application, facilitating debugging and monitoring.
- Utilizes the Slf4j logging facade to abstract the logging backend, allowing easy switching between different logging frameworks.
- Shows how to implement Spring MVC's @ControllerAdvice to provide application-wide exception handling.
- Uses @ExceptionHandler methods to return controlled responses for different types of exceptions.

### 13. Spring Boot
This [subproject](/boot) demonstrates the core features of Spring Boot, showcasing how it simplifies the development
of production-ready Spring applications through convention over configuration and automatic setup of commonly used dependencies.

Key Features:
- Demonstrates Spring Boot's auto-configuration capabilities and how to customize them using properties and profiles.
- Shows the implementation of RESTful APIs using Spring Boot's web starter with minimal configuration.
- Utilizes Spring Data JPA for database operations with automatic repository implementations.
- Shows how to use different configuration approaches including environment variables and profiles:

```bash
# Run the application with default configuration
./gradlew bootRun

# Run with a custom word value set through environment variable
EXAMPLE_WORD="hello" ./gradlew bootRun

# Run using Ukrainian localization profile
SPRING_PROFILES_ACTIVE=ukrainian ./gradlew bootRun

# Run using Italian localization profile
SPRING_PROFILES_ACTIVE=italian ./gradlew bootRun
```

## Dependency Management

This project utilizes Gradle version catalogs for efficient and centralized dependency management. This modern approach simplifies the declaration and maintenance of dependencies across multiple subprojects, ensuring consistency and ease of updates.

For detailed configuration, refer to the `gradle/libs.versions.toml` file within the project repository.

## Getting Started

To get started with these demos, clone the repository to your local machine:

```bash
git clone https://gitlab.com/DmyMi/spring-lecture.git
```

Navigate into each subproject directory to find specific instructions on building and running the demos.
