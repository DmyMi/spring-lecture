plugins {
    kotlin("jvm")
    application
}

group = "cloud.dmytrominochkin.spring"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    compileOnly(libs.jakarta.servlet)
    implementation(libs.jetty.webapp)
    implementation(libs.jetty.annotations)
    implementation(libs.bundles.logging)

    testImplementation(libs.kotlin.test)
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(17)
}

application {
    mainClass.set("cloud.dmytrominochkin.spring.servlet.Main")
}