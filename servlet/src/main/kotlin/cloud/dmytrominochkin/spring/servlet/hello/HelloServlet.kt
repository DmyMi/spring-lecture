package cloud.dmytrominochkin.spring.servlet.hello

import jakarta.servlet.annotation.WebServlet
import jakarta.servlet.http.HttpServlet
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse

/**
 * Defines a servlet that responds to HTTP GET requests with a simple "Hello World!" message.
 * Utilizes annotations to declare servlet configuration and overrides methods from the [HttpServlet]
 * to customize its behavior.
 *
 * [HttpServlet] class provided by Jakarta EE simplifies handling HTTP requests by providing
 *  methods for each HTTP method (GET, POST, etc.).
 *
 * [WebServlet]: Marks this class as a servlet and configures its servlet mapping. The [WebServlet.name] attribute
 * specifies the name of the servlet, which can be used for referencing the servlet in other parts of
 * the web application configuration. The [WebServlet.value] attribute defines one or more URL patterns that the
 * servlet will handle, in this case, requests to "/hello".
 */
@WebServlet(name = "helloServlet", value = ["/hello"])
class HelloServlet : HttpServlet() {
    // A lateinit variable to hold a message string that will be initialized in the init method.
    private lateinit var message: String

    /**
     * Initializes the servlet. This method is called by the servlet container when the servlet is first loaded,
     * before handling any requests.
     */
    override fun init() {
        // Initialize the message variable with a string. This could be extended to perform more complex
        // initialization logic, such as configuring resources needed by the servlet.
        message = "Hello World!"
    }

    /**
     * Handles HTTP GET requests. This method is called by the servlet container to allow the servlet to respond
     * to a GET request.
     *
     * @param request The HttpServletRequest object that contains the request the client made to the servlet.
     * @param response The HttpServletResponse object that contains the response the servlet sends to the client.
     */
    public override fun doGet(request: HttpServletRequest, response: HttpServletResponse) {
        // Sets the content type of the response to "text/html", indicating that the servlet will return HTML.
        response.contentType = "text/html"

        // Retrieves a PrintWriter object that can send character text to the client. Used here to write HTML content.
        val out = response.writer

        // Using the PrintWriter to write HTML content to the response. This demonstrates sending a simple
        // HTML page that includes the "Hello World!" message.
        out.println("<html><body>")
        out.println("<h1>$message</h1>")
        out.println("</body></html>")
    }

    /**
     * Called by the servlet container to indicate to a servlet that the servlet is being taken out of service.
     * This is the opportunity to clean up any resources (like closing database connections) before the servlet
     * is destroyed. This implementation is empty because there are no resources to release.
     */
    override fun destroy() {
    }
}
