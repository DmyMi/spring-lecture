package cloud.dmytrominochkin.spring.servlet.hello

import jakarta.servlet.annotation.WebServlet
import jakarta.servlet.http.HttpServlet
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse

/**
 * A servlet that demonstrates a simple use case of handling HTTP requests and using the request dispatcher
 * to forward these requests within the server-side context.
 */
@WebServlet(name = "ajaxServlet", value = ["/ajax"])
class AjaxServlet : HttpServlet() {
    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        // Obtain a RequestDispatcher for "/ajax.html". This object allows the servlet to forward
        // the request to another resource within the application, abstracting the internal redirection.
        val requestDispatcher = req.getRequestDispatcher("/ajax.html")

        // Uses the RequestDispatcher to forward the incoming request and its response to "/ajax.html".
        // This effectively delegates the response handling to the specified resource, making it possible
        // to separate the servlet logic from the presentation layer or static content delivery.
        requestDispatcher.forward(req, resp)
    }
}
