package cloud.dmytrominochkin.spring.servlet.form

import jakarta.servlet.annotation.WebServlet
import jakarta.servlet.http.HttpServlet
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse

/**
 * A servlet dedicated to handling user registration. It supports both GET and POST requests to
 * accommodate form submission via query parameters (GET) or form data (POST). The servlet
 * demonstrates basic form processing and response generation in a web application context.
 */
@WebServlet(name = "RegisterServlet", value = ["/register"])
class RegisterServlet : HttpServlet() {
    /**
     * Handles GET requests. This method is invoked for requests made with the GET method, often including
     * query parameters as part of the URL. It delegates processing to a shared method to handle both GET
     * and POST requests uniformly.
     *
     * @param request Provides request information for HTTP servlets.
     * @param response Provides HTTP-specific functionality in sending a response.
     */
    override fun doGet(request: HttpServletRequest, response: HttpServletResponse) = processRequest(request, response)

    /**
     * Handles POST requests. This method is called for requests made using the POST method, typically containing
     * form data sent as part of the request body. Like doGet, it forwards processing to a common method.
     *
     * @param request HttpServletRequest object containing client request data.
     * @param response HttpServletResponse object for crafting the servlet's response.
     */
    override fun doPost(request: HttpServletRequest, response: HttpServletResponse) = processRequest(request, response)

    /**
     * Processes both GET and POST requests by extracting form data, performing some operation (e.g., saving data to a database),
     * and generating a response.
     *
     * @param request HttpServletRequest object, providing access to request information.
     * @param response HttpServletResponse object, used to formulate the response.
     */
    private fun processRequest(request: HttpServletRequest, response: HttpServletResponse) {
        // Extract "name" and "mail" parameters from the request. These would typically come from form fields submitted by the user.
        val name = request.getParameter("name")
        val mail = request.getParameter("mail")

        // Obtain a PrintWriter from the response object to write response content.
        val out = response.writer

        // Here you might insert logic to save the registration information to a database.

        // Generate and send a confirmation message as the response. Demonstrates simple text output to the client.
        out.println("Thank you for registration, $name ($mail)")
    }
}
