package cloud.dmytrominochkin.spring.servlet.cookie

import jakarta.servlet.annotation.WebServlet
import jakarta.servlet.http.Cookie
import jakarta.servlet.http.HttpServlet
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse

/**
 * A servlet that demonstrates the use of HTTP cookies to maintain state across multiple requests.
 * Each time a GET request is made to this servlet, it increments a counter stored in a cookie.
 */
@WebServlet(name = "cookieServlet", value = ["/cookie"])
class CookieServlet : HttpServlet() {
    /**
     * Handles HTTP GET requests by reading, updating, and saving a counter value in a cookie.
     *
     * @param request The HttpServletRequest object containing the client's request.
     * @param response The HttpServletResponse object for the servlet's response.
     */
    override fun doGet(request: HttpServletRequest, response: HttpServletResponse) {
        // Initialize the sum variable to hold the sum of numbers sent in requests.
        var sum = 0

        // Retrieve an array of Cookies from the request.
        val cookies = request.cookies

        // Check if the cookie array is not null or empty, then search for a specific cookie by name.
        if (cookies.isNullOrEmpty().not()) {
            cookies.find { cookie -> cookie.name == "CookieServlet.sum" }?.let { found ->
                // If found, parse its value to an integer and assign it to sum.
                sum = found.value.toInt()
            }
        }

        // Increment the sum by 1 for each request.
        sum += 1

        // Create a new Cookie to save the updated sum. The cookie's name is "CookieServlet.sum", and its value is the updated sum.
        val cookie = Cookie("CookieServlet.sum", sum.toString()).apply {
            // Set the maximum age of the cookie to 30 minutes (30 * 60 seconds).
            maxAge = 30 * 60
        }

        // Add the cookie to the response, instructing the client to store/update the cookie.
        response.addCookie(cookie)

        // Obtain a writer to send text data to the client.
        val out = response.writer

        // Write a simple message to indicate that the counter has been updated.
        out.println("counted")
    }
}