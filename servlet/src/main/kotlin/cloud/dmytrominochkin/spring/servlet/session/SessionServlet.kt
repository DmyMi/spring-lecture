package cloud.dmytrominochkin.spring.servlet.session

import jakarta.servlet.annotation.WebServlet
import jakarta.servlet.http.HttpServlet
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse

/**
 * A servlet that demonstrates the use of HTTP sessions to maintain state across multiple requests.
 * The servlet calculates the sum of numbers provided in query parameters across requests within the same session.
 *
 * Invocation Instructions:
 * 1. Use `curl -v http://localhost:8080/session\?number\=1` to send a request with a number as a query parameter.
 *    You'll receive a JSESSIONID cookie in the response. This initiates or continues a session.
 * 2. Copy the JSESSIONID value from the response.
 * 3. To continue the session and send another number, use:
 *    `curl -v http://localhost:8080/session\?number\=1 --cookie "JSESSIONID=your_session_id_here"`
 */
@WebServlet(name = "sessionServlet", value = ["/session"])
class SessionServlet : HttpServlet() {
    /**
     * Handles HTTP GET requests by managing a session-based sum calculation.
     *
     * @param request The HttpServletRequest object containing the client's request.
     * @param response The HttpServletResponse object for the servlet's response.
     */
    override fun doGet(request: HttpServletRequest, response: HttpServletResponse) {
        // Request or create a new HTTP session. The `true` parameter ensures that a new session is created if none exists.
        val session = request.getSession(true)

        // Retrieve the 'number' query parameter from the request and convert it to an integer.
        val number = request.getParameter("number").toInt()

        // Initialize or retrieve the current sum stored in the session. If the session is new, start from 0.
        var sum = if (session.isNew) {
            0
        } else {
            // Retrieve the sum from the session if it exists.
            session.getAttribute("SessionServlet.sum") as Int
        }

        // Add the provided number to the current sum.
        sum += number

        // Store the updated sum back into the session for future requests.
        session.setAttribute("SessionServlet.sum", sum)

        // If the provided number is 0, invalidate the session, effectively resetting the sum.
        if (number == 0) {
            session.invalidate()
        }

        // Obtain a writer to send data back to the client.
        val out = response.writer

        // Respond to the client with the current sum.
        out.println("Counted: $sum")
    }
}
