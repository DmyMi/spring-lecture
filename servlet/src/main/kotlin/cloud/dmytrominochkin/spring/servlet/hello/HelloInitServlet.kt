package cloud.dmytrominochkin.spring.servlet.hello

import jakarta.servlet.http.HttpServlet
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse

/**
 * A servlet that responds to HTTP GET requests by displaying a message initialized from servlet configuration parameters.
 * This servlet demonstrates how to access initialization parameters defined in `web.xml` or through annotations.
 */
class HelloInitServlet : HttpServlet() {
    // A lateinit variable to hold the message to be displayed. The use of lateinit indicates the variable will be
    // initialized before it's used, specifically in the init method.
    private lateinit var message: String

    /**
     * Initializes the servlet. Overrides the init method to perform initial setup, retrieving the "message"
     * initialization parameter from the servlet's configuration.
     */
    override fun init() {
        // Retrieve the ServletConfig object, which contains initialization and startup parameters for this servlet.
        val config = servletConfig
        // Using the ServletConfig object to retrieve the "message" parameter value specified during the servlet's
        // configuration phase. This demonstrates a dynamic way to set properties of the servlet based on configuration.
        message = config.getInitParameter("message")
    }

    /**
     * Responds to HTTP GET requests. Overrides doGet to provide custom handling of GET requests, returning
     * an HTML page that includes a configurable message.
     *
     * @param request The HttpServletRequest object containing the client's request.
     * @param response The HttpServletResponse object for the servlet's response.
     */
    public override fun doGet(request: HttpServletRequest, response: HttpServletResponse) {
        // Set the MIME type of the response body to "text/html". This informs the client that the response
        // content should be interpreted as HTML.
        response.contentType = "text/html"

        // Obtains a PrintWriter object to send character text to the client. Used here to write HTML content.
        val out = response.writer
        // Writing HTML content to the response, demonstrating how the dynamically configured message is included
        // in the response content sent to the client.
        out.println("<html><body>")
        out.println("<h1>$message</h1>")
        out.println("</body></html>")
    }

    /**
     * Called by the servlet container to indicate to a servlet that the servlet is being taken out of service.
     * This is the opportunity to clean up any resources (like closing database connections) before the servlet
     * is destroyed. This implementation is empty because there are no resources to release.
     */
    override fun destroy() {
    }
}
