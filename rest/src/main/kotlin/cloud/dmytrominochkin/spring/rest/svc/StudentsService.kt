package cloud.dmytrominochkin.spring.rest.svc

import cloud.dmytrominochkin.spring.rest.dto.StudentRequest
import cloud.dmytrominochkin.spring.rest.dto.StudentResponse

/**
 * Defines the service layer interface for managing students within the application.
 * This interface provides methods to perform CRUD (Create, Read, Update, Delete) operations on student entities,
 * abstracting the business logic and data access mechanisms behind managing students.
 *
 * Each method is designed to interact with student data, transforming input request data into response forms,
 * and ensuring business rules and validations are enforced.
 */
interface StudentsService {
    /**
     * Retrieves a list of all students in the system.
     *
     * @return A list of [StudentResponse] objects, each representing a student stored in the database.
     */
    fun getAll(): List<StudentResponse>

    /**
     * Retrieves a single student by their unique identifier.
     *
     * @param id The unique ID of the student to be retrieved.
     * @return The [StudentResponse] object corresponding to the student.
     * @throws NoSuchElementException If no student with the given ID exists.
     */
    fun getById(id: Long): StudentResponse

    /**
     * Adds a new student to the system.
     *
     * @param student A [StudentRequest] object containing the information needed to create a new student.
     * @return A [StudentResponse] object representing the newly created student, including their generated ID.
     */
    fun add(student: StudentRequest): StudentResponse

    /**
     * Updates an existing student's information.
     *
     * @param id The unique identifier of the student to update.
     * @param student A [StudentRequest] containing the updated data for the student.
     * @return A [StudentResponse] object representing the updated student data.
     * @throws NoSuchElementException If no student with the given ID exists.
     */
    fun update(
        id: Long,
        student: StudentRequest
    ): StudentResponse

    /**
     * Deletes a student from the system by their ID.
     *
     * @param id The unique identifier of the student to be deleted.
     * @return A [StudentResponse] object that was deleted.
     * @throws NoSuchElementException If no student with the given ID exists.
     */
    fun delete(id: Long): StudentResponse
}
