package cloud.dmytrominochkin.spring.rest.entity

import jakarta.persistence.*

/**
 * Represents a student entity in the database.
 *
 * This class is annotated with JPA annotations to describe the table mapping, column details,
 * and primary key generation strategy. It uses Kotlin's data class feature to automatically
 * provide functionality like [equals], [hashCode], and [toString] based on the primary fields.
 *
 */
@Entity
@Table(name = "students")
data class Student(
    @Column(name = "name")
    var name: String = "",
    @Column(name = "email")
    var email: String = "",
    @Column(name = "phone")
    var phone: String = "",
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0,
)