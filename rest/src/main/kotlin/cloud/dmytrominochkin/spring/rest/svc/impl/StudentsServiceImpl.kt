package cloud.dmytrominochkin.spring.rest.svc.impl

import cloud.dmytrominochkin.spring.rest.dto.StudentRequest
import cloud.dmytrominochkin.spring.rest.dto.StudentResponse
import cloud.dmytrominochkin.spring.rest.entity.Student
import cloud.dmytrominochkin.spring.rest.repository.StudentsRepository
import cloud.dmytrominochkin.spring.rest.svc.StudentsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class StudentsServiceImpl @Autowired constructor(
    private val studentsRepository: StudentsRepository
) : StudentsService {
    override fun getAll(): List<StudentResponse> = studentsRepository.findAll().map(::mapFromEntityToDto)

    override fun getById(id: Long): StudentResponse =
        studentsRepository.findById(id).map(::mapFromEntityToDto).orElseThrow()

    override fun add(student: StudentRequest): StudentResponse {
        val studentEntity = Student(
            student.name,
            student.email,
            student.phone
        )
        studentsRepository.save(studentEntity)
        return mapFromEntityToDto(studentEntity)
    }

    override fun update(id: Long, student: StudentRequest): StudentResponse {
        val studentEntity = studentsRepository.findById(id).orElseThrow()
        studentEntity.name = student.name
        studentEntity.email = student.email
        studentEntity.phone = student.phone
        studentsRepository.save(studentEntity)
        return mapFromEntityToDto(studentEntity)
    }

    override fun delete(id: Long): StudentResponse {
        val studentEntity = studentsRepository.findById(id).orElseThrow()
        studentsRepository.delete(studentEntity)
        return mapFromEntityToDto(studentEntity)
    }

    /**
     * Converts a [Student] entity into a [StudentResponse] DTO.
     *
     * @param entity The [Student] entity to convert.
     * @return A [StudentResponse] containing the data from the entity.
     */
    private fun mapFromEntityToDto(entity: Student) = StudentResponse(
        entity.name,
        entity.email,
        entity.phone,
        entity.id
    )
}