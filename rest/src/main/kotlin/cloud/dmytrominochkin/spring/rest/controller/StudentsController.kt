package cloud.dmytrominochkin.spring.rest.controller

import cloud.dmytrominochkin.spring.rest.dto.StudentRequest
import cloud.dmytrominochkin.spring.rest.dto.StudentResponse
import cloud.dmytrominochkin.spring.rest.svc.StudentsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

/**
 * REST controller for managing students.
 * This controller handles API requests for the student data management,
 * providing endpoints for CRUD operations.
 *
 * It uses Spring MVC annotations to map HTTP requests to handler methods and to define routing and input/output handling.
 *
 * @property studentsService The service used to perform business logic operations on students.
 */
@RestController
@RequestMapping("/students")
class StudentsController @Autowired constructor(
    private val studentsService: StudentsService
) {
    /**
     * Retrieves all students.
     *
     * @return A list of [StudentResponse] representing all students currently in the system.
     */
    @GetMapping(path = ["", "/"])
    fun students(): List<StudentResponse> = studentsService.getAll()

    /**
     * Retrieves a specific student by their ID.
     *
     * @param id The ID of the student to retrieve.
     * @return A [ResponseEntity] containing [StudentResponse] if found, or an HTTP 404 error if not found.
     */
    @GetMapping("{id}")
    fun getStudent(@PathVariable("id") id: Long): ResponseEntity<StudentResponse> =
        try {
            ResponseEntity.ok(studentsService.getById(id))
        } catch (e: NoSuchElementException) {
            ResponseEntity.notFound().build()
        }

    /**
     * Adds a new student to the system.
     *
     * Calling example: curl -X POST --data '{"name": "Patron", "email": "a@a.com", "phone": "1234567"}'
     *
     * @param student The [StudentRequest] containing the data needed to create a new student.
     * @return A [StudentResponse] representing the newly added student.
     */
    @PostMapping(path = ["", "/"])
    fun addStudent(@RequestBody student: StudentRequest): StudentResponse = studentsService.add(student)

    /**
     * Updates an existing student identified by ID.
     *
     * @param id The ID of the student to update.
     * @param student The [StudentRequest] containing updated information for the student.
     * @return A [StudentResponse] representing the updated student.
     */
    @PutMapping("{id}")
    fun updateStudent(
        @PathVariable("id") id: Long,
        @RequestBody student: StudentRequest,
    ): StudentResponse = studentsService.update(id, student)

    /**
     * Deletes an existing student by their ID.
     *
     * @param id The ID of the student to be deleted.
     * @return A [StudentResponse] representing the deleted student.
     */
    @DeleteMapping("{id}")
    fun deleteStudent(@PathVariable("id") id: Long): StudentResponse = studentsService.delete(id)
}