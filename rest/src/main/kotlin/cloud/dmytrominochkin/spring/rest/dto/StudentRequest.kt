package cloud.dmytrominochkin.spring.rest.dto

/**
 * Represents a request payload for student information.
 *
 * This data class is typically used to receive student data from web forms or API requests where a student
 * is being created or updated. It includes essential fields that can be submitted by the user.
 *
 * @property name The name of the student to be created or updated.
 * @property email The email address of the student, which should be unique and valid.
 * @property phone The phone number of the student, formatted according to local or international standards.
 */
data class StudentRequest(
    var name: String,
    var email: String,
    var phone: String
)
