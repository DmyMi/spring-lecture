package cloud.dmytrominochkin.spring.rest.config

import cloud.dmytrominochkin.spring.rest.dto.StudentRequest
import cloud.dmytrominochkin.spring.rest.svc.StudentsService
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.function.RouterFunction
import org.springframework.web.servlet.function.body
import org.springframework.web.servlet.function.router
import java.text.SimpleDateFormat

/**
 * Configuration class for Spring MVC, enabling MVC support and configuring JSON message converters
 * and functional endpoints for handling HTTP requests related to students.
 *
 * This class implements the [WebMvcConfigurer] interface to customize the configuration of Spring MVC.
 */
@Configuration
@EnableWebMvc
class WebConfig : WebMvcConfigurer {

    /**
     * Configures the JSON message converters for Spring MVC's HTTP messaging.
     * This method sets up a Jackson object mapper for JSON processing, enabling pretty printing,
     * and configuring date format handling.
     *
     * @param converters A mutable list of [HttpMessageConverter] to which custom converters can be added.
     */
    override fun configureMessageConverters(converters: MutableList<HttpMessageConverter<*>>) {
        val builder = Jackson2ObjectMapperBuilder()
            .indentOutput(true)
            .dateFormat(SimpleDateFormat("yyyy-MM-dd"))
            .modulesToInstall(KotlinModule.Builder().build())

        converters
            .add(MappingJackson2HttpMessageConverter(builder.build()))
    }

    /**
     * Defines a RouterFunction bean that handles routes related to student operations using functional endpoints.
     * This functional approach is an alternative to the annotation-based controllers, providing a more explicit and fluent route configuration.
     *
     * @param studentsService The service layer used to perform business logic operations on students.
     * @return A [RouterFunction] configuring the routes and their handling methods.
     */
    @Bean
    fun studentsFunctional(studentsService: StudentsService): RouterFunction<*> = router {
        // Parent path for all functional endpoints
        "/fn".nest {
            // Nested path specifically for student resources
            "/students".nest {
                // Handle DELETE requests for a student by ID
                DELETE("/{id}") { serverRequest ->
                    val studentId = serverRequest.pathVariable("id").toLong()
                    // Call service to delete student and return the result
                    ok().body(studentsService.delete(studentId))
                }
                // Handle PUT requests to update a student
                PUT("/{id}") { serverRequest ->
                    val studentId = serverRequest.pathVariable("id").toLong()
                    // Automatically deserialize body to StudentRequest
                    val student = serverRequest.body<StudentRequest>()
                    // Update student and return updated info
                    ok().body(studentsService.update(studentId, student))
                }
                // Handle GET requests to fetch a student by ID
                GET("/{id}") { serverRequest ->
                    val studentId = serverRequest.pathVariable("id").toLong()
                    try {
                        // Fetch and return student details
                        ok().body(studentsService.getById(studentId))
                    } catch (e: NoSuchElementException) {
                        // Handle the case where the student is not found
                        notFound().build()
                    }
                }
                // Handle POST requests to add a new student
                // curl -X POST --data '{"name": "Patron", "email": "a@a.com", "phone": "1234567"}'
                POST("") { serverRequest ->
                    // Deserialize request body to StudentRequest
                    val student = serverRequest.body<StudentRequest>()
                    // Add student and return the newly added student info
                    ok().body(studentsService.add(student))
                }
                // Handle GET requests to list all students
                GET("") {
                    // Retrieve and return all students
                    ok().body(studentsService.getAll())
                }
            }

        }
    }
}