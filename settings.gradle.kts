plugins {
    id("org.gradle.toolchains.foojay-resolver-convention") version "0.9.0"
}
rootProject.name = "spring-lecture"
include(
    "ioc",
    "jdbc",
    "jpa",
    "data",
    "servlet",
    "webcomp",
    "mvc",
    "rest",
    "security",
    "jwt",
    "oauth2",
    "exceptions",
    "boot"
)

// The version catalog is now managed in gradle/libs.versions.toml
