plugins {
    kotlin("jvm")
    application
}

group = "cloud.dmytrominochkin.spring"
version = "1.0-SNAPSHOT"

dependencies {
    implementation(libs.spring.ioc)
    testImplementation(libs.kotlin.test)
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(17)
}

application {
    mainClass.set("cloud.dmytrominochkin.spring.ioc.AnnotationMainKt")
}
