package cloud.dmytrominochkin.spring.ioc.svc

/**
 * An interface defining a service for validating and converting digit-containing strings to a standardized format.
 */
interface ConvertService {

    /**
     * Validates the digits contained in the provided text.
     *
     * This function checks the input string for digit compliance according to a specific rule set
     * defined by the implementation. It might modify the input text to correct errors or simply
     * validate its content without modification, depending on the implementation.
     *
     * @param text The input text containing digits to be validated.
     * @return A string that represents the validated (and possibly modified) text.
     *         The exact nature of the modifications, if any, depends on the implementation.
     */
    fun validateDigits(text: String): String
}
