package cloud.dmytrominochkin.spring.ioc

import cloud.dmytrominochkin.spring.ioc.svc.EuclideanService
import org.springframework.context.support.ClassPathXmlApplicationContext

/**
 * Initializes and utilizes a ClassPathXmlApplicationContext with a specified configuration XML file.
 * Demonstrates retrieving an EuclideanService bean by type (and optionally by name) to calculate the GCD of two numbers.
 */
fun main() {
    val xmlConfigContext = ClassPathXmlApplicationContext("config.xml")
    val euclideanService = xmlConfigContext.getBean(EuclideanService::class.java)
    // or by name
    // val euclideanService = annotationConfigContext.getBean("EuclideanService")
    xmlConfigContext.close()
    println("GCD(18, 30) = ${euclideanService.calculateGCD("18", "30")}")
}