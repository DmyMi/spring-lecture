package cloud.dmytrominochkin.spring.ioc.svc.impl

import cloud.dmytrominochkin.spring.ioc.svc.DigitalService
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import kotlin.math.abs

/**
 * A Spring component that implements [DigitalService], providing digital conversion functionalities.
 * Instances of this class are created as prototypes, meaning a new instance will be created each time
 * it is requested from the Spring context.
 *
 * This implementation focuses on converting a string argument to its integer representation, ensuring
 * that the result is always non-negative by applying an absolute value operation.
 */
@Component
@Scope("prototype")
class DigitalServiceImpl : DigitalService {

    /**
     * Converts a given string argument to an integer. If the string does not represent a valid integer,
     * an [IllegalArgumentException] is thrown with a detailed message. The function guarantees
     * that the returned integer is non-negative by returning the absolute value of the conversion result.
     *
     * @param arg The string argument to be converted into an integer.
     * @return The absolute integer value of the argument.
     * @throws IllegalArgumentException If the argument cannot be converted into an integer, including
     * cases where the argument is null, empty, or non-numeric.
     */
    override fun toInt(arg: String): Int {
        val result = try {
            arg.toInt()
        } catch (e: NumberFormatException) {
            throw IllegalArgumentException("Invalid argument ${arg}, message $e")
        }
        return abs(result)
    }
}
