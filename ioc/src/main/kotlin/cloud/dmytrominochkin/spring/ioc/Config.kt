package cloud.dmytrominochkin.spring.ioc

import cloud.dmytrominochkin.spring.ioc.svc.impl.ConvertServiceImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration


/**
 * Configuration class for Spring's ApplicationContext. It defines the component scanning base package
 * and bean definitions for the application.
 */
@Configuration
@ComponentScan("cloud.dmytrominochkin.spring.ioc.svc")
open class Config {
    /**
     * Defines a bean for the ConvertService using a factory method from the ConvertServiceImpl.
     *
     * @return An instance of ConvertService.
     */
    @Bean(name = ["convertService"])
    open fun createConvertService() = ConvertServiceImpl.convertService()
}