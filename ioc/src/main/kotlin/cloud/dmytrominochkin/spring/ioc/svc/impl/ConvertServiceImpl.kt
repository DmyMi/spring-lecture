package cloud.dmytrominochkin.spring.ioc.svc.impl

import cloud.dmytrominochkin.spring.ioc.svc.ConvertService

/**
 * An implementation of [ConvertService] that provides functionality for validating and converting
 * digit-containing strings to a standardized format.
 *
 * This implementation removes any non-digit characters from the input string if the string contains
 * a mix of digit and non-digit characters. If the input string is empty or contains only non-digit
 * characters, it returns "0".
 *
 * This class cannot be instantiated directly and must be accessed through its companion object.
 */
class ConvertServiceImpl private constructor() : ConvertService {

    /**
     * A regular expression pattern matching non-digit characters.
     */
    private val notDigit = "\\D"

    /**
     * A compiled regular expression pattern used to check if the input text contains sequences of
     * non-digits and digits.
     */
    private val patternNotDigitsExist = "([^\\d]+\\d+[^\\d]+)+|(\\d+[^\\d]+)+|([^\\d]+\\d+)+".toRegex()

    /**
     * Validates and converts the input text by removing all non-digit characters if any digit
     * characters are present. If the resulting string is empty, it defaults to "0".
     *
     * @param text The input text to be validated and converted.
     * @return A string containing only digits or "0" if the input is empty or contains only non-digit characters.
     */
    override fun validateDigits(text: String): String {
        var result = text
        if (patternNotDigitsExist.matches(text)) {
            result = text.replace(notDigit.toRegex(), "")
        }
        return result.ifEmpty { "0" }
    }

    companion object {
        /**
         * Provides a static method to access an instance of [ConvertService], ensuring that
         * the implementation details are encapsulated within the [ConvertServiceImpl] class.
         *
         * @return An instance of [ConvertService] ready to be used for digit validation and conversion.
         */
        @JvmStatic
        fun convertService(): ConvertService = ConvertServiceImpl()
    }
}
