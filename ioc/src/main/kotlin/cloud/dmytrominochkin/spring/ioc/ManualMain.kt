package cloud.dmytrominochkin.spring.ioc

import cloud.dmytrominochkin.spring.ioc.svc.DigitalService
import cloud.dmytrominochkin.spring.ioc.svc.EuclideanService
import cloud.dmytrominochkin.spring.ioc.svc.OperationService
import cloud.dmytrominochkin.spring.ioc.svc.OrderService
import cloud.dmytrominochkin.spring.ioc.svc.impl.ConvertServiceImpl
import cloud.dmytrominochkin.spring.ioc.svc.impl.DigitalServiceImpl
import cloud.dmytrominochkin.spring.ioc.svc.impl.EuclideanServiceImpl
import cloud.dmytrominochkin.spring.ioc.svc.impl.OrderServiceImpl
import cloud.dmytrominochkin.spring.ioc.svc.impl.op.ModOperationService


/**
 * Demonstrates manual dependency injection without using Spring's dependency injection framework.
 * It manually creates instances of the required services and injects them into an instance of EuclideanService.
 */
fun main() {
    val digitalService: DigitalService = DigitalServiceImpl()
    val operationService: OperationService = ModOperationService(digitalService)
    val orderService: OrderService = OrderServiceImpl(digitalService)
    val euclideanService: EuclideanService = EuclideanServiceImpl(operationService, ConvertServiceImpl.convertService(), orderService)
    println("GCD(18, 30) = ${euclideanService.calculateGCD("18a", "30")}")
}