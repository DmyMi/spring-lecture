package cloud.dmytrominochkin.spring.ioc.svc.impl.op

import cloud.dmytrominochkin.spring.ioc.svc.DigitalService
import cloud.dmytrominochkin.spring.ioc.svc.OperationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component

/**
 * A Spring component that implements [OperationService], specifically performing modulo operations
 * on numerical values represented as strings. This service is qualified with "ModOperationService"
 * to distinguish it among other [OperationService] implementations when autowiring.
 *
 * It relies on [DigitalService] to convert string arguments to integers before performing the
 * modulo operation.
 */
@Component
@Qualifier("ModOperationService")
class ModOperationService @Autowired constructor(
    private val digitalService: DigitalService
) : OperationService {

    /**
     * Performs a modulo operation on two string arguments after converting them to integers.
     *
     * This method uses [DigitalService] to safely convert the string arguments to integers and then
     * calculates their modulus. The result of the modulo is returned as a string.
     *
     * @param arg0 The first string argument to be converted and processed.
     * @param arg1 The second string argument to be converted and processed.
     * @return The result of the modulo operation as a string.
     */
    override fun operation(arg0: String, arg1: String) =
        "${digitalService.toInt(arg0) % digitalService.toInt(arg1)}"
}