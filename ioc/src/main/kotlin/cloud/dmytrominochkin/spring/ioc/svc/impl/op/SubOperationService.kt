package cloud.dmytrominochkin.spring.ioc.svc.impl.op

import cloud.dmytrominochkin.spring.ioc.svc.DigitalService
import cloud.dmytrominochkin.spring.ioc.svc.OperationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component

/**
 * A Spring component that implements [OperationService], specifically performing subtraction operations
 * on numerical values represented as strings. This service is qualified with "SubOperationService"
 * to distinguish it among other [OperationService] implementations when autowiring.
 *
 * It relies on [DigitalService] to convert string arguments to integers before performing the
 * subtraction operation.
 */
@Component
@Qualifier("SubOperationService")
class SubOperationService @Autowired constructor(
    private val digitalService: DigitalService
) : OperationService {

    /**
     * Performs a subtraction operation on two string arguments after converting them to integers.
     *
     * This method uses [DigitalService] to safely convert the string arguments to integers and then
     * calculates their difference. The result of the subtraction is returned as a string.
     *
     * @param arg0 The first string argument to be converted and subtracted.
     * @param arg1 The second string argument to be converted and subtracted.
     * @return The result of the subtraction operation as a string.
     */
    override fun operation(arg0: String, arg1: String) =
        "${digitalService.toInt(arg0) - digitalService.toInt(arg1)}"
}