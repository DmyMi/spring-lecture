package cloud.dmytrominochkin.spring.ioc.svc.impl

import cloud.dmytrominochkin.spring.ioc.svc.DigitalService
import cloud.dmytrominochkin.spring.ioc.svc.OrderService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * A Spring component that implements [OrderService], providing operations to determine the maximum,
 * minimum, and zero-value conditions of given string arguments based on their numeric values.
 *
 * This service relies on [DigitalService] to convert string arguments into integers for comparison.
 * The [DigitalService] is automatically injected into this class by Spring's dependency injection
 * mechanism.
 */
@Component
class OrderServiceImpl @Autowired constructor(
    private var digitalService: DigitalService
) : OrderService {

    /**
     * Determines the maximum of two string arguments based on their numeric values.
     *
     * This method compares the numeric value of two strings and returns the string that represents
     * the higher numeric value. If the numeric values are equal, the first argument is returned.
     *
     * @param arg0 The first string argument to compare.
     * @param arg1 The second string argument to compare.
     * @return The string argument that represents the higher numeric value.
     */
    override fun max(arg0: String, arg1: String): String {
        return if (digitalService.toInt(arg0) > digitalService.toInt(arg1)) arg0 else arg1
    }

    /**
     * Determines the minimum of two string arguments based on their numeric values.
     *
     * This method compares the numeric value of two strings and returns the string that represents
     * the lower numeric value. If the numeric values are equal, the first argument is returned.
     *
     * @param arg0 The first string argument to compare.
     * @param arg1 The second string argument to compare.
     * @return The string argument that represents the lower numeric value.
     */
    override fun min(arg0: String, arg1: String): String {
        return if (digitalService.toInt(arg0) < digitalService.toInt(arg1)) arg0 else arg1
    }

    /**
     * Determines whether the numeric value of a given string argument is zero.
     *
     * This method converts a string argument to its numeric value and checks if it is equal to zero.
     *
     * @param arg The string argument to check.
     * @return True if the numeric value of the argument is zero, false otherwise.
     */
    override fun isZero(arg: String): Boolean {
        return digitalService.toInt(arg) == 0
    }
}
