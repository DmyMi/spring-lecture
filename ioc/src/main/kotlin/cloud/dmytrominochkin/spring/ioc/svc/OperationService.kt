package cloud.dmytrominochkin.spring.ioc.svc

/**
 * Defines a service interface for performing arithmetic operations on string arguments that represent
 * numerical values. Implementations of this interface are responsible for defining the specific
 * operation (e.g., addition, subtraction, multiplication, division) to be performed on the inputs.
 */
interface OperationService {

    /**
     * Performs a specific arithmetic operation on two string arguments. The nature of the operation
     * (addition, subtraction, multiplication, division, etc.) is defined by the implementing class.
     *
     * The method is responsible for converting the string arguments to appropriate numerical values,
     * performing the operation, and then returning the result as a string. Implementations should
     * handle potential issues such as non-numeric inputs, numeric overflow, or division by zero,
     * as appropriate for the operation.
     *
     * @param arg0 The first string argument, representing a numerical value, to be used in the operation.
     * @param arg1 The second string argument, representing a numerical value, to be used in the operation.
     * @return The result of the operation as a string.
     */
    fun operation(arg0: String, arg1: String): String
}
