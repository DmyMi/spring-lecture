package cloud.dmytrominochkin.spring.ioc.svc

/**
 * Defines a service interface for digital conversion operations. This service provides functionality
 * to convert strings to integers.
 */
interface DigitalService {

    /**
     * Converts a given string argument to an integer. This function is designed to take a string
     * representation of a number and convert it into its integer value.
     *
     *
     * @param arg The string argument to be converted into an integer. The nature of the argument
     *        (e.g., format, expected range) depends on the specific implementation.
     * @return The integer representation of the input string.
     */
    fun toInt(arg: String): Int
}
