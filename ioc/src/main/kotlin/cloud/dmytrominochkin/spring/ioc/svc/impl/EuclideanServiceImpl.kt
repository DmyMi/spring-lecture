package cloud.dmytrominochkin.spring.ioc.svc.impl

import cloud.dmytrominochkin.spring.ioc.svc.ConvertService
import cloud.dmytrominochkin.spring.ioc.svc.EuclideanService
import cloud.dmytrominochkin.spring.ioc.svc.OperationService
import cloud.dmytrominochkin.spring.ioc.svc.OrderService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component


/**
 * A Spring component that implements [EuclideanService], providing an implementation of the
 * Euclidean algorithm to calculate the greatest common divisor (GCD) of two numbers represented
 * as strings. This implementation utilizes other service components for operations such as
 * validation, comparison, and arithmetic operations.
 *
 * @property operationService An [OperationService] qualified as "ModOperationService", used for
 * performing the modulo operation as part of the GCD calculation.
 * @property convertService A [ConvertService] used to validate and convert string inputs into a
 * proper numeric format.
 * @property orderService An [OrderService] used to determine the maximum and minimum of two numbers
 * during the GCD calculation process.
 */
@Component("EuclideanService")
class EuclideanServiceImpl @Autowired constructor(
    @Qualifier("ModOperationService") private val operationService: OperationService,
    private var convertService: ConvertService,
    private var orderService: OrderService
) : EuclideanService {

    /**
     * Calculates the greatest common divisor (GCD) of two numbers represented as strings using the
     * Euclidean algorithm. The inputs are first validated and converted to numeric format, and the
     * algorithm is then applied to find the GCD.
     *
     * The process involves repeated application of the modulo operation until the remainder becomes
     * zero, at which point the last non-zero remainder is returned as the GCD.
     *
     * @param arg0 The first string argument representing a number for the GCD calculation.
     * @param arg1 The second string argument representing a number for the GCD calculation.
     * @return The greatest common divisor of the two numbers as a string.
     */
    override fun calculateGCD(arg0: String, arg1: String): String {
        var max = orderService.max(convertService.validateDigits(arg0), convertService.validateDigits(arg1))
        var min = orderService.min(convertService.validateDigits(arg0), convertService.validateDigits(arg1))
        while (!orderService.isZero(min)) {
            val temp = operationService.operation(max, min)
            max = orderService.max(min, temp)
            min = orderService.min(min, temp)
        }
        return max
    }
}
