package cloud.dmytrominochkin.spring.ioc

import cloud.dmytrominochkin.spring.ioc.svc.EuclideanService
import org.springframework.context.annotation.AnnotationConfigApplicationContext


/**
 * Initializes and utilizes an AnnotationConfigApplicationContext with the defined Config class.
 * Demonstrates retrieving an EuclideanService bean and using it to calculate the GCD of two numbers.
 */
fun main() {
    val annotationConfigContext = AnnotationConfigApplicationContext(Config::class.java)
    val euclideanService = annotationConfigContext.getBean(EuclideanService::class.java)
    // or by name
    // val euclideanService = annotationConfigContext.getBean("EuclideanService")
    annotationConfigContext.close()
    println("GCD(18, 30) = ${euclideanService.calculateGCD("18", "30")}")
}