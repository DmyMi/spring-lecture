package cloud.dmytrominochkin.spring.ioc.svc

/**
 * Defines a service interface for calculating the Greatest Common Divisor (GCD) of two numbers.
 * Implementations of this interface are expected to provide an algorithm to compute the GCD of
 * two numbers represented as strings. This could involve validating the input strings as proper
 * numeric representations, converting them into an appropriate numeric format, and then applying
 * the Euclidean algorithm (or any other efficient algorithm) to find their GCD.
 */
interface EuclideanService {

    /**
     * Calculates and returns the Greatest Common Divisor (GCD) of two numbers represented by the
     * string arguments. The method is responsible for handling any necessary parsing or validation
     * of the input strings to ensure they represent valid numbers. The GCD is to be calculated in
     * a manner consistent with the Euclidean algorithm or a similar mathematical approach.
     *
     * @param arg0 The first string argument representing a number for the GCD calculation.
     * @param arg1 The second string argument representing a number for the GCD calculation.
     * @return The GCD of the two numbers as a string. If either input is not a valid representation
     *         of a number, the implementation should define how such cases are handled, potentially
     *         throwing an appropriate exception.
     */
    fun calculateGCD(arg0: String, arg1: String): String
}
