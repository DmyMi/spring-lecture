package cloud.dmytrominochkin.spring.ioc.svc

/**
 * Defines a service interface for performing order and numeric state evaluations on strings that
 * represent numerical values. This interface provides methods to determine the maximum and minimum
 * values between two strings, as well as to check if a given string represents the numeric value zero.
 */
interface OrderService {

    /**
     * Determines and returns the greater of two string arguments based on their numeric values.
     * If the numeric values are equal, the implementation may choose which argument to return based
     * on its own criteria.
     *
     * @param arg0 The first string argument to be compared.
     * @param arg1 The second string argument to be compared.
     * @return The string that represents the numerically greater value.
     */
    fun max(arg0: String, arg1: String): String

    /**
     * Determines and returns the lesser of two string arguments based on their numeric values.
     * If the numeric values are equal, the implementation may choose which argument to return based
     * on its own criteria.
     *
     * @param arg0 The first string argument to be compared.
     * @param arg1 The second string argument to be compared.
     * @return The string that represents the numerically lesser value.
     */
    fun min(arg0: String, arg1: String): String

    /**
     * Checks if the numeric value represented by a given string is zero.
     *
     * @param arg The string argument whose numeric value is to be checked.
     * @return True if the numeric value is zero, false otherwise.
     */
    fun isZero(arg: String): Boolean
}
