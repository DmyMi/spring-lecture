package cloud.dmytrominochkin.spring.ioc

import cloud.dmytrominochkin.spring.ioc.svc.EuclideanService
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.TestInstance
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import kotlin.test.assertEquals
import kotlin.test.Test


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AnnotationTest {

    private lateinit var annotationConfigContext: AnnotationConfigApplicationContext
    private val euclideanService: EuclideanService
        get() = annotationConfigContext.getBean(EuclideanService::class.java)

    @BeforeAll
    fun setup() {
        annotationConfigContext = AnnotationConfigApplicationContext(Config::class.java)
    }

    @AfterAll
    fun teardown() {
        annotationConfigContext.close()
    }

    @Test
    fun checkSimple() {
        val expected = "6"
        val actual = euclideanService.calculateGCD("18", "30")
        assertEquals(expected, actual, "check GCD(18,30)")
    }

    @Test
    fun checkOne() {
        val expected = "1"
        val actual = euclideanService.calculateGCD("1", "5")
        assertEquals(expected, actual, "check GCD(1,5)")
    }

    @Test
    fun checkEquals() {
        val expected = "2"
        val actual = euclideanService.calculateGCD("2", "2")
        assertEquals(expected, actual, "check GCD(2,2)")
    }

    @Test
    fun checkZero() {
        val expected = "2"
        val actual = euclideanService.calculateGCD("0", "2")
        assertEquals(expected, actual, "check GCD(0,2)")
    }

    @Test
    fun checkNegative() {
        val expected = "4"
        val actual = euclideanService.calculateGCD("-8", "20")
        assertEquals(expected, actual, "check GCD(-8,20)")
    }

    @Test
    fun checkInvalidData() {
        val expected = "4"
        val actual = euclideanService.calculateGCD("-8a", "20b")
        assertEquals(expected, actual, "check GCD(8a,20b)")
    }
}