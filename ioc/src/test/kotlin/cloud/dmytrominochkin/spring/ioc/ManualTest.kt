package cloud.dmytrominochkin.spring.ioc

import cloud.dmytrominochkin.spring.ioc.svc.DigitalService
import cloud.dmytrominochkin.spring.ioc.svc.EuclideanService
import cloud.dmytrominochkin.spring.ioc.svc.OperationService
import cloud.dmytrominochkin.spring.ioc.svc.OrderService
import cloud.dmytrominochkin.spring.ioc.svc.impl.ConvertServiceImpl
import cloud.dmytrominochkin.spring.ioc.svc.impl.DigitalServiceImpl
import cloud.dmytrominochkin.spring.ioc.svc.impl.EuclideanServiceImpl
import cloud.dmytrominochkin.spring.ioc.svc.impl.OrderServiceImpl
import cloud.dmytrominochkin.spring.ioc.svc.impl.op.ModOperationService
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ManualTest {
    private lateinit var euclideanService: EuclideanService

    @BeforeAll
    fun setup() {
        val digitalService: DigitalService = DigitalServiceImpl()
        val operationService: OperationService = ModOperationService(digitalService)
        val orderService: OrderService = OrderServiceImpl(digitalService)
        euclideanService = EuclideanServiceImpl(operationService, ConvertServiceImpl.convertService(), orderService)
    }

    @Test
    fun checkSimple() {
        val expected = "6"
        val actual = euclideanService.calculateGCD("18", "30")
        assertEquals(expected, actual, "check GCD(18,30)")
    }

    @Test
    fun checkOne() {
        val expected = "1"
        val actual = euclideanService.calculateGCD("1", "5")
        assertEquals(expected, actual, "check GCD(1,5)")
    }

    @Test
    fun checkEquals() {
        val expected = "2"
        val actual = euclideanService.calculateGCD("2", "2")
        assertEquals(expected, actual, "check GCD(2,2)")
    }

    @Test
    fun checkZero() {
        val expected = "2"
        val actual = euclideanService.calculateGCD("0", "2")
        assertEquals(expected, actual, "check GCD(0,2)")
    }

    @Test
    fun checkNegative() {
        val expected = "4"
        val actual = euclideanService.calculateGCD("-8", "20")
        assertEquals(expected, actual, "check GCD(-8,20)")
    }

    @Test
    fun checkInvalidData() {
        val expected = "4"
        val actual = euclideanService.calculateGCD("-8a", "20b")
        assertEquals(expected, actual, "check GCD(8a,20b)")
    }
}