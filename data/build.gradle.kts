plugins {
    alias(libs.plugins.kotlin.jvm)
    // The noArg plugin automatically generates zero-argument constructors for classes annotated with specific annotations.
    // Hibernate requires no-argument constructors for entity classes to instantiate them through reflection.
    alias(libs.plugins.kotlin.noarg)
    // The allOpen plugin makes classes annotated with specific annotations non-final. Kotlin classes are final by default,
    // which prevents Hibernate from proxying them to handle lazy loading of associations. The allOpen plugin is essential
    // for Hibernate to function correctly by enabling runtime enhancement of entity classes.
    alias(libs.plugins.kotlin.allopen)
    // The Spring plugin automatically applies both noArg and allOpen configurations for typical Spring annotations,
    // facilitating the integration with Spring Data, which also benefits from these adjustments for proxying and reflection.
    alias(libs.plugins.kotlin.spring)
    application
}

group = "cloud.dmytrominochkin.spring"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.spring.ioc)
    implementation(libs.hsqldb)
    implementation(libs.hibernate.core)
    // Kotlin's reflection is required for Spring Data's runtime reflection capabilities.
    implementation(libs.kotlin.reflect)
    implementation(libs.spring.data)

    implementation(libs.bundles.logging)

    testImplementation(libs.kotlin.test)
}

tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(17)
}

application {
    mainClass.set("cloud.dmytrominochkin.spring.data.MainKt")
}

// Configures the noArg plugin to generate zero-argument constructors for JPA entities,
// which is necessary for entity instantiation by Hibernate.
noArg {
    annotation("jakarta.persistence.Entity")
    annotation("jakarta.persistence.MappedSuperclass")
    annotation("jakarta.persistence.Embeddable")
    invokeInitializers = true // Ensures that property initializers are called in the generated constructors.
}

// Configures the allOpen plugin to prevent Kotlin from marking classes as final if they are annotated
// with JPA annotations. This is crucial for enabling runtime proxying and AOP-based enhancements.
allOpen {
    annotation("jakarta.persistence.Entity")
    annotation("jakarta.persistence.MappedSuperclass")
    annotation("jakarta.persistence.Embeddable")
}
