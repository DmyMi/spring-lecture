package cloud.dmytrominochkin.spring.data.repository

import cloud.dmytrominochkin.spring.data.entity.Book
import cloud.dmytrominochkin.spring.data.entity.Reader
import cloud.dmytrominochkin.spring.data.entity.Using
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*


/**
 * Spring Data repository interface for [Book] entities.
 *
 * Inherits from [JpaRepository], providing CRUD operations and more for [Book] entities without requiring implementations.
 *
 * - [JpaRepository]: A JPA-specific extension of [org.springframework.data.repository.Repository] interfaces in Spring Data. It provides a rich collection of methods
 *   for performing CRUD and pagination operations. Being a part of Spring Data, it allows for domain classes to be managed with
 *   minimal boilerplate code.
 *
 * - [findBookByTitleIgnoreCase]: Custom query method derived from method name conventions supported by Spring Data. It searches for a
 *   [Book] entity where the title matches the specified string, ignoring case differences.
 *
 * - [findAllByOrderByTitleAsc]: Another example of a query method inferred from the method name, returning a list of [Book] entities
 *   ordered by the title in ascending order.
 *
 * - [Query]: Specifies a custom JPQL (Java Persistence Query Language) query to fetch [Book] entities along with their associated
 *   [Using] entities (`usings`) in a single query. This helps in optimizing performance by reducing the number of database hits, a concept
 *   known as eager fetching.
 */

interface BookRepository : JpaRepository<Book, Long> {
    fun findBookByTitleIgnoreCase(title: String): Book?
    fun findAllByOrderByTitleAsc(): List<Book>
    @Query("SELECT DISTINCT book FROM Book book LEFT JOIN FETCH book.using usings")
    fun findAllWithUsing(): List<Book>
    @Query("SELECT book FROM Book book LEFT JOIN FETCH book.using usings WHERE book.idBook = :id")
    fun findByIdWithUsing(@Param("id") id: Long): Optional<Book>
}

/**
 * Repository interface for [Reader] entities.
 *
 * Inherits from [JpaRepository], providing CRUD operations and more for [Reader] entities without requiring implementations.
 *
 * - [findByNameIgnoreCase]: Custom repository method to find a [Reader] by their name, case-insensitively.
 */
interface ReaderRepository : JpaRepository<Reader, Long> {
    fun findByNameIgnoreCase(name: String): Reader?
}

/**
 * Repository interface for [Using] entities.
 *
 * Inherits from [JpaRepository], providing CRUD operations and more for [Using] entities without requiring implementations.
 */
interface UsingRepository : JpaRepository<Using, Long>