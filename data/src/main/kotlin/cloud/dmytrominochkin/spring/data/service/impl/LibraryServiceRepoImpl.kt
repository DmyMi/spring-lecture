package cloud.dmytrominochkin.spring.data.service.impl

import cloud.dmytrominochkin.spring.data.entity.Using
import cloud.dmytrominochkin.spring.data.repository.BookRepository
import cloud.dmytrominochkin.spring.data.repository.ReaderRepository
import cloud.dmytrominochkin.spring.data.repository.UsingRepository
import cloud.dmytrominochkin.spring.data.service.LibraryServiceBase
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.sql.Date

/**
 * This service is an example of using Repository
 * if you can't use [jakarta.persistence.CascadeType.PERSIST] in your entity
 */
@Service("repo")
class LibraryServiceRepoImpl @Autowired constructor(
    bookRepository: BookRepository,
    readerRepository: ReaderRepository,
    private val usingRepository: UsingRepository // Inject the UsingRepository
) : LibraryServiceBase(bookRepository, readerRepository) {
    override fun recordBookUsage(bookId: Long, readerId: Long, returnDate: Date): Using {
        val book = bookRepository.findByIdWithUsing(bookId).orElseThrow { IllegalArgumentException("Book not found") }
        val reader = readerRepository.findById(readerId).orElseThrow { IllegalArgumentException("Reader not found") }
        val usage = Using(dateReturn = returnDate, book = book, reader = reader)

        // Call UsingRepository to save the Using entity
        return usingRepository.save(usage)
    }
}