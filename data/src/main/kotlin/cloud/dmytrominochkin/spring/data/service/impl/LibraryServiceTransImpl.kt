package cloud.dmytrominochkin.spring.data.service.impl

import cloud.dmytrominochkin.spring.data.entity.Using
import cloud.dmytrominochkin.spring.data.repository.BookRepository
import cloud.dmytrominochkin.spring.data.repository.ReaderRepository
import cloud.dmytrominochkin.spring.data.service.LibraryServiceBase
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.sql.Date

/**
 * This service is built upon the usage of [Transactional] annotation
 * to automatically populate Lazy relationships (which might lead to N+1 queries, so better use join fetches)
 * and save updated entities
 */
@Service("trans")
class LibraryServiceTransImpl @Autowired constructor(
    bookRepository: BookRepository,
    readerRepository: ReaderRepository
) : LibraryServiceBase(bookRepository, readerRepository) {
    @Transactional
    override fun recordBookUsage(bookId: Long, readerId: Long, returnDate: Date): Using {
        // Using a simple findById, Lazy relationship will be populated automatically during the larger transaction
        val book = bookRepository.findById(bookId).orElseThrow { IllegalArgumentException("Book not found") }
        val reader = readerRepository.findById(readerId).orElseThrow { IllegalArgumentException("Reader not found") }
        val usage = Using(dateReturn = returnDate, book = book, reader = reader)

        // Associate the new Using with the Book.
        // Can also associate with Reader if needed for in logic
        book.using.add(usage)

        // Persist the Book at the end of transaction; the associated Using will be persisted due to CascadeType.PERSIST

        return usage
    }
}