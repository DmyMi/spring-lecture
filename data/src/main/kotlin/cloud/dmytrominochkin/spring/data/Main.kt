package cloud.dmytrominochkin.spring.data

import cloud.dmytrominochkin.spring.data.service.LibraryService
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import java.sql.Date

fun main() {
    val context = AnnotationConfigApplicationContext(Config::class.java)

    books(context)
}

fun books(context: ApplicationContext) {
    // Retrieve an instance of LibraryService from the Spring application context.
    // The service is identified by the key "main", which is how it's defined in the Spring configuration.
    val libraryService = context.getBeansOfType(LibraryService::class.java)["main"]!!

    // Creating Readers
    val reader1 = libraryService.addReader("Mykola")
    val reader2 = libraryService.addReader("Petro")

    // Creating Books
    val book1 = libraryService.addBook("Kobzar")
    val book2 = libraryService.addBook("1984")
    val book3 = libraryService.addBook("Dune")

    // Recording Book Usages
    val tomorrow = Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000)
    val nextWeek = Date(System.currentTimeMillis() + 7 * 24 * 60 * 60 * 1000)

    libraryService.recordBookUsage(book1.idBook, reader1.idReader, tomorrow)
    libraryService.recordBookUsage(book2.idBook, reader2.idReader, nextWeek)
    libraryService.recordBookUsage(book3.idBook, reader1.idReader, nextWeek)

    // Listing all Books to see the usages
    println("\nAll Books After Recording Usages:")
    libraryService.listAllBooksWithUsage().forEach { book ->
        println("Book: ${book.title} (ID: ${book.idBook})")
        book.using.forEach { using ->
            println("\tBorrowed by Reader ID: ${using.reader?.idReader} until ${using.dateReturn}")
        }
    }

    // Update a book title
    val updatedBook = book3.apply { title = "Dune 2" }
    libraryService.updateBook(updatedBook)

    // Delete a book
    libraryService.deleteBook(book2.idBook)

    // List all books after updates
    println("\nAll Books After Updates:")
    libraryService.listAllBooks().forEach {
        println("Book: ${it.title} (ID: ${it.idBook})")
    }
}