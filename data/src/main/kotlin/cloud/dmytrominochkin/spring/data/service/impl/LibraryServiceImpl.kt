package cloud.dmytrominochkin.spring.data.service.impl

import cloud.dmytrominochkin.spring.data.entity.Using
import cloud.dmytrominochkin.spring.data.repository.BookRepository
import cloud.dmytrominochkin.spring.data.repository.ReaderRepository
import cloud.dmytrominochkin.spring.data.service.LibraryServiceBase
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.sql.Date


/**
 * This service is built upon the usage of [jakarta.persistence.CascadeType.PERSIST]
  */
@Service("main")
class LibraryServiceImpl @Autowired constructor(
    bookRepository: BookRepository,
    readerRepository: ReaderRepository
) : LibraryServiceBase(bookRepository, readerRepository) {

    override fun recordBookUsage(bookId: Long, readerId: Long, returnDate: Date): Using {
        // Use a custom query to populate the `using` list
        val book = bookRepository.findByIdWithUsing(bookId).orElseThrow { IllegalArgumentException("Book not found") }
        val reader = readerRepository.findById(readerId).orElseThrow { IllegalArgumentException("Reader not found") }
        val usage = Using(dateReturn = returnDate, book = book, reader = reader)

        // Associate the new Using with the Book.
        // Can also associate with Reader if needed for in logic
        book.using.add(usage)

        // Persist the Book; the associated Using will be persisted due to CascadeType.PERSIST
        bookRepository.save(book)

        return usage
    }
}