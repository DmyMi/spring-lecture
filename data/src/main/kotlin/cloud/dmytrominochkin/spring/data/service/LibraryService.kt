package cloud.dmytrominochkin.spring.data.service

import cloud.dmytrominochkin.spring.data.entity.Book
import cloud.dmytrominochkin.spring.data.entity.Reader
import cloud.dmytrominochkin.spring.data.entity.Using
import java.sql.Date

interface LibraryService {

    /**
     * Adds a new book to the library.
     *
     * @param title The title of the book to add.
     * @return The saved [Book] entity.
     */
    fun addBook(title: String): Book

    /**
     * Finds a book by its title, ignoring case.
     *
     * @param title The title of the book to find.
     * @return The found [Book] or null if no book with the given title exists.
     */
    fun findBookByTitle(title: String): Book?

    /**
     * Lists all books in the library, ordered by title in ascending order.
     *
     * @return A list of all [Book].
     */
    fun listAllBooks(): List<Book>

    /**
     * Lists all books in the library, including their usage records.
     *
     * @return A list of all [Book].
     */
    fun listAllBooksWithUsage(): List<Book>

    /**
     * Updates the details of an existing book in the library.
     *
     * @param book The book entity with updated information.
     * @return The updated [Book] entity.
     */
    fun updateBook(book: Book): Book

    /**
     * Deletes a book from the library by its ID.
     *
     * @param bookId The ID of the book to delete.
     */
    fun deleteBook(bookId: Long)

    /**
     * Adds a new reader to the library.
     *
     * @param name The name of the reader to add.
     * @return The saved [Reader] entity.
     */
    fun addReader(name: String): Reader

    /**
     * Finds a reader by their name, ignoring case.
     *
     * @param name The name of the reader to find.
     * @return The found [Reader] or null if no reader with the given name exists.
     */
    fun findReaderByName(name: String): Reader?

    /**
     * Records a book usage by associating a book with a reader and setting the return date.
     *
     * @param bookId The ID of the book being used.
     * @param readerId The ID of the reader using the book.
     * @param returnDate The date by which the book should be returned.
     * @return The saved [Using] entity.
     */
    fun recordBookUsage(bookId: Long, readerId: Long, returnDate: Date): Using
}