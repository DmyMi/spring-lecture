package cloud.dmytrominochkin.spring.data.service

import cloud.dmytrominochkin.spring.data.entity.Book
import cloud.dmytrominochkin.spring.data.entity.Reader
import cloud.dmytrominochkin.spring.data.repository.BookRepository
import cloud.dmytrominochkin.spring.data.repository.ReaderRepository

/**
 * Base class for [LibraryService] with basic methods. [LibraryService.recordBookUsage] is implemented in
 * specific child classes to showcase different options you can use while working with Spring Data JPA.
 */
abstract class LibraryServiceBase(
    protected val bookRepository: BookRepository,
    protected val readerRepository: ReaderRepository
) : LibraryService {
    override fun addBook(title: String): Book {
        val book = Book(title = title)
        return bookRepository.save(book)
    }

    override fun findBookByTitle(title: String): Book? = bookRepository.findBookByTitleIgnoreCase(title)

    override fun listAllBooks(): List<Book> = bookRepository.findAllByOrderByTitleAsc()

    override fun listAllBooksWithUsage(): List<Book> = bookRepository.findAllWithUsing()

    override fun updateBook(book: Book): Book {
        // Check if the book exists in the database
        val existingBook = bookRepository.findById(book.idBook)
            .orElseThrow { IllegalArgumentException("Book with ID ${book.idBook} not found") }

        // Update the existing book's details
        existingBook.title = book.title

        // Save the updated book to the database
        return bookRepository.save(existingBook)
    }

    override fun deleteBook(bookId: Long) {
        // Check if the book exists before attempting to delete to avoid ConstraintViolationException
        if (!bookRepository.existsById(bookId)) {
            throw IllegalArgumentException("Book with ID $bookId does not exist")
        }
        bookRepository.deleteById(bookId)
    }

    override fun addReader(name: String): Reader {
        val reader = Reader(name = name)
        return readerRepository.save(reader)
    }

    override fun findReaderByName(name: String): Reader? = readerRepository.findByNameIgnoreCase(name)
}