package cloud.dmytrominochkin.spring.data.service.impl

import cloud.dmytrominochkin.spring.data.entity.Using
import cloud.dmytrominochkin.spring.data.repository.BookRepository
import cloud.dmytrominochkin.spring.data.repository.ReaderRepository
import cloud.dmytrominochkin.spring.data.service.LibraryServiceBase
import jakarta.persistence.EntityManager
import jakarta.persistence.PersistenceContext
import jakarta.persistence.PersistenceContextType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.sql.Date

/**
 * This service is an example of using [PersistenceContext] ([EntityManager])
 * if you can't use [jakarta.persistence.CascadeType.PERSIST] in your entity or operation is not directly supported
 * by the repository.
 */
@Service("pc")
class LibraryServicePCImpl @Autowired constructor(
    bookRepository: BookRepository,
    readerRepository: ReaderRepository
) : LibraryServiceBase(bookRepository, readerRepository) {

    /**
     * The [PersistenceContext] annotation is used within the JPA to automatically
     * inject an [EntityManager] instance into a field, method, or constructor of a bean. This annotation
     * abstracts the process of looking up and creating [EntityManager] instances, allowing developers to
     * focus on business logic rather than boilerplate code for resource management.
     *
     * The most common usage, where [EntityManager] is directly injected into a field
     *   of a bean managed by the Spring container or another Java EE container.
     *
     * Attributes:
     * - [PersistenceContext.unitName]: Optional attribute specifying the name of the persistence unit. It is necessary when
     *   there are multiple persistence units defined in the application.
     *
     * - [PersistenceContext.type]: Specifies the type of the persistence context. It can be [PersistenceContextType.TRANSACTION]
     *   (default) for transaction-scoped persistence contexts or [PersistenceContextType.EXTENDED] for
     *   extended persistence contexts.
     *
     * By default, the [PersistenceContext] annotation injects a transaction-scoped [EntityManager].
     *   The lifecycle of this [EntityManager] is bound to the current JPA transaction.
     *
     * For stateful beans requiring a longer-lived persistence context, the EXTENDED type can be used,
     *   which spans multiple transactions and is tied to the lifecycle of the bean instance.
     */
    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    private lateinit var entityManager: EntityManager

    override fun recordBookUsage(bookId: Long, readerId: Long, returnDate: Date): Using {
        // Using a custom query to populate the `using` list
        val book = bookRepository.findByIdWithUsing(bookId).orElseThrow()
        val reader = readerRepository.findById(readerId).orElseThrow()
        val usage = Using(dateReturn = returnDate, book = book, reader = reader)

        // Persist the Using with an EntityManager.
        // It is better to use repositories or cascade instead of pure JPA if you have such option
        entityManager.persist(usage)
        return usage
    }
}